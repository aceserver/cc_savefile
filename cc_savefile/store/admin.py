from django.contrib import admin

from .models import (StoreProduct, Transaction, CartItem, GenerateLicenseRequest)
from nested_admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline

class StoreProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'order', 'is_active')
    list_editable = ["order", "is_active"]
    search_fields = ["title",]


class CartItemInline(NestedTabularInline):
    model = CartItem
    extra = 0


class TransactionAdmin(NestedModelAdmin):
    list_display = ('transaction_id', 'transaction_date', 'user', 'grandtotal', 'status', )
    list_filter = ["status"]
    search_fields = ["transaction_id", "user__username", "user__email"]

    inlines = [
        CartItemInline,
        ]

class CartItemAdmin(NestedModelAdmin):
    list_display = ('product', 'qty', 'user', 'total', 'transaction', )
    list_filter = ["product"]
    search_fields = ["product__title", "user__username", "user__email"]
    list_editable = ["transaction"]


class GenerateLicenseRequestAdmin(NestedModelAdmin):
    list_display = ('created', 'schooluser', 'grandtotal', 'status', 'receipt_file', 'is_generated')
    list_filter = ["status"]
    list_editable = ['status']
    search_fields = ["schooluser__auth_user__username", 
    "schooluser__auth_user__email", "schooluser__full_name"]

    inlines = [
        CartItemInline,
        ]

admin.site.register(CartItem, CartItemAdmin)
admin.site.register(StoreProduct, StoreProductAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(GenerateLicenseRequest, GenerateLicenseRequestAdmin)