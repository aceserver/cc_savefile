import datetime
import pytz
import json
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy as reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.middleware import csrf
from django.shortcuts import render
from django.template import loader, RequestContext
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.views.generic import CreateView, TemplateView, DetailView
from django.views.generic.edit import FormView, UpdateView
from moneyed import Money, USD
from moneyed.localization import format_money

from cc_savefile.utils import json_response

from users.models import (MasterUser, SchoolUser, )
from users.utils import send_generate_license_request_email
from gamesaves.models import (GamePlatform, Game, Platform)

from .models import StoreProduct, CartItem, Transaction, GenerateLicenseRequest
from .utils import get_cart_table_context, get_cart_table
from .forms import AttachPaymentReceiptForm


class StoreView(TemplateView):
    template_name = 'store/store.html'

    def get_context_data(self, **kwargs):
        context = super(StoreView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['cartitems'] = self.request.user.cartitem_set.filter(transaction__isnull=True, generate_request__isnull=True)
        else:
            context['cartitems'] = []
        context['storeproducts'] = StoreProduct.objects.filter(is_active=True)
        return context



""" ---  CART --- """
class CartView(TemplateView):
    template_name = 'store/cart.html'

    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        context['cart_table'] = get_cart_table_context(self.request)
        # if self.request.user.is_authenticated():
        #     context['cartitems'] = self.request.user.cartitem_set.filter(transaction__isnull=True, generate_request__isnull=True)
        # else:
        #     context['cartitems'] = []

        # AttachPaymentReceipt Form
        try:
            schooluser = self.request.user.schooluser
            if schooluser:
                context['attach_payment_receipt_form'] = AttachPaymentReceiptForm(initial={"schooluser": schooluser})
        except:
            pass

        return context


class CartBuySuccessView(TemplateView):
    template_name = 'store/buy_success.html'

    def get_context_data(self, **kwargs):
        context = super(CartBuySuccessView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['cartitems'] = self.request.user.cartitem_set.filter(transaction__isnull=True, generate_request__isnull=True)
        else:
            context['cartitems'] = []

        return context


def attach_payment_receipt(request):
    post_data = request.POST
    file_data = request.FILES
    if post_data:
        form = AttachPaymentReceiptForm(request.POST, request.FILES)

        if form.is_valid():
            generate_request = form.save()
            cartitems = request.user.cartitem_set.filter(transaction__isnull=True, generate_request__isnull=True)
            grandtotal = Money(0, USD)
            for item in cartitems:
                grandtotal += item.total
            generate_request.grandtotal = grandtotal
            generate_request.save()

            cartitems.update(generate_request=generate_request)
            
            schoolname = generate_request.schooluser.school if generate_request.schooluser.school else generate_request.schooluser.full_name
            send_generate_license_request_email(generate_request, schoolname)
            return HttpResponseRedirect(reverse('store_attach_payment_receipt_success'))
        else:
            print form.errors
            return form
    else:
        return HttpResponseRedirect(reverse('store'))


class AttachPaymentReceiptSuccessView(TemplateView):
    template_name = 'store/attach_payment_receipt_success.html'


