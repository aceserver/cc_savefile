from __future__ import unicode_literals
import datetime
import pytz
import string
import random
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import now as datetime_now
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth.models import User
from django.db import models
from moneyed import Money, USD
from djmoney.models.fields import MoneyField
from moneyed.localization import format_money

from gamesaves.models import Game, Platform, GamePlatform
from users.models import MasterUser, SchoolUser
from users.utils import *

class StoreProduct(TimeStampedModel):
    title = models.CharField(verbose_name=_("Title"),
        max_length=255, blank=True, null=True)
    sku = models.CharField(verbose_name=_("SKU"),
        max_length=100, blank=True, null=True)
    gameplatforms = models.ManyToManyField("gamesaves.GamePlatform",
                        verbose_name=_("Game-Platforms"),blank=True)

    price = MoneyField(max_digits=10, decimal_places=2, default_currency='USD')

    order = models.PositiveIntegerField(_("Order"), default=0, help_text=_("Lower number comes first"))

    is_active = models.BooleanField(_('Is Active'), default=True)

    class Meta:
        ordering = ["order"]

    def __unicode__(self):
        return "%s" % self.title

    def get_formatted_price(self):
        return format_money(self.price)



class CartItem(TimeStampedModel):
    product = models.ForeignKey('store.StoreProduct',
        verbose_name=_('Product'), blank=True, null=True)
    user = models.ForeignKey("auth.User",
        verbose_name=_("Ordered By"), blank=True, null=True, editable=False)
    qty = models.IntegerField(verbose_name=_("Quantity"),
        default=1)
    total = MoneyField(verbose_name=_("Total"),
        max_digits=10, decimal_places=2, default_currency='USD')
    transaction = models.ForeignKey('store.Transaction',
        verbose_name=_('Transaction'), blank=True, null=True)
    generate_request = models.ForeignKey('store.GenerateLicenseRequest',
        verbose_name=_('Generate License Request'), blank=True, null=True)

    class Meta:
        ordering = ["-modified"]

    def get_formatted_total(self):
        return format_money(self.total)


class Transaction(TimeStampedModel):
    PENDING = 1
    PAID = 2
    CANCELLED = 3
    STATUS = (
        (PENDING, _('Pending Payment')), # Pending payment
        (PAID, _('Paid')), # Payment confirmed
        (CANCELLED, _('Payment Cancelled')), # Payment cancelled
    )

    user = models.ForeignKey("auth.User",
        verbose_name=_("Ordered By"), blank=True, null=True, editable=False)
    transaction_id = models.CharField(verbose_name=_("Transaction ID"),
        max_length=255, db_index=True, unique=True)
    transaction_date = models.DateTimeField(verbose_name=_('Transaction Date'),
        blank=True, null=True)
    grandtotal = MoneyField(verbose_name=_("Grand Total"),
        max_digits=10, decimal_places=2, default_currency='USD')
    status = models.IntegerField(_("Status"),
        choices=STATUS, default=PENDING)

    class Meta:
        ordering = ["-modified"]

    def __unicode__(self):
        return "%s" % self.transaction_id

    def generate_random_id(self):
        _key = ''.join(random.choice(string.ascii_uppercase) for _ in range(8))
        _date = datetime_now().date()
        return "%s%s%s%s" % (_date.year, _date.month, _date.day, _key)

    def save(self, *args, **kwargs):
        if not self.transaction_id:
            generate = True
            while generate:
                transaction_id = self.generate_random_id()
                exist_transaction = Transaction.objects.filter(transaction_id=transaction_id)
                if not exist_transaction:
                    self.transaction_id = transaction_id
                    generate = False

        super(Transaction, self).save(*args, **kwargs)



class GenerateLicenseRequest(TimeStampedModel):
    FOLLOWUP = 1
    DECLINED = 2
    APPROVED = 3

    STATUS_LIST = (
        (FOLLOWUP, _("Follow-up")),
        (DECLINED, _("Declined")),
        (APPROVED, _("Approved")),
    )

    schooluser = models.ForeignKey("users.SchoolUser",
        verbose_name=_("Requested By"), blank=True, null=True)
    grandtotal = MoneyField(verbose_name=_("Grand Total"),
        max_digits=10, decimal_places=2, default_currency='USD')
    status = models.IntegerField(_("Status"),
        choices=STATUS_LIST, default=FOLLOWUP)
    receipt_file = models.FileField(
        upload_to='uploads/payment_receipt/', blank=True, null=True)
    is_generated = models.BooleanField(_("Is Generated"), default=False)

    def save(self, *args, **kwargs):
        if self.status == self.APPROVED and not self.is_generated:
            cartitems = self.cartitem_set.all()
            for item in cartitems:
                gameplatforms = item.product.gameplatforms.all()
                for i in range(0, item.qty):
                    license = MasterUser.objects.create(schooluser=self.schooluser)
                    license.gameplatforms.add(*gameplatforms)

            self.is_generated = True

            if self.id:
                generate_license_approve_email(self)
        super(GenerateLicenseRequest, self).save(*args, **kwargs)

    def license_count(self):
        count = 0
        for item in self.cartitem_set.all():
            count += item.qty
        return count
