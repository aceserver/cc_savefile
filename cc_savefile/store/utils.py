import datetime, pytz, json, requests
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy as reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now as datetime_now
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.http import require_http_methods

from moneyed import Money, USD
from moneyed.localization import format_money

from constance import config

from cc_savefile.utils import json_response

from users.models import (MasterUser, SchoolUser, )
from gamesaves.models import (GamePlatform, Game, Platform)

from .models import StoreProduct, CartItem, Transaction


def get_cart_table_context(request):
    context = {}
    
    if request.user.is_authenticated() and request.user.cartitem_set.all():
        context['cartitems'] = request.user.cartitem_set.filter(Q(generate_request__isnull=True), Q(transaction__isnull=True) | Q(transaction__status=Transaction.PENDING)).order_by("product__order")
    else:
        context['cartitems'] = []

    grandtotal = Money(0, USD)
    for item in context['cartitems']:
        grandtotal += item.total

    context['grandtotal'] = grandtotal
    context['grandtotal_formatted'] = format_money(grandtotal)
    
    return context


def get_cart_table(request):
    context = get_cart_table_context(request)
    return render(request, 'store/includes/cart_table.html', context)


def update_cartitem(request):
    qty = request.GET.get("qty")
    cartitem_id = request.GET.get("cartitem", None)
    if cartitem_id:
        cartitem = CartItem.objects.get(id=cartitem_id)
        cartitem.qty = qty
        cartitem.total = cartitem.qty * cartitem.product.price
        cartitem.save()

    cart_html = get_cart_table(request)
    return cart_html



def remove_cartitem(request):
    cartitem_id = request.GET.get("cartitem")
    cartitem = CartItem.objects.get(id=cartitem_id)
    cartitem.delete()
    cart_html = get_cart_table(request)
    return cart_html


def add_to_cart(request):
    qty = request.POST.get('qty', 0)
    product_id = request.POST.get('product', None)

    product = StoreProduct.objects.get(id=product_id)

    exist_cartitem = request.user.cartitem_set.filter(Q(product=product), Q(generate_request__isnull=True), Q(transaction__isnull=True) | Q(transaction__status=Transaction.PENDING))
    if exist_cartitem:
        cartitem = exist_cartitem.first()
        cartitem.qty += int(qty)
        cartitem.total = cartitem.qty * product.price
        cartitem.save()
    else:
        cartitem = CartItem.objects.create(
                    product=product,
                    user=request.user,
                    qty=int(qty),
                    total=int(qty)*product.price
                    )

    return HttpResponseRedirect(reverse('cart'))


import paypalrestsdk
import logging
from django.contrib.sites.models import Site

@csrf_exempt
@require_http_methods(["GET", "POST"])
def create_payment(request):
    if settings.TEST == True:
        mode = "sandbox"
        client_id = config.PAYPAL_CLIENT_ID_SANDBOX
        client_secret = config.PAYPAL_CLIENT_SECRET_SANDBOX
    else:
        mode = "live"
        client_id = config.PAYPAL_CLIENT_ID_LIVE
        client_secret = config.PAYPAL_CLIENT_SECRET_LIVE

    if settings.DEBUG == True:
        host = "http://localhost:8000"
    else:
        host = "https://%s" % Site.objects.get_current()

    context_data = get_cart_table_context(request)
    if context_data['cartitems']:
        exist_item_with_transaction = context_data['cartitems'].filter(transaction__isnull=False).first()
        if exist_item_with_transaction:
            user_transaction = exist_item_with_transaction.transaction
            user_transaction.grandtotal = context_data["grandtotal"]
            user_transaction.transaction_date = datetime_now()
            user_transaction.save()
        else:
            user_transaction = Transaction.objects.create(user=request.user, transaction_date=datetime_now(), grandtotal=context_data["grandtotal"])

        items_list = []
        for cartitem in context_data["cartitems"]:
            item_data = {
                "quantity": cartitem.qty,
                "sku": str(cartitem.product.sku),
                "name": str(cartitem.product.title),
                "price": "%.2f" % cartitem.product.price.amount,
                "currency": str(cartitem.product.price.currency),
                }
            items_list.append(item_data)

        context_data["cartitems"].update(transaction=user_transaction)


        myapi = paypalrestsdk.configure({
            "mode": mode, # sandbox or live
            "client_id": client_id,
            "client_secret": client_secret 
            })

        transaction_data = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"},
            "redirect_urls": {
                "return_url": "%s%s" % (host, reverse("store_buy_success")),
                "cancel_url": "%s%s" % (host, reverse("cart"))
                },
            "transactions": [{
                "item_list": {
                    "items": items_list
                    },
                "amount": {
                    "total": "%.2f" % context_data["grandtotal"].amount,
                    "currency": str(context_data["grandtotal"].currency)
                    },
                "description": "",
                "invoice_number": "%s" % user_transaction.transaction_id
                }]
            }

        payment = paypalrestsdk.Payment(transaction_data, api = myapi)

        if payment.create():
            print("Payment created successfully")

            data = {'paymentID': payment.id}
            return json_response(**data)

        else:
            print("Payment create error")
            print(payment.error)
            return HttpResponseRedirect(reverse("cart"))
    else:
        return HttpResponseRedirect(reverse("cart"))


from django.db import transaction

@csrf_exempt
@require_http_methods(["GET", "POST"])
def execute_payment(request):
    payer_id = request.POST.get("payerID")
    payment_id = request.POST.get("paymentID")
    try:
        payment = paypalrestsdk.Payment.find(payment_id)

        if payment.execute({"payer_id": payer_id}):
            print("Payment execute successfully")
            user_transaction = Transaction.objects.get(transaction_id=payment.transactions[0].invoice_number)

            if payment.state == "approved":
                # Approved payment
                sid = transaction.savepoint()

                try:
                    for item in payment.transactions[0].item_list.items:
                        storeproduct = StoreProduct.objects.get(sku=item.sku)

                        for i in range(0, item.quantity):
                            license = MasterUser.objects.create(schooluser=request.user.schooluser)
                            license.gameplatforms.add(*storeproduct.gameplatforms.all())

                    user_transaction.status = Transaction.PAID
                    user_transaction.save()

                    transaction.savepoint_commit(sid)
                except Exception as e:
                    print e
                    transaction.savepoint_rollback(sid)

                data = {'redirect_url': payment.redirect_urls.return_url}
                return json_response(**data)

            else:
                # Cancelled payment
                user_transaction.status = Transaction.CANCELLED
                user_transaction.save()

                cartitems = user_transaction.cartitem_set.all()
                cartitems.update(transaction=None)

                data = {'redirect_url': payment.redirect_urls.cancel_url}
                return json_response(**data)
        else:
            print("Execute payment error")
            print(payment.error) # Error Hash
            return HttpResponseRedirect(reverse("cart"))
    
    except Exception as e:
        print "Execute payment error. %s" % e
        return HttpResponseRedirect(reverse("cart"))



