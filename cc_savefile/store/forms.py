import json
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (authenticate, get_user_model, password_validation)
from django.contrib.auth.models import User
from django.forms.widgets import HiddenInput, CheckboxSelectMultiple, DateInput, TimeInput
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import localtime, now
from form_utils.forms import BetterModelForm, BetterForm

from .models import *
# from .utils import send_license_activation_email, send_license_set_username_email, send_license_set_username_success_email



class AttachPaymentReceiptForm(BetterModelForm):
    def __init__(self, *args, **kwargs):
        super(AttachPaymentReceiptForm, self).__init__(*args, **kwargs)

    receipt_file = forms.FileField(widget=forms.ClearableFileInput(), required=False)

    class Meta:
        model = GenerateLicenseRequest
        exclude = ['status', 'is_generated', 'grandtotal']
        fields = ['receipt_file', 'schooluser']
        widgets = {'schooluser': forms.HiddenInput()}

    def clean(self): 
        return self.cleaned_data
