from django.conf.urls import include, url
from django.conf.urls import url
from .views import (StoreView, CartView, 
    CartBuySuccessView, attach_payment_receipt, 
    AttachPaymentReceiptSuccessView)
from .utils import update_cartitem, remove_cartitem, add_to_cart, create_payment, execute_payment

urlpatterns = [
    url(r'^store/add/$',
        add_to_cart,
        name='add_to_cart'),

    url(r'^store/update-cart/$',
        update_cartitem,
        name='store_update_cartitem'),

    url(r'^store/remove-cart-item/$',
        remove_cartitem,
        name='store_remove_cartitem'),

    url(r'^store/create_payment/$',
        create_payment,
        name='store_create_payment'),

    url(r'^store/execute_payment/$',
        execute_payment,
        name='store_execute_payment'),

    url(r'^store/attach-payment-receipt/$',
        attach_payment_receipt,
        name='store_attach_payment_receipt'),

    url(r'^store/buy-success/$',
        CartBuySuccessView.as_view(),
        name='store_buy_success'),

    url(r'^store/attach-payment-receipt-success/$',
        AttachPaymentReceiptSuccessView.as_view(),
        name='store_attach_payment_receipt_success'),

    url(r'^store/$',
        StoreView.as_view(),
        name='store'),

    url(r'^checkout/$',
        CartView.as_view(),
        name='cart'),

    ]