from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core import signing
from django.core.mail import mail_admins, send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse_lazy as reverse
from django.core.mail import EmailMultiAlternatives

from constance import config

REGISTRATION_SALT = getattr(settings, 'REGISTRATION_SALT', 'registration')

def create_inactive_user(form):
    """
    Create the inactive user account and send an email containing
    activation instructions.

    """
    new_user = form.save(commit=False)
    new_user.is_active = False
    new_user.save()

    send_activation_email(new_user)

    return new_user

def get_activation_key(user, salt=None, add_email=True):
    """
    Generate the activation key which will be emailed to the user.

    """
    if not salt:
        salt = REGISTRATION_SALT

    if add_email:
        return signing.dumps(
            obj="%s#%s" % (getattr(user, user.USERNAME_FIELD), user.email),
            salt=salt
        )
    else:
        return signing.dumps(
            obj="%s" % getattr(user, user.USERNAME_FIELD),
            salt=salt
        )


def validate_activation_key(keystring, salt=None, check_max_age=True):
    """
    Validate the activation key which was emailed to the user.

    """
    if not salt:
        salt = REGISTRATION_SALT

    try:
        if check_max_age:
            userinfo_string = signing.loads(
                    keystring,
                    salt=salt,
                    max_age=config.LINK_ACTIVATION_DAYS * 86400
                )
        else:
            userinfo_string = signing.loads(
                    keystring,
                    salt=salt
                )
        userinfo_string_split = userinfo_string.split("#")
        username = userinfo_string_split[0]
        email = userinfo_string_split[1]
        return username, email
    # SignatureExpired is a subclass of BadSignature, so this will
    # catch either one.
    except signing.BadSignature:
        return None, None


def get_email_context(activation_key):
    """
    Build the template context used for the activation email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s' % reverse("set_password_before_activate", kwargs={"activation_key": activation_key})
    else:
        url = 'https://%s%s' % (Site.objects.get_current(), reverse("set_password_before_activate", kwargs={"activation_key": activation_key}))

    return {
        # 'activation_key': activation_key,
        'expiration_days': config.LINK_ACTIVATION_DAYS,
        'activation_url': url,
    }


def get_activation_email_context(activation_key):
    """
    Build the template context used for the activation email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s' % reverse("registration_activate", kwargs={"activation_key": activation_key})
    else:
        url = 'https://%s%s' % (Site.objects.get_current(), reverse("registration_activate", kwargs={"activation_key": activation_key}))

    return {
        # 'activation_key': activation_key,
        'expiration_days': config.LINK_ACTIVATION_DAYS,
        'activation_url': url,
    }


def send_activation_email(user):
    """
    Send the activation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    email_body_template = 'registration/activation_email.txt'
    email_body_html_template = 'registration/activation_email.html'
    email_subject_template = 'registration/activation_email_subject.txt'

    activation_key = get_activation_key(user, add_email=False)
    context = get_activation_email_context(activation_key)
    # context = get_email_context(activation_key)

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context.update({
        'user': user,
        'site': site,
    })
    subject = render_to_string(email_subject_template,
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template, context)
    user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


def resend_activation_email(request):
    email = request.POST.get("email") or request.GET.get("email")
    exist_user = User.objects.filter(email=email).first()
    if exist_user:
        send_activation_email(exist_user)
    return HttpResponseRedirect(reverse("home"))


# START License Emails
def get_license_activation_email_context(activation_key):
    """
    Build the template context used for the activation email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s' % reverse("activate_license", kwargs={"activation_key": activation_key})
    else:
        url = 'https://%s%s' % (Site.objects.get_current(), reverse("activate_license", kwargs={"activation_key": activation_key}))

    return {
        'expiration_days': config.LINK_ACTIVATION_DAYS,
        'activation_url': url
    }

def send_license_activation_email(masteruser):
    """
    Send the activation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    email_body_template = 'users/emails/license_activation_email.txt'
    email_body_html_template = 'users/emails/license_activation_email.html'
    email_subject_template = 'users/emails/license_activation_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    activation_key = get_activation_key(masteruser.auth_user)
    context = get_license_activation_email_context(activation_key)
    context.update({
        'masteruser': masteruser,
        'site':site,
    })

    subject = render_to_string(email_subject_template,
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    masteruser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)



def get_license_set_username_email_context(activation_key):
    """
    Build the template context used for the set username email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s' % reverse("license_set_username", kwargs={"activation_key": activation_key})
    else:
        url = 'https://%s%s' % (Site.objects.get_current(), reverse("license_set_username", kwargs={"activation_key": activation_key}))

    return {
        'expiration_days': config.LINK_ACTIVATION_DAYS,
        'activation_url': url
    }

def send_license_set_username_email(masteruser):
    """
    Send the set username email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    email_body_template = 'users/emails/license_set_username_email.txt'
    email_body_html_template = 'users/emails/license_set_username_email.html'
    email_subject_template = 'users/emails/license_set_username_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    activation_key = get_activation_key(masteruser.auth_user)
    context = get_license_set_username_email_context(activation_key)
    context.update({
        'masteruser': masteruser,
        'site':site,
    })

    subject = render_to_string(email_subject_template,
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    masteruser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


def send_license_set_username_success_email(masteruser):
    """
    Send the set username success email.

    """
    email_body_template = 'users/emails/license_set_username_success_email.txt'
    email_body_html_template = 'users/emails/license_set_username_success_email.html'
    email_subject_template = 'users/emails/license_set_username_success_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'masteruser': masteruser,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    masteruser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)



def send_cancel_invite_email(license_email, license_fullname, license_schoolname):
    """
    Send the activation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    email_body_template = 'users/emails/cancel_invite_email.txt'
    email_body_html_template = 'users/emails/cancel_invite_email.html'
    email_subject_template = 'users/emails/cancel_invite_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_email': license_email,
        'license_fullname': license_fullname,
        'license_schoolname': license_schoolname,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [license_email], html_message=html_message)

# END License Emails


def get_acelogin_activation_email_context(activation_key):
    """
    Build the template context used for the activation email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s' % reverse("activate_license", kwargs={"activation_key": activation_key})
    else:
        url = 'https://%s%s' % (Site.objects.get_current(), reverse("activate_license", kwargs={"activation_key": activation_key}))

    return {
        'expiration_days': config.LINK_ACTIVATION_DAYS,
        'activation_url': url
    }

def send_acelogin_activation_email(masteruser):
    """
    Send the activation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    email_body_template = 'users/emails/masteruser_activation_email.txt'
    email_body_html_template = 'users/emails/masteruser_activation_email.html'
    email_subject_template = 'users/emails/masteruser_activation_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    activation_key = get_activation_key(masteruser.auth_user)
    context = get_acelogin_activation_email_context(activation_key)
    context.update({
        'masteruser': masteruser,
        'site':site,
    })

    subject = render_to_string(email_subject_template,
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    masteruser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


# License Request
def send_more_freefull_license_request_email(license_request_obj, schoolname):
    """
    Send the set username success email.

    """
    email_body_template = 'users/emails/more_freefull_license_request_email.txt'
    email_body_html_template = 'users/emails/more_freefull_license_request_email.html'
    email_subject_template = 'users/emails/more_freefull_license_request_email_subject.txt'

    to_system_email_body_template = 'users/emails/more_freefull_license_request_email_to_system.txt'
    to_system_email_body_html_template = 'users/emails/more_freefull_license_request_email_to_system.html'
    to_system_email_subject_template = 'users/emails/more_freefull_license_request_email_subject_to_system.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'schoolname': schoolname,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.freefull_schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


    to_system_subject = render_to_string(to_system_email_subject_template,
                               context)
    to_system_subject = ''.join(to_system_subject.splitlines())
    to_system_message = render_to_string(to_system_email_body_template,
                               context)
    to_system_html_message = render_to_string(to_system_email_body_html_template,
                               context)
    mail_admins(to_system_subject, to_system_message, settings.DEFAULT_FROM_EMAIL, html_message=to_system_html_message)



def send_more_demo_license_request_email(license_request_obj, schoolname):
    """
    Send the set username success email.

    """
    email_body_template = 'users/emails/more_demo_license_request_email.txt'
    email_body_html_template = 'users/emails/more_demo_license_request_email.html'
    email_subject_template = 'users/emails/more_demo_license_request_email_subject.txt'

    to_system_email_body_template = 'users/emails/more_demo_license_request_email_to_system.txt'
    to_system_email_body_html_template = 'users/emails/more_demo_license_request_email_to_system.html'
    to_system_email_subject_template = 'users/emails/more_demo_license_request_email_subject_to_system.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'schoolname': schoolname,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.demo_schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


    to_system_subject = render_to_string(to_system_email_subject_template,
                               context)
    to_system_subject = ''.join(to_system_subject.splitlines())
    to_system_message = render_to_string(to_system_email_body_template,
                               context)
    to_system_html_message = render_to_string(to_system_email_body_html_template,
                               context)
    mail_admins(to_system_subject, to_system_message, settings.DEFAULT_FROM_EMAIL, html_message=to_system_html_message)


def send_generate_license_request_email(license_request_obj, schoolname):
    """
    Send the set username success email.

    """
    email_body_template = 'users/emails/generate_license_request_email.txt'
    email_body_html_template = 'users/emails/generate_license_request_email.html'
    email_subject_template = 'users/emails/generate_license_request_email_subject.txt'

    to_system_email_body_template = 'users/emails/generate_license_request_email_to_system.txt'
    to_system_email_body_html_template = 'users/emails/generate_license_request_email_to_system.html'
    to_system_email_subject_template = 'users/emails/generate_license_request_email_subject_to_system.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'schoolname': schoolname,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


    to_system_subject = render_to_string(to_system_email_subject_template,
                               context)
    to_system_subject = ''.join(to_system_subject.splitlines())
    to_system_message = render_to_string(to_system_email_body_template,
                               context)
    to_system_html_message = render_to_string(to_system_email_body_html_template,
                               context)
    mail_admins(to_system_subject, to_system_message, settings.DEFAULT_FROM_EMAIL, html_message=to_system_html_message)


def freefull_license_approve_email(license_request_obj):
    """
    Send approved License request email to School User.

    """
    email_body_template = 'users/emails/freefull_license_approve_email.txt'
    email_body_html_template = 'users/emails/freefull_license_approve_email.html'
    email_subject_template = 'users/emails/freefull_license_approve_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.freefull_schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)



def demo_license_approve_email(license_request_obj):
    """
    Send approved License request email to School User.

    """
    email_body_template = 'users/emails/demo_license_approve_email.txt'
    email_body_html_template = 'users/emails/demo_license_approve_email.html'
    email_subject_template = 'users/emails/demo_license_approve_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.demo_schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)


def generate_license_approve_email(license_request_obj):
    """
    Send approved License request email to School User.

    """
    email_body_template = 'users/emails/generate_license_approve_email.txt'
    email_body_html_template = 'users/emails/generate_license_approve_email.html'
    email_subject_template = 'users/emails/generate_license_approve_email_subject.txt'

    if settings.DEBUG:
        site = 'http://localhost:8000'
    else:
        site = 'https://%s' % Site.objects.get_current()

    context = {
        'license_request_obj': license_request_obj,
        'site':site,
    }

    subject = render_to_string(email_subject_template,
                               context)
    subject = ''.join(subject.splitlines())
    message = render_to_string(email_body_template,
                               context)
    html_message = render_to_string(email_body_html_template,
                               context)
    license_request_obj.schooluser.auth_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL, html_message=html_message)

# License Request END


def get_invitation_to_download_email_context(key):
    """
    Build the template context used for the activation email.

    """
    if settings.DEBUG:
        url = 'http://localhost:8000%s?key=%s' % (reverse("invitation_to_download_game"), key)
    else:
        url = 'https://%s%s?key=%s' % (Site.objects.get_current(), reverse("invitation_to_download_game"), key)

    return {
        'expiration_days': settings.LINK_ACTIVATION_DAYS,
        'activation_url': url
    }


def send_invitation_to_download_email(user_obj):
    """
    Send the invitation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    if hasattr(user_obj, "masteruser"):
        if not user_obj.masteruser.invitation_key:
            key = get_activation_key(user_obj.masteruser.auth_user, "download")
            user_obj.masteruser.invitation_key = key
            user_obj.masteruser.save()
        else:
            try:
                key = user_obj.masteruser.invitation_key
                username, email = validate_activation_key(key, "download")

            except signing.BadSignature:
                key = get_activation_key(user_obj.masteruser.auth_user, "download")

        context = get_invitation_to_download_email_context(key)
        context.update({
            'user': user_obj
        })

        invitation_email_body_template = 'registration/invitation_to_download_email.txt'
        invitation_email_subject_template = 'registration/invitation_to_download_email_subject.txt'

        subject = render_to_string(invitation_email_subject_template,
                                   context)
        # Force subject to a single line to avoid header-injection
        # issues.
        subject = ''.join(subject.splitlines())
        message = render_to_string(invitation_email_body_template,
                                   context)
        user_obj.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
    else:
        raise Exception("User does not have MasterUser linked to it.")


def send_request_additional_license_email(user, trial_count, full_count):
    """
    Send the request for additional license email.

    """
    context = {'user': user,
            'schooluser': '',
            'trial_count': trial_count,
            'full_count':full_count}
    if hasattr(user, "schooluser") and user.schooluser:
        context.update({
            'schooluser': user.schooluser
        })

    subject = render_to_string('users/emails/additional_license_email_subject.txt',
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string('users/emails/additional_license_email.txt',
                               context)
    user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

    subject_to_system = render_to_string('users/emails/additional_license_email_subject_to_system.txt',
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject_to_system = ''.join(subject.splitlines())
    message_to_system = render_to_string('users/emails/additional_license_email_to_system.txt',
                               context)

    mail_admins(subject_to_system, message_to_system, fail_silently=False)



def send_request_program_extension_email(user, day_count):
    """
    Send the request for program extension email.

    """
    context = {'user': user,
            'schooluser': '',
            'day_count': day_count}
    if hasattr(user, "schooluser") and user.schooluser:
        context.update({
            'schooluser': user.schooluser
        })

    subject = render_to_string('users/emails/program_extension_email_subject.txt',
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string('users/emails/program_extension_email.txt',
                               context)
    user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

    subject_to_system = render_to_string('users/emails/program_extension_email_subject_to_system.txt',
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject_to_system = ''.join(subject.splitlines())
    message_to_system = render_to_string('users/emails/program_extension_email_to_system.txt',
                               context)

    mail_admins(subject_to_system, message_to_system, fail_silently=False)


def send_request_trial_email_to_system(schooluser):
    """
    Send the activation email. The activation key is simply the
    username, signed using TimestampSigner.

    """
    context = {'schooluser': schooluser}

    subject = render_to_string('users/emails/request_trial_email_subject_to_system.txt',
                               context)
    # Force subject to a single line to avoid header-injection
    # issues.
    subject = ''.join(subject.splitlines())
    message = render_to_string('users/emails/request_trial_email_to_system.txt',
                               context)
    mail_admins(subject, message, fail_silently=False)
