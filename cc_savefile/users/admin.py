from django.contrib import admin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy as reverse
from django.utils.html import format_html

import nested_admin
from .models import MasterUser, SchoolUser, AccountPackage, DemoLicenseRequest, FreeFullLicenseRequest
from .base_models import Position, SchoolType, Curriculum

class DemoLicenseRequestInline(nested_admin.NestedTabularInline):
    model = DemoLicenseRequest
    extra = 0

class FreeFullLicenseRequestInline(nested_admin.NestedTabularInline):
    model = FreeFullLicenseRequest
    extra = 0


class MasterUserAdmin(nested_admin.NestedModelAdmin):
    list_display = ("auth_user", 'user_id', 'email', "is_expired_account", 'is_trial', 'schooluser', 'created', 'modified')
    list_filter = ["is_expired_account", "is_trial", "schooluser"]
    list_editable = ["is_expired_account", "is_trial"]
    search_fields = ["username", "user_id", "email", "schooluser__school", "apple_id", "googleplay_id",
                    "steam_id", "filament_id", "teachergaming_id", "full_name", ]


class SchoolUserAdmin(nested_admin.NestedModelAdmin):
    list_display = ('full_name', "school", 'email', 'country', 'auth_user_link', 'get_is_active', 'created', 'modified')
    search_fields = ["school", "full_name", "email",]
    # list_display_link = [full_name, auth_user_link]
    inlines = [DemoLicenseRequestInline, FreeFullLicenseRequestInline]

    fieldsets = (
            ('User Info', {
              'fields': ('full_name', 'email', 'school', 'position_fk', 'state', 'country', 'timezone', )
            }),
            ('School Info', {
              'fields': ('school_type', 'curriculums', 'school_id', )
            }),
            ('Subscription Info', {
              'fields': ('total_students_num',
                # 'requested_trial_accounts_num',
                # 'trial_accounts_num',
                # 'trial_start_date', 'trial_end_date',
                # 'requested_full_accounts_num',
                # 'full_accounts_num',
                # 'full_start_date', 'full_end_date',
                # 'status', 'is_approved',
                # 'start_date', 'end_date',
                'game_download_url', 'android_download_url',
                )
            }),
        )

    def get_is_active(self, obj):
        if obj.auth_user:
            return obj.auth_user.is_active
        else:
            return False

    get_is_active.admin_order_field  = 'auth_user__is_active'  #Allows column order sorting
    get_is_active.short_description = 'Is Activated'  #Renames column head

    def auth_user_link(self, obj):
        if obj.auth_user:
            url = reverse('admin:auth_user_change', args=(obj.auth_user.id,))
            print url
            return format_html("<a href='{}'>{}</a>", url, obj.auth_user)
        else:
            return '-'

    auth_user_link.admin_order_field = 'auth_user'
    auth_user_link.short_description = 'User'


class DemoLicenseRequestAdmin(nested_admin.NestedModelAdmin):
    list_display = ("demo_schooluser", 'demo_accounts_num', 'demo_start_date', "demo_end_date", 'demo_status', 'is_generated', 'created', 'modified')
    list_filter = ["demo_schooluser",]
    search_fields = ["demo_schooluser__full_name", "demo_schooluser__email", "demo_schooluser__school" ]


class FreeFullLicenseRequestAdmin(nested_admin.NestedModelAdmin):
    list_display = ("freefull_schooluser", 'freefull_accounts_num', 'freefull_start_date', "freefull_end_date", 'freefull_status', 'is_generated', 'created', 'modified')
    list_filter = ["freefull_schooluser",]
    search_fields = ["freefull_schooluser__full_name", "freefull_schooluser__email", "freefull_schooluser__school" ]



admin.site.register(MasterUser, MasterUserAdmin)
admin.site.register(SchoolUser, SchoolUserAdmin)
admin.site.register(DemoLicenseRequest, DemoLicenseRequestAdmin)
admin.site.register(FreeFullLicenseRequest, FreeFullLicenseRequestAdmin)
admin.site.register(Position)
admin.site.register(SchoolType)
admin.site.register(Curriculum)
# admin.site.register(AccountPackage, AccountPackageAdmin)

