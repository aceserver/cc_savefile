import json
import csv
import codecs
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (authenticate, get_user_model, password_validation)
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.forms.widgets import HiddenInput, CheckboxSelectMultiple, DateInput, TimeInput
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import localtime, now
from django_countries.widgets import CountrySelectWidget
from django_select2.forms import Select2Widget
from form_utils.forms import BetterModelForm, BetterForm
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox
# from nocaptcha_recaptcha.fields import NoReCaptchaField
# from nocaptcha_recaptcha.widgets import NoReCaptchaWidget
from usernames import is_safe_username
from cc_savefile.widgets import DateTimePicker
from .models import *
from .utils import send_license_activation_email, send_license_set_username_email, send_license_set_username_success_email

def convert_to_choices(queryset):
    return_list = []
    for i in queryset:
        list_obj = (i.id, "%s" % i)
        return_list.append(list_obj)
    return return_list


class SignUpPilotForm(BetterModelForm):
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={"placeholder": "Please enter a password. Must be at least 8 alphanumeric characters"}),
        strip=False,
        required=True
    )
    captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox)

    class Meta:
        model = SchoolUser
        exclude = ('auth_user', "account_package",)
        fields = ['full_name', 'email', 'password', 'captcha']

    def __init__(self, *args, **kwargs):
        super(SignUpPilotForm, self).__init__(*args, **kwargs)
        self.fields['full_name'].required=True
        self.fields['full_name'].widget.attrs['placeholder'] = "Full Name (as per I.C.)"
        self.fields['email'].required=True
        self.fields['email'].widget.attrs['placeholder'] = "Please enter your email address"

    def clean_password(self):
        password = self.cleaned_data.get('password')
        password_validation.validate_password(password)
        return password

    def clean_email(self):
        email = self.cleaned_data.get('email')
        exist_schooluser = SchoolUser.objects.filter(email=email)
        if exist_schooluser:
            raise forms.ValidationError("This email belongs to another account. Please login instead if the account belongs to you.")
        return email

    def save(self, commit=True):
        schooluser = super(SignUpPilotForm, self).save()
        if commit:
            password = self.cleaned_data["password"]
            schooluser.auth_user.set_password(password)
            schooluser.auth_user.save()
            print "saved password as %s" % password
        return schooluser



class RequestFreeTrialForm(BetterModelForm):
    def __init__(self, *args, **kwargs):
        super(RequestFreeTrialForm, self).__init__(*args, **kwargs)
        self.fields['full_name'].required = True
        self.fields['full_name'].widget.attrs['placeholder'] = "Full name as per I.C."

        self.fields['position_fk'].required  = True
        self.fields['position_fk'].widget.attrs['placeholder'] = "Select your position"

        self.fields['school'].required    = True
        self.fields['school'].widget.attrs['placeholder'] = "Enter school name here"

        self.fields['school_type'].required    = True
        self.fields['school_type'].widget.attrs['placeholder'] = "Select one"

        self.fields['country'].required  = True
        self.fields['country'].widget.attrs['placeholder'] = "Select one"

        self.fields['state'].required     = True
        self.fields['state'].widget.attrs['placeholder'] = "Enter your state here"

        self.fields['total_students_num'].required   = True
        self.fields['total_students_num'].widget.attrs['placeholder'] = "Number of students in your school"

        self.fields['curriculums'].queryset  = Curriculum.objects.all()
        self.fields['curriculums'].widget    = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['curriculums'].queryset))
        self.fields['curriculums'].required  = True

        # print self.initial.get('full_name', None)
        # print self.initial.get('curriculums', None)
        if self.initial.get('full_name', None):
            self.fields['full_name'].widget.attrs['readonly'] = True

        if self.initial.get('position_fk', None):
            self.fields['position_fk'].widget.attrs['readonly'] = True

        if self.initial.get('school', None):
            self.fields['school'].widget.attrs['readonly'] = True

        if self.initial.get('school_type', None):
            self.fields['school_type'].widget.attrs['readonly'] = True

        if self.initial.get('country', None):
            self.fields['country'].widget.attrs['readonly'] = True

        if self.initial.get('state', None):
            self.fields['state'].widget.attrs['readonly'] = True

        if self.initial.get('total_students_num', None):
            self.fields['total_students_num'].widget.attrs['readonly'] = True

        if self.initial.get('curriculums', None):
            self.fields['curriculums'].widget.attrs['readonly'] = True

    class Meta:
        model = SchoolUser
        exclude = ('auth_user', "account_package",)
        fields = ['full_name', 'position_fk', 'school', 'school_type',
            'curriculums', 'country', 'state', 'total_students_num',
            ]
        widgets = {'country': CountrySelectWidget,
                    'school_type': Select2Widget,
                    'position_fk': Select2Widget,

                    }


class RequestMoreFreeFullForm(BetterModelForm):
    def __init__(self, *args, **kwargs):
        super(RequestMoreFreeFullForm, self).__init__(*args, **kwargs)
        self.fields['freefull_accounts_num'].required=False
        self.fields['freefull_schooluser'].required=False

    freefull_start_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS,
        widget=DateTimePicker(options={"format": "DD/MM/YYYY", "minDate":localtime(now())}, div_attrs={"class": ""}),
        required=False)

    class Meta:
        model = FreeFullLicenseRequest
        exclude = ['freefull_end_date', 'freefull_status']
        fields = ['freefull_schooluser', 'freefull_accounts_num', 'freefull_start_date']
        widgets = {'freefull_schooluser': forms.HiddenInput()}

    def clean(self):
        freefull_accounts_num = self.cleaned_data.get('freefull_accounts_num')
        freefull_start_date = self.cleaned_data.get('freefull_start_date')
        if freefull_accounts_num and not freefull_start_date:
            self.add_error('freefull_start_date', 'This field is required')
        if freefull_start_date and not freefull_accounts_num:
            self.add_error('freefull_accounts_num', 'This field is required')

        return self.cleaned_data


class RequestMoreDemoForm(BetterModelForm):
    def __init__(self, *args, **kwargs):
        super(RequestMoreDemoForm, self).__init__(*args, **kwargs)
        self.fields['demo_accounts_num'].required=False
        self.fields['demo_schooluser'].required=False

    demo_start_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS,
        widget=DateTimePicker(options={"format": "DD/MM/YYYY", "minDate":localtime(now())}, div_attrs={"class": ""}),
        required=False)

    class Meta:
        model = DemoLicenseRequest
        exclude = ['demo_end_date', 'demo_status']
        fields = ['demo_schooluser', 'demo_accounts_num', 'demo_start_date']
        widgets = {'demo_schooluser': forms.HiddenInput()}

    def clean(self):
        demo_accounts_num = self.cleaned_data.get('demo_accounts_num')
        demo_start_date = self.cleaned_data.get('demo_start_date')
        if demo_accounts_num and not demo_start_date:
            self.add_error('demo_start_date', 'This field is required')
        if demo_start_date and not demo_accounts_num:
            self.add_error('demo_accounts_num', 'This field is required')

        return self.cleaned_data


class UploadCsvForm(forms.Form):
    csv_file = forms.FileField(label=_("CSV File"), required=True)
    schooluser = forms.IntegerField(label=_("School User ID"), widget=HiddenInput(), required=True)
    # is_trial = forms.BooleanField(label=_("Is Trial"), widget=HiddenInput(), required=False)

    error_message = {'empty': _('Please select a CSV file.'),}

    def clean_csv_file(self):
        data = self.cleaned_data.get('csv_file')
        if not data:
            raise forms.ValidationError(self.error_message['empty'])
        return data

    def clean(self):
        csvfile = self.cleaned_data.get('csv_file')
        schooluser_id = self.cleaned_data.get('schooluser')
        schooluser = SchoolUser.objects.get(id=schooluser_id)

        if csvfile:
            dialect = csv.Sniffer().sniff(codecs.EncodedFile(csvfile, "utf-8").readline())
            csvfile.open()
            reader = csv.reader(codecs.EncodedFile(csvfile, "utf-8"), delimiter=',', dialect=dialect)

            errors = []
            field_name_list = []
            for i,row in enumerate(reader):
                if (i == 1):
                    serial_no_field_name  = row[0]
                    user_id_field_name    = row[1]
                    full_name_field_name  = row[2]
                    username_field_name   = row[3]
                    email_field_name      = row[4]

                elif i >= 2:
                    serial_no  = row[0]
                    user_id    = row[1]
                    full_name  = row[2]
                    username   = row[3]
                    email      = row[4]

                    # validate full name, username and email upon import
                    if full_name != "":
                        split_name_list = full_name.split()
                        if len(split_name_list) <= 1:
                            if not is_safe_username(full_name.lower()):
                                self.add_error('csv_file', '%s - This name is not allowed.' % full_name)
                    else:
                        full_name = None

                    if username != "":
                        if not is_safe_username(username.lower()):
                            self.add_error('csv_file', '%s - This username is not allowed.' % username)
                    else:
                        username = None

                    if email != "":
                        try:
                            validate_email(email)
                        except ValidationError:
                            self.add_error('csv_file', '%s - This email is not allowed.' % email)
                    else:
                        email = None

                    exist_masteruser = None
                    if user_id != "":
                        exist_masteruser = MasterUser.objects.filter(user_id=user_id).first()

                    # print exist_masteruser
                    if exist_masteruser and exist_masteruser.auth_user.is_active:
                        if exist_masteruser.user_id != user_id or exist_masteruser.full_name != full_name or exist_masteruser.username != username or exist_masteruser.email != email:
                            self.add_error('csv_file', "%s %s cannot be updated. License is already activated." % (serial_no, user_id) )
                    elif exist_masteruser and not exist_masteruser.auth_user.is_active:
                        # if no validation errors, you can update the MasterUser fields
                        if not errors:
                            can_save = False
                            if full_name:
                                exist_masteruser.full_name = full_name
                                can_save = True
                            if username:
                                exist_masteruser.username = username
                                can_save = True
                            if email:
                                exist_masteruser.email = email
                                can_save = True

                            if can_save:
                                exist_masteruser.save()
                                if username and email and full_name:
                                    send_license_set_username_email(exist_masteruser)

                    elif not exist_masteruser:
                        self.add_error('csv_file', "License cannot be created with the info provided on row %s." % i)

            if errors:
                self._errors["csv_file"] = errors

        # print self._errors
        return self.cleaned_data


# Forms for School bulk license SchoolUser workflow
class RedeemLicenseForm(BetterForm):
    def __init__(self, *args, **kwargs):
        super(RedeemLicenseForm, self).__init__(*args, **kwargs)
        self.fields['redeem_code'].widget.attrs['placeholder'] = "Enter your Redeem Code here"
    redeem_code = forms.CharField(label=_("Redeem Code"),
                required=True)
    full_name = forms.CharField(label=_("Full Name"),
                required=True)
    username = forms.CharField(label=_("Username"),
                required=True)
    email = forms.EmailField(label=_("Email"),
                required=True)

    error_message = {'empty_username': _('Please key in the username.'),
                    'unsafe_username': _('This username is not allowed.'),
                    'exist_username': _('This username already exists.'),
                    'empty_full_name': _('Please key your full name.'),
                    'unsafe_full_name': _('This name is not allowed.'),
                    'empty_email': _('Please key in the email.'),
                    'exist_email': _('This email already exists.'),
                    'redeem_code_activated': _('This License has already been activated. Please use another Redeem Code.'),
                    }

    class Meta:
        row_attrs = {
            'username': {'class': 'hide required'},
            'email': {'class': 'hide required'},
            'full_name': {'class': 'hide required'},
            }

    def clean_username(self):
        data = self.cleaned_data.get('username')
        if not data:
            raise forms.ValidationError(self.error_message['empty_username'])
        else:
            if not is_safe_username(data):
                raise forms.ValidationError(self.error_message['unsafe_username'])

        return data

    def clean_full_name(self):
        data = self.cleaned_data.get('full_name')

        if not data:
            raise forms.ValidationError(self.error_message['empty_full_name'])
        else:
            split_name_list = data.split()
            if len(split_name_list) <= 1:
                if not is_safe_username(data.lower()):
                    raise forms.ValidationError(self.error_message['unsafe_full_name'])

        return data


    def clean(self):
        cleaned_data = super(RedeemLicenseForm, self).clean()
        redeem_code = cleaned_data.get("redeem_code")
        full_name = cleaned_data.get("full_name")
        username = cleaned_data.get("username")
        email = cleaned_data.get("email")
        # print email
        if redeem_code and username and email and full_name:
            masteruser = MasterUser.objects.get(user_id=redeem_code)
            if masteruser.auth_user.is_active:
                # if auth_user is already active, this means this
                # MasterUser user_id has already been redeemed/activated
                self._errors['redeem_code'] = [self.error_message['redeem_code_activated']]
            else:
                # crosscheck username with other MasterUsers
                exist_username = MasterUser.objects.filter(username=username).exclude(id=masteruser.id)
                if exist_username:
                    self._errors['username'] = [self.error_message['exist_username']]

                # crosscheck email with other MasterUsers
                exist_email = MasterUser.objects.filter(email=email).exclude(id=masteruser.id)
                if exist_email:
                    self._errors['email'] = [self.error_message['exist_email']]

                # crosscheck email with SchoolUsers only
                exist_school_email = SchoolUser.objects.filter(email=email)
                if exist_school_email:
                    self._errors['email'] = [self.error_message['exist_email']]

                # print "errors", self._errors
                if not self._errors:
                    masteruser.username = username
                    masteruser.email = email
                    masteruser.full_name = full_name
                    masteruser.save()
                    send_license_activation_email(masteruser)

                # crosscheck username with those MasterUsers with SchoolUsers only (DEPRECEATED)
                # exist_username = MasterUser.objects.filter(username=username, schooluser__isnull=False).exclude(id=masteruser.id)
                # crosscheck email with those MasterUsers with SchoolUsers only (DEPRECEATED)
                # exist_email = MasterUser.objects.filter(email=email, schooluser__isnull=False).exclude(id=masteruser.id)
        return cleaned_data


# Forms for Online Registered SchoolUser workflow
class InviteLicenseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(InviteLicenseForm, self).__init__(*args, **kwargs)
        self.fields['full_name'].widget.attrs['placeholder'] = "Full Name (as per I.C.)"
        self.fields['email'].widget.attrs['placeholder'] = "Email"

        self.fields['full_name'].required   = True
        self.fields['email'].required       = True
        self.fields['schooluser'].required  = True
        self.fields['is_trial'].required    = False

    error_message = {'empty_full_name': _('Please key in the full name.'),
                    'unsafe_full_name': _('This name is not allowed.'),
                    'empty_email': _('Please key in the email.'),
                    'exist_email': _('This email is already in use. Please use another email.'),
                    'limit': _('License limit reached. Please request additional licenses by contacting us.')}

    class Meta:
        model = MasterUser
        exclude = ('auth_user', 'trial_start_date', 'trial_end_date', 'username')
        fields = ("full_name", "email", "role", "schooluser", "is_trial", 'user_id',)
        widgets = {'schooluser': HiddenInput(),
                    'is_trial': HiddenInput(),
                    'user_id': HiddenInput(),
                    'role': forms.RadioSelect(),
                    }

    def clean_full_name(self):
        data = self.cleaned_data.get('full_name')
        if not data:
            raise forms.ValidationError(self.error_message['empty_full_name'])
        else:
            split_name_list = data.split()
            if len(split_name_list) <= 1:
                if not is_safe_username(data.lower()):
                    raise forms.ValidationError(self.error_message['unsafe_full_name'])

        return data


    def clean_email(self):
        data = self.cleaned_data.get('email')
        if not data:
            raise forms.ValidationError(self.error_message['empty_email'])
        else:
            exist_email = MasterUser.objects.filter(email=data)
            if exist_email:
                raise forms.ValidationError(self.error_message['exist_email'])

            # exist_school_email = SchoolUser.objects.filter(email=data)
            # if exist_school_email:
            #     raise forms.ValidationError(self.error_message['exist_email'])
        return data

    def clean(self):
        schooluser = self.cleaned_data.get('schooluser')
        is_trial = self.cleaned_data.get('is_trial')
        full_name = self.cleaned_data.get('full_name')
        email = self.cleaned_data.get('email')
        user_id = self.cleaned_data.get('user_id')

        if is_trial:
            accounts_left = schooluser.get_trial_accounts().filter(username__isnull=True)
        else:
            accounts_left = schooluser.get_full_accounts().filter(Q(username__isnull=True) | Q(username=""))
        print accounts_left
        if accounts_left.count() <= 0:
            raise forms.ValidationError(self.error_message['limit'])
        else:
            if not self._errors:
                if user_id:
                    license = MasterUser.objects.get(user_id=user_id)
                else:
                    license = accounts_left.first()
                license.full_name = full_name
                license.email = email
                license.save()
                send_license_set_username_email(license)
        return self.cleaned_data


class LicenseSetUsernameForm(BetterForm):
    def __init__(self, masteruser, *args, **kwargs):
        self.masteruser = masteruser
        # print masteruser
        # print kwargs
        super(LicenseSetUsernameForm, self).__init__(*args, **kwargs)
        if self.masteruser:
            self.initial["username"] = self.masteruser.username

    username = forms.CharField(label=_("Username"),
                required=True)

    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={"placeholder": "Please enter a password. Must be at least 8 alphanumeric characters"}),
        strip=False,
        required=True
    )

    error_message = {'empty_username': _('Please key in the username.'),
                    'unsafe_username': _('This username is not allowed.'),
                    'exist_username': _('This username already exists.'),
                    'empty_full_name': _('Please key your full name.'),
                    'unsafe_full_name': _('This name is not allowed.'),
                    'license_activated': _('This License has already been activated.'),
                    }

    def clean_username(self):
        data = self.cleaned_data.get('username')
        if not data:
            raise forms.ValidationError(self.error_message['empty_username'])
        else:
            if not is_safe_username(data):
                raise forms.ValidationError(self.error_message['unsafe_username'])

        return data

    def clean_password(self):
        password = self.cleaned_data.get('password')
        password_validation.validate_password(password)
        return password

    def clean(self):
        cleaned_data = super(LicenseSetUsernameForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        masteruser = self.masteruser

        if username:
            if masteruser.auth_user.is_active:
                # if auth_user is already active, this means this
                # MasterUser user_id has already been redeemed/activated
                self._errors['username'] = [self.error_message['license_activated']]
            else:
                # crosscheck MasterUser username globally
                exist_username = MasterUser.objects.filter(username=username).exclude(id=masteruser.id)
                if exist_username:
                    self._errors['username'] = [self.error_message['exist_username']]

                print "errors", self._errors
                if not self._errors:
                    masteruser.username = username
                    masteruser.save()
                    masteruser.auth_user.is_active = True
                    masteruser.auth_user.set_password(password)
                    print "saved password as %s" % password
                    masteruser.auth_user.save()
                    send_license_set_username_success_email(masteruser)

        return cleaned_data


# ====== Account Password Form ======
class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput,
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        if commit:
            self.user.save()
            print "saved password as %s" % password
        return self.user


class ResendActivationEmailForm(forms.Form):
    """
    A form that lets a user resend the activation email
    to the email provided in the field
    """
    error_messages = {
        'validation_error': _("Please enter a valid email address."),
    }
    email = forms.EmailField(label=_("Email"))


class EmailAuthenticationForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super(EmailAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _('Email')
        self.fields['username'].widget.attrs['placeholder'] = 'Enter your email address'
        self.fields['password'].widget.attrs['placeholder'] = 'Enter your password'

