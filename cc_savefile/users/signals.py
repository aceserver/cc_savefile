from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE
from rest_framework.authtoken.models import Token

__all__ = ["user_postsave", "login_user", "logout_user"]


def user_postsave(sender, instance, created, **kwargs):
    from .models import MasterUser

    if created and instance:
        Token.objects.get_or_create(user=instance)
        # MasterUser.objects.get_or_create(auth_user=instance)

    if not created and instance:
        if hasattr(instance, 'masteruser'):
            if instance.masteruser and instance.email and instance.email != instance.masteruser.email:
                instance.masteruser.save()

        if hasattr(instance, 'schooluser'):
            if instance.schooluser and instance.email and instance.email != instance.schooluser.email:
                instance.schooluser.save()

    if not created and not hasattr(instance, 'auth_token'):
        Token.objects.get_or_create(user=instance)



def login_user(sender, request, user, **kwargs):
    from .models import MasterUser
    if hasattr(user, 'masteruser'):
        if user.masteruser:
            user.masteruser.is_logged_in = True
            user.masteruser.save()
            user.masteruser.save_last_activity()

def logout_user(sender, request, user, **kwargs):
    from .models import MasterUser
    try:
        if hasattr(user, 'masteruser'):
            if user.masteruser:
                user.masteruser.is_logged_in = False
                user.masteruser.save()
    except Exception as e:
        print e


