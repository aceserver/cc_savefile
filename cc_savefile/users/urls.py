# TEMPLATE URLS
from django.conf.urls import include, url
from django.conf.urls import url
from .views import (
    RegisterView,
    RegisterThankYouView,
    RequestTrialView,
    SignupLoginView,
    SchoolUserManagementView,
    SchoolUserDetailView,
    SchoolUserUpdateProfileView,

    RedeemLicenseView,
    RedeemLicenseThankYouView,
    ActivateLicenseView,
    InviteLicenseView,
    CancelInvitationView,
    LicenseSetUsernameView,
    LicenseSetUsernameThankYouView,

    RequestMoreFreeFullView,
    RequestMoreDemoView,
    send_invite_license_email,

    UploadCsvView,
    license_import_template_export,
    quiz_sittings_export,
    generate_licenses,
    export_licenses_as_csv,
    gamesession_csv_export,

    trialaccount_send_activation_email,
    trialaccount_activate, trialaccount_deactivate,
    trialaccount_sendinvitation, trialaccount_getprogress,
    TrialAccountReportView,

    fullaccount_send_activation_email,
    fullaccount_deactivate, fullaccount_activate,
    fullaccount_sendinvitation,

    test_send_email
    )
from .utils import resend_activation_email
from .account_views import (PasswordResetBeforeActivationView,
                        ResendActivationEmailView,
                        ResendActivationEmailSuccessView,)
from .ajax import check_redeem_code

urlpatterns = [
    url(r'^test_send_email/$',
        test_send_email,
        name='test_send_email'),

    url(r'^register/$',
        RegisterView.as_view(),
        name='register_form'),

    url(r'^thank-you/$',
        RegisterThankYouView.as_view(),
        name='register_thank_you'),

    url(r'^request-trial/$',
        RequestTrialView.as_view(),
        name='request_trial'),

    url(r'^signup-login/$',
        SignupLoginView.as_view(),
        name='signup_login'),

    url(r'^users/ajax/check-redeem-code/$',
        check_redeem_code,
        name="users_ajax_check_redeem_code"),

    url(r'^users/resend-activation-email/$',
        resend_activation_email,
        name="users_resend_activation_email"),

    url(r'^users/resend-activation-email-form/$',
        ResendActivationEmailView.as_view(),
        name="users_resend_activation_email_form"),

    url(r'^users/resend-activation-email-success/(?P<mode>.*)/$',
        ResendActivationEmailSuccessView.as_view(),
        name="users_resend_activation_email_success"),

    url(r'^users/set-password-before-activate/(?P<activation_key>[-:\w]+)/$',
        PasswordResetBeforeActivationView.as_view(),
        name="set_password_before_activate"),

    url(r'^users/management/$',
        SchoolUserManagementView.as_view(),
        name="schooluser_management"),

    url(r'^users/account/$',
        SchoolUserDetailView.as_view(),
        name="schooluser_detail"),

    url(r'^users/account/update/$',
        SchoolUserUpdateProfileView.as_view(),
        name="schooluser_update"),

    url(r'^users/upload-csv/$',
        UploadCsvView.as_view(),
        name="import_account_from_csv"),

    url(r'^users/send-invite/',
        send_invite_license_email,
        name="send_invite_license_email"),

    url(r'^users/export-license-template/',
        license_import_template_export,
        name="license_import_template_export"),

    url(r'^users/generate-licenses/(?P<account_type>.*)/',
        generate_licenses,
        name="generate_licenses"),

    url(r'^users/export-licenses-as-csv/',
        export_licenses_as_csv,
        name="export_licenses_as_csv"),

    url(r'^users/export-quiz-sittings/',
        quiz_sittings_export,
        name="quiz_sittings_export"),

    url(r'^users/export-gamesession-data/',
        gamesession_csv_export,
        name="gamesession_csv_export"),

    # Redeem
    url(r'^redeem/$',
        RedeemLicenseView.as_view(),
        name="license_redeem"),

    url(r'^redeem/success/$',
        RedeemLicenseThankYouView.as_view(),
        name="license_redeem_thankyou"),

    # License Activation
    url(r'^license/activate/(?P<activation_key>[-:\w]+)/$',
        ActivateLicenseView.as_view(),
        name="activate_license"),

    # url(r'^invite_license/$',
    #     InviteLicenseView.as_view(),
    #     name="invite_license"),

    url(r'^invite_license/(?P<user_id>.*)/$',
        InviteLicenseView.as_view(),
        name="invite_license"),

    url(r'^set-username/success/$',
        LicenseSetUsernameThankYouView.as_view(),
        name="license_set_username_thankyou"),

    url(r'^set-username/(?P<activation_key>[-:\w]+)/$',
        LicenseSetUsernameView.as_view(),
        name="license_set_username"),

    url(r'^cancel-invitation/$',
        CancelInvitationView.as_view(),
        name="cancel_invitation"),

    # Request more licenses
    url(r'^request-free-full/$',
        RequestMoreFreeFullView.as_view(),
        name="request_more_free_full"),

    url(r'^request-demo/$',
        RequestMoreDemoView.as_view(),
        name="request_more_demo"),


    # trial account urls
    url(r'^users/trial-send-activation-email/$',
        trialaccount_send_activation_email,
        name="trialaccount_send_activation_email"),

    url(r'^users/trial-deactivate/$',
        trialaccount_deactivate,
        name="trialaccount_deactivate"),

    url(r'^users/trial-activate/$',
        trialaccount_activate,
        name="trialaccount_activate"),

    url(r'^users/trial-send-invitation-to-download/$',
        trialaccount_sendinvitation,
        name="trialaccount_sendinvitation"),

    url(r'^users/trial-get-progress/$',
        trialaccount_getprogress,
        name="trialaccount_getprogress"),

    url(r'^users/demo-license-report/(?P<user_id>.*)/$',
        TrialAccountReportView.as_view(),
        name="trialaccount_report"),


    url(r'^users/full-send-activation-email/$',
        fullaccount_send_activation_email,
        name="fullaccount_send_activation_email"),

    url(r'^users/full-deactivate/$',
        fullaccount_deactivate,
        name="fullaccount_deactivate"),

    url(r'^users/full-activate/$',
        fullaccount_activate,
        name="fullaccount_activate"),

    url(r'^users/full-send-invitation-to-download/$',
        fullaccount_sendinvitation,
        name="fullaccount_sendinvitation"),
]


# API URLS
from rest_framework import routers
from .api.viewsets import MasterUserViewSet

router = routers.DefaultRouter()

router.register(r'masteruser', MasterUserViewSet)

urlpatterns += [
    url(r'^api/', include(router.urls)),
]