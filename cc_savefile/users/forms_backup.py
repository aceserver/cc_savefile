if settings.DEBUG:
    trial_form_fieldsets = [
        ('credentials', {'fields': ["first_name", "last_name",
                                "email", "school",
                                "schoolgroup", "position", "address1",
                                "address2", "postcode", "state", "country"],
                               'legend': _("")
                               }),
        ('school_info', {'fields': ["organisation_model", "level_of_edu",
                                "facilities", "extracurricular", "events",
                                "curriculums", "examinations", ],
                               'legend': _("")
                               }),
        ('trial_info', {'fields': ["total_students_num", "trial_students_num",
                                "total_teachers_num", "trial_teachers_num",
                                "intended_trial_start_date",
                                "intended_trial_allocated_time",
                                "source", "source_text", ],
                               'legend': _("")
                               }),
    ]
else:
    trial_form_fieldsets = [
        ('credentials', {'fields': ["captcha", "first_name", "last_name",
                                "email", "school",
                                "schoolgroup", "position", "address1",
                                "address2", "postcode", "state", "country"],
                               'legend': _("")
                               }),
        ('school_info', {'fields': ["organisation_model", "level_of_edu",
                                "facilities", "extracurricular", "events",
                                "curriculums", "examinations", ],
                               'legend': _("")
                               }),
        ('trial_info', {'fields': ["total_students_num", "trial_students_num",
                                "total_teachers_num", "trial_teachers_num",
                                "intended_trial_start_date",
                                "intended_trial_allocated_time",
                                "source", "source_text", ],
                               'legend': _("")
                               }),
    ]

class RegisterTrialForm(BetterModelForm):
    def __init__(self, *args, **kwargs):
        super(RegisterTrialForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['first_name'].label    = _("First Name")

        self.fields['last_name'].required = True
        self.fields['last_name'].label    = _("Last Name")
        self.fields['email'].required     = True
        self.fields['school'].required    = True
        self.fields['position'].required  = True
        self.fields['address1'].required  = True
        self.fields['postcode'].required  = True
        self.fields['state'].required     = True
        self.fields['country'].required   = True

        self.fields['level_of_edu'].queryset    = LevelOfEducation.objects.all()
        self.fields['facilities'].queryset      = Facility.objects.all()
        self.fields['extracurricular'].queryset = ExtraCurricular.objects.all()
        self.fields['events'].queryset          = Event.objects.all()
        self.fields['curriculums'].queryset     = Curriculum.objects.all()
        self.fields['examinations'].queryset    = Examination.objects.all()

        self.fields['level_of_edu'].widget    = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['level_of_edu'].queryset))
        self.fields['facilities'].widget      = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['facilities'].queryset))
        self.fields['extracurricular'].widget = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['extracurricular'].queryset))
        self.fields['events'].widget          = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['events'].queryset))
        self.fields['curriculums'].widget     = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['curriculums'].queryset))
        self.fields['examinations'].widget    = CheckboxSelectMultiple(choices=convert_to_choices(self.fields['examinations'].queryset))

        self.fields['organisation_model'].required = True
        self.fields['level_of_edu'].required       = True
        self.fields['curriculums'].required        = True
        self.fields['examinations'].required       = True


        self.fields['total_students_num'].required = True
        self.fields['trial_students_num'].required = True
        self.fields['total_teachers_num'].required = True
        self.fields['trial_teachers_num'].required = True
        self.fields['intended_trial_start_date'].required     = True
        self.fields['intended_trial_allocated_time'].required = True
        self.fields['source'].required = True



    error_message = {'email_trial': _('You have already requested for a trial using this email.'),
                    'email_user': _('This email is already in use.'),
                    'empty': _('Please fill in this field.'),
                    }

    intended_trial_start_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS,
        widget=DateTimePicker(options={"format": "DD/MM/YYYY", "minDate":localtime(now())}, div_attrs={"class": ""})
        )

    if not settings.DEBUG:
        captcha = ReCaptchaField()
    # captcha = NoReCaptchaField(
    #         gtag_attrs={
    #             'callback': 'onSubmit',  # name of JavaScript callback function
    #             'bind': 'submit-button'  # submit button's ID in the form template
    #         },
    #         widget=NoReCaptchaWidget
    #     )


    class Meta:
        model = SchoolUser
        exclude = ('auth_user', "account_package",)
        fieldsets = trial_form_fieldsets
        widgets = {'country': CountrySelectWidget,
                    'organisation_model': Select2Widget,
                    'source': Select2Widget,

                    }

    def clean_email(self):
        data = self.cleaned_data['email']
        if data:
            schoolusers = SchoolUser.objects.filter(email=data)
            if self.instance.id and schoolusers:
                schoolusers = schoolusers.exclude(id=self.instance.id)

            if schoolusers:
                raise forms.ValidationError(self.error_message['email_trial'])

            users = User.objects.filter(email=data)
            if self.instance.id and self.instance.auth_user and users:
                users = users.exclude(id=self.instance.auth_user.id)

            if users:
                raise forms.ValidationError(self.error_message['email_user'])

        return data



class MasterUserAccountCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MasterUserAccountCreateForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = "Username"
        self.fields['email'].widget.attrs['placeholder'] = "Email"

        self.fields['username'].required           = True
        self.fields['email'].required              = True
        self.fields['schooluser'].required         = False
        self.fields['is_trial'].required           = False
        self.fields['is_expired_account'].required = False

    error_message = {'empty_username': _('Please key in the username.'),
                    'unsafe_username': _('This username is not allowed.'),
                    'empty_email': _('Please key in the email.'),
                    'exist_email': _('This email is already in use. Please use another email.'),
                    'limit': _('License limit reached. Please request additional licenses by contacting us.')}

    class Meta:
        model = MasterUser
        exclude = ('auth_user', 'trial_start_date', 'trial_end_date',  'user_id')
        fields = ("username", "email", "schooluser", "is_trial", "is_expired_account")
        widgets = {'schooluser': HiddenInput(),
                    'is_trial': HiddenInput(),
                    'is_expired_account': HiddenInput(),
                    }


    def clean_username(self):
        data = self.cleaned_data.get('username')
        if not data:
            raise forms.ValidationError(self.error_message['empty_username'])
        else:
            if not is_safe_username(data):
                raise forms.ValidationError(self.error_message['unsafe_username'])

            # crosscheck username with those MasterUsers with SchoolUsers only
            exist_username = MasterUser.objects.filter(username=data, schooluser__isnull=False).exclude(id=self.instance.id)
            if exist_username:
                raise forms.ValidationError(self.error_message['exist_username'])

        return data


    def clean_email(self):
        data = self.cleaned_data.get('email')
        if not data:
            raise forms.ValidationError(self.error_message['empty_email'])
        else:
            exist_email = MasterUser.objects.filter(email=data)
            if exist_email:
                raise forms.ValidationError(self.error_message['exist_email'])

            exist_school_email = SchoolUser.objects.filter(email=data)
            if exist_school_email:
                raise forms.ValidationError(self.error_message['exist_email'])
        return data

    def clean(self):
        schooluser = self.cleaned_data.get('schooluser')
        is_trial = self.cleaned_data.get('is_trial')

        if is_trial:
            accounts_left = schooluser.trial_accounts_num - schooluser.get_trial_accounts().count()
        else:
            accounts_left = schooluser.full_accounts_num - schooluser.get_full_accounts().count()

        if accounts_left <= 0:
            raise forms.ValidationError(self.error_message['limit'])
        else:
            return self.cleaned_data



class LicenseUpdateForm(forms.ModelForm):
    '''License Update Form to set username and email inside SchoolUser management view'''
    def __init__(self, *args, **kwargs):
        super(LicenseUpdateForm, self).__init__(*args, **kwargs)
        self.fields['username'].required           = True
        self.fields['email'].required              = True

    error_message = {'empty_username': _('Please key in the username.'),
                    'unsafe_username': _('This username is not allowed.'),
                    'empty_email': _('Please key in the email.'),
                    'exist_username': _('This username already exists.'),
                    'exist_email': _('This email already exists.'),
                    }

    class Meta:
        model = MasterUser
        exclude = ('auth_user', 'trial_start_date', 'trial_end_date',  'user_id')
        fields = ("username", "email",)


    def clean_username(self):
        data = self.cleaned_data.get('username')
        if not data:
            raise forms.ValidationError(self.error_message['empty_username'])
        else:
            if not is_safe_username(data):
                raise forms.ValidationError(self.error_message['unsafe_username'])

            # crosscheck username with those MasterUsers with SchoolUsers only
            exist_username = MasterUser.objects.filter(username=data, schooluser__isnull=False).exclude(id=self.instance.id)
            if exist_username:
                raise forms.ValidationError(self.error_message['exist_username'])

        return data

    def clean_email(self):
        data = self.cleaned_data.get('email')
        if not data:
            raise forms.ValidationError(self.error_message['empty_email'])
        else:
            exist_email = MasterUser.objects.filter(email=data, schooluser__isnull=False).exclude(id=self.instance.id)
            if exist_email:
                raise forms.ValidationError(self.error_message['exist_email'])
            exist_school_email = SchoolUser.objects.filter(email=data)
            if exist_school_email:
                raise forms.ValidationError(self.error_message['exist_email'])

        return data


class RequestAdditionalLicenseForm(forms.Form):
    trial_count = forms.IntegerField(label=_("Trial Licenses"),
                help_text=_("Number of additional Trial Licenses"),
                required=False)
    full_count = forms.IntegerField(label=_("Full Access Licenses"),
                help_text=_("Number of additional Full Access Licenses"),
                required=False)

    def clean(self):
        trial_count = self.cleaned_data.get("trial_count")
        full_count = self.cleaned_data.get("full_count")
        if not trial_count and not full_count:
            self._errors['trial_count'] = [_("Please enter a number for Trial Licenses or Full Access Licenses.")]
            self._errors['full_count'] = [_("Please enter a number for Trial Licenses or Full Access Licenses.")]
        return self.cleaned_data

class RequestProgramExtensionForm(forms.Form):
    day_count = forms.IntegerField(label=_("Number of days to extend"), required=True)
