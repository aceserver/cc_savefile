from __future__ import unicode_literals
import datetime
import pytz
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from django_countries.fields import CountryField

class Position(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class SchoolType(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class LevelOfEducation(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class Facility(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title


class ExtraCurricular(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class Event(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class Curriculum(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title

class Examination(TimeStampedModel):
    title = models.CharField(_('Name'), max_length=300, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.title