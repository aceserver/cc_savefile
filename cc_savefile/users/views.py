import datetime
import pytz
import json
from dateutil.relativedelta import relativedelta
from django.db.models import Q
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core import signing
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy as reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.middleware import csrf
from django.shortcuts import render
from django.template import loader, RequestContext
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.views.generic import CreateView, TemplateView, DetailView
from django.views.generic.edit import FormView, UpdateView
from django.forms.models import modelformset_factory

from cc_savefile.utils import json_response

from gamesaves.models import Platform, Game

from users.forms import SignUpPilotForm, EmailAuthenticationForm

from .account_views import PasswordResetBeforeActivationView
from .models import (MasterUser, SchoolUser, AccountPackage,
    DemoLicenseRequest, FreeFullLicenseRequest)
from .forms import (SignUpPilotForm, RequestFreeTrialForm,
    UploadCsvForm, RedeemLicenseForm, LicenseSetUsernameForm,
    InviteLicenseForm, RequestMoreFreeFullForm, RequestMoreDemoForm)
from .utils import (send_activation_email,
    send_invitation_to_download_email,
    send_request_additional_license_email,
    send_request_program_extension_email,
    send_request_trial_email_to_system,
    validate_activation_key,
    send_license_set_username_email,
    send_cancel_invite_email)

def test_send_email(request):
    send_activation_email(request.user)
    return HttpResponseRedirect(reverse('register_thank_you'))

class RegisterView(CreateView):
    template_name = 'users/register_form.html'
    login_required = False
    form_class = SignUpPilotForm
    success_url = reverse('register_thank_you')


class RegisterThankYouView(TemplateView):
    '''
    Sign Up form success will redirect here
    Sign Up form view is in cc_savefile.views
    '''

    template_name = 'users/register_thankyou.html'
    login_required = False


DemoLicenseRequestFormSet = modelformset_factory(DemoLicenseRequest,
                    form=RequestMoreDemoForm,
                    extra = 1, max_num=1)

FreeFullLicenseRequestFormSet = modelformset_factory(FreeFullLicenseRequest,
                    form=RequestMoreFreeFullForm,
                    extra = 1, max_num=1)


class RequestTrialView(UpdateView):
    '''
    After login successfully, will redirect here.
    Call to action to buy or sign up for free trial.
    '''
    model = SchoolUser
    form_class = RequestFreeTrialForm
    template_name = 'users/request_trial.html'
    login_required = True
    success_url = reverse('request_trial')

    def get_object(self, queryset=None):
        if hasattr(self.request.user, "schooluser") and self.request.user.schooluser:
            return self.request.user.schooluser
        else:
            raise Http404(_("You are not a School User. You cannot access this page."))

    def check_allow_apply_trial(self):
        schooluser = self.get_object()
        all_accounts_list = schooluser.masteruser_set.all()
        if all_accounts_list.filter(is_trial=False, is_free_full=False):
            # bought game before
            return False
        else:
            return True

    def get_context_data(self, **kwargs):
        context = super(RequestTrialView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            try:
                schooluser = self.get_object()
                context['schooluser'] = schooluser

                context["allow_apply_trial"] = self.check_allow_apply_trial()

                latest_freefull_request = schooluser.freefulllicenserequest_set.latest() if schooluser.freefulllicenserequest_set.all() else None
                latest_demo_request = schooluser.demolicenserequest_set.latest() if schooluser.demolicenserequest_set.all() else None
                if latest_freefull_request:
                    if latest_freefull_request.freefull_status == FreeFullLicenseRequest.FOLLOWUP:
                        context['allow_freefull_request'] = False
                    else:
                        context['allow_freefull_request'] = True
                else:
                    context['allow_freefull_request'] = True

                if latest_demo_request:
                    if latest_demo_request.demo_status == DemoLicenseRequest.FOLLOWUP:
                        context['allow_demo_request'] = False
                    else:
                        context['allow_demo_request'] = True
                else:
                    context['allow_demo_request'] = True
            except Exception, e:
                pass

        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if self.check_allow_apply_trial():
            formset_demo = DemoLicenseRequestFormSet(queryset=DemoLicenseRequest.objects.none())
            formset_freefull = FreeFullLicenseRequestFormSet(queryset=FreeFullLicenseRequest.objects.none())

            for formset in formset_demo:
                formset.initial={'demo_schooluser': self.object}

            for formset in formset_freefull:
                formset.initial={'freefull_schooluser': self.object}

            return self.render_to_response(self.get_context_data(form=form, formset_demo=formset_demo, formset_freefull=formset_freefull))
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if self.check_allow_apply_trial():
            formset_demo = DemoLicenseRequestFormSet(request.POST)
            formset_freefull = FreeFullLicenseRequestFormSet(request.POST)

            if (form.is_valid() and formset_demo.is_valid() and formset_freefull.is_valid()):
                return self.form_valid(form, formset_demo, formset_freefull)
            return self.form_invalid(form, formset_demo, formset_freefull)
        else:
            return self.form_invalid(form)

    def form_valid(self, form, formset_demo=None, formset_freefull=None):
        schooluser = form.save()
        if self.check_allow_apply_trial():
            requested = False
            for f in formset_demo:
                if f.cleaned_data.get('demo_accounts_num') and f.cleaned_data.get('demo_start_date'):
                    f.save()
                    requested = True
            for f in formset_freefull:
                if f.cleaned_data.get('freefull_accounts_num') and f.cleaned_data.get('freefull_start_date'):
                    f.save()
                    requested = True

            return HttpResponseRedirect(self.success_url + "#requestform")
        else:
            return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form, formset_demo=None, formset_freefull=None):
        print form.errors
        print formset_demo.errors
        print formset_freefull.errors
        return self.render_to_response(self.get_context_data(form=form, formset_demo=formset_demo, formset_freefull=formset_freefull))


class SignupLoginView(TemplateView):
    template_name = 'users/signup_login.html'
    login_required = False

    def get_template_names(self):
        if self.request.is_ajax():
            return ['users/includes/signup_login_modal.html']
        else:
            return [self.template_name]

    def get_context_data(self, **kwargs):
        context = super(SignupLoginView, self).get_context_data(**kwargs)
        context['signup_form'] = SignUpPilotForm()
        context['login_form'] = EmailAuthenticationForm()
        return context



# MANAGEMENT
class SchoolUserManagementView(DetailView):
    model = SchoolUser
    template_name = 'users/schooluser_management.html'
    login_required = True

    def get_object(self):
        """
        Returns the current user.
        """
        try:
            if self.request.user.is_authenticated():
                if self.request.user.is_superuser:
                    schooluser = self.request.GET.get('schooluser', '')
                    if schooluser:
                        schooluser_obj = SchoolUser.objects.filter(email=schooluser)
                        if schooluser_obj.first():
                            return schooluser_obj.first()
                        else:
                            raise PermissionDenied
                    else:
                        raise PermissionDenied
                else:
                    try:
                        current_user = self.request.user.schooluser
                        return current_user
                    except:
                        try:
                            current_user = self.request.user.masteruser
                            self.model = MasterUser
                            return current_user
                        except:
                            raise PermissionDenied
            else:
                raise PermissionDenied
        except PermissionDenied:
            raise PermissionDenied
        except Exception as e:
            raise Http404(_("%s" % e))

    def get_context_data(self, **kwargs):
        context = super(SchoolUserManagementView, self).get_context_data(**kwargs)
        if self.model == SchoolUser:
            # CSV form
            schooluser = self.get_object()
            full_initial_data = {'schooluser': schooluser.id}
            all_csv_form = UploadCsvForm(initial=full_initial_data)
            context['all_csv_form'] = all_csv_form

            latest_freefull_request = schooluser.freefulllicenserequest_set.latest() if schooluser.freefulllicenserequest_set.all() else None
            latest_demo_request = schooluser.demolicenserequest_set.latest() if schooluser.demolicenserequest_set.all() else None
            if latest_freefull_request:
                if latest_freefull_request.freefull_status == FreeFullLicenseRequest.FOLLOWUP:
                    context['allow_freefull_request'] = False
                else:
                    context['allow_freefull_request'] = True
                    request_freefull_initial_data = {'freefull_schooluser': schooluser.id}
                    request_freefull_form = RequestMoreFreeFullForm(initial=request_freefull_initial_data)
                    context['request_freefull_form'] = request_freefull_form
            else:
                context['allow_freefull_request'] = True
                request_freefull_initial_data = {'freefull_schooluser': schooluser.id}
                request_freefull_form = RequestMoreFreeFullForm(initial=request_freefull_initial_data)
                context['request_freefull_form'] = request_freefull_form

            if latest_demo_request:
                if latest_demo_request.demo_status == DemoLicenseRequest.FOLLOWUP:
                    context['allow_demo_request'] = False
                else:
                    context['allow_demo_request'] = True
                    request_demo_initial_data = {'demo_schooluser': schooluser.id}
                    request_demo_form = RequestMoreDemoForm(initial=request_demo_initial_data)
                    context['request_demo_form'] = request_demo_form
            else:
                context['allow_demo_request'] = True
                request_demo_initial_data = {'demo_schooluser': schooluser.id}
                request_demo_form = RequestMoreDemoForm(initial=request_demo_initial_data)
                context['request_demo_form'] = request_demo_form

            # Allow apply trial
            all_accounts_list = self.object.masteruser_set.all().order_by("is_trial", "-auth_user__is_active", "serial_no")

            if all_accounts_list.filter(is_trial=False, is_free_full=False):
                # bought game before
                context["allow_apply_trial"] = False
            else:
                context["allow_apply_trial"] = True

            if all_accounts_list:
                context["allow_import_csv"] = True
            else:
                context["allow_import_csv"] = False

            # Pagination for licenses
            paginate_by = 10

            search_query = self.request.GET.get('search_box', "")
            context["search_query"] = search_query

            if search_query:
                q_all_list = all_accounts_list.filter(
                                    Q(username__contains=search_query) |
                                    Q(full_name__icontains=search_query) |
                                    Q(email__contains=search_query)
                                    )
                all_accounts_list = q_all_list
            all_paginator = Paginator(all_accounts_list, paginate_by)
            context['all_accounts_pages'] = all_paginator.num_pages
            context['all_accounts_pagerange'] = all_paginator.page_range
            context["all_accounts"] = []
            for i in all_paginator.page_range:
                page = all_paginator.page(i)
                context["all_accounts"].append((i,page))

            context["paginate_by"] = paginate_by

        return context


class SchoolUserDetailView(DetailView):
    model = SchoolUser
    template_name = 'users/schooluser_detail.html'
    login_required = True

    def get_object(self):
        """
        Returns the current user.
        """
        try:
            if self.request.user.is_authenticated():
                if self.request.user.is_superuser:
                    schooluser = self.request.GET.get('schooluser', '')
                    if schooluser:
                        schooluser_obj = SchoolUser.objects.filter(email=schooluser)
                        if schooluser_obj.first():
                            return schooluser_obj.first()
                        else:
                            raise PermissionDenied
                    else:
                        raise PermissionDenied
                else:
                    try:
                        current_user = self.request.user.schooluser
                        return current_user
                    except:
                        try:
                            current_user = self.request.user.masteruser
                            self.model = MasterUser
                            return current_user
                        except:
                            raise PermissionDenied
            else:
                raise PermissionDenied
        except PermissionDenied:
            raise PermissionDenied
        except Exception as e:
            raise Http404(_("%s" % e))


class SchoolUserUpdateProfileView(UpdateView):
    model = SchoolUser
    form_class = RequestFreeTrialForm
    template_name = 'users/schooluser_profile_form.html'
    login_required = True
    success_url = reverse('schooluser_detail')

    def get_object(self, queryset=None):
        if hasattr(self.request.user, "schooluser") and self.request.user.schooluser:
            return self.request.user.schooluser
        else:
            raise Http404(_("You are not a School User. You cannot access this page."))



# REDEEM
class RedeemLicenseView(FormView):
    template_name = 'users/redeem_license.html'
    form_class = RedeemLicenseForm
    success_url = reverse('license_redeem_thankyou')


class RedeemLicenseThankYouView(TemplateView):
    template_name = 'users/redeem_license_thankyou.html'


# LICENSE ACTIVATION
class ActivateLicenseView(PasswordResetBeforeActivationView):
    template_name = 'users/activate_license.html'

    def get(self, *args, **kwargs):
        super(ActivateLicenseView, self).get(*args, **kwargs)
        context = {}
        context['is_already_activated'] = False
        context['activated_user'] = False
        username, email = self.validate_key(kwargs.get('activation_key'))
        if username:
            user = self.get_user(username, email)
            if user:
                if user.is_active:
                    context['is_already_activated'] = True
                    context['activated_user'] = user
                else:
                    activated_user = self.activate(*args, **kwargs)
                    context['activated_user'] = activated_user

        return TemplateResponse(self.request, self.template_name, context)


class InviteLicenseView(UpdateView):
    template_name = 'users/includes/invite_license.html'
    model = MasterUser
    slug_url_kwarg = 'user_id'
    slug_field = 'user_id'
    form_class = InviteLicenseForm
    success_url = reverse('schooluser_management')

    def get_context_data(self, **kwargs):
        context = super(InviteLicenseView, self).get_context_data(**kwargs)
        context['license'] = self.get_object()

        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            template_name = 'users/includes/invite_license_success.html'
            context = {"full_name": form.cleaned_data['full_name']}
            return TemplateResponse(self.request, template_name, context)

        else:
            return super(InviteLicenseView, self).form_valid(form)


    def form_invalid(self, form):
        return super(InviteLicenseView, self).form_invalid(form)


class LicenseSetUsernameView(FormView):
    template_name = 'users/license_set_username.html'
    form_class = LicenseSetUsernameForm
    success_url = reverse('license_set_username_thankyou')

    def get_context_data(self, **kwargs):
        context = super(LicenseSetUsernameView, self).get_context_data(**kwargs)
        form_kwargs = self.get_form_kwargs()
        context['masteruser'] = form_kwargs.get('masteruser')
        return context

    def get_form_kwargs(self):
        kwargs = super(LicenseSetUsernameView, self).get_form_kwargs()
        username, email = validate_activation_key(self.kwargs.get('activation_key'))
        masteruser = None
        if username is not None and email is not None:
            user = User.objects.filter(username=username, email=email).first()
            # print user
            if user and hasattr(user, "masteruser") and user.masteruser:
                masteruser = user.masteruser

        # print masteruser
        kwargs.update({
            'masteruser': masteruser
        })
        # print kwargs
        return kwargs


class LicenseSetUsernameThankYouView(TemplateView):
    template_name = 'users/license_set_username_thankyou.html'


def send_invite_license_email(request):
    email = request.POST.get("email") or request.GET.get("email")
    license = MasterUser.active_objects.filter(email=email).first()
    if license:
        send_license_set_username_email(license)
        data = {"success": ["Invitation email sent to %s." % (email)]}
    else:
        data = {}
    return json_response(**data)


class CancelInvitationView(TemplateView):
    template_name = 'users/includes/cancel_invitation.html'

    def get(self, *args, **kwargs):
        super(CancelInvitationView, self).get(*args, **kwargs)
        context = {}
        code = self.request.GET.get("code")
        context['code'] = code.strip()
        masteruser = MasterUser.active_objects.filter(user_id__exact=context['code']).first()
        context['masteruser'] = masteruser
        context["message"] = "Are you sure you want to cancel invitation for %s?" % masteruser.email
        context["cancel_success"] = False

        return TemplateResponse(self.request, self.template_name, context)

    def post(self, *args, **kwargs):
        context = super(CancelInvitationView, self).get_context_data(**kwargs)
        code = self.request.GET.get("code").strip()
        masteruser = MasterUser.active_objects.filter(user_id__exact=code).first()
        context["cancel_success"] = False

        if masteruser and masteruser.auth_user.is_active:
            context["message"] = 'This License has already been activated. Invitation cannot be cancelled.'
        elif masteruser and not masteruser.auth_user.is_active:
            license_email = masteruser.email
            license_fullname = masteruser.full_name
            license_schoolname = masteruser.schooluser.school or masteruser.schooluser.full_name

            context["message"] = 'Invitation cancelled for %s' % license_email
            masteruser.username = None
            masteruser.email = None
            masteruser.full_name = None
            masteruser.auth_user.username = masteruser.user_id
            masteruser.auth_user.email = ""
            masteruser.auth_user.save()
            masteruser.save()
            context["cancel_success"] = True
            # send cancel invitation email

            send_cancel_invite_email(license_email, license_fullname, license_schoolname)
        elif not masteruser:
            context["message"] = 'This Redeem Code does not exist.'

        return TemplateResponse(self.request, self.template_name, context)


class RequestMoreFreeFullView(FormView):
    template_name = 'users/includes/request_more_freefull_form.html'
    form_class = RequestMoreFreeFullForm
    success_url = reverse('schooluser_management')

    def form_valid(self, form):
        if form.cleaned_data.get("freefull_accounts_num") and form.cleaned_data.get("freefull_start_date"):
            form.save()
            messages.success(self.request, 'Requested for more Trial Licenses successfully.')
            # NOTE TO SEND NEW REQUEST EMAIL
        return super(RequestMoreFreeFullView, self).form_valid(form)

class RequestMoreDemoView(FormView):
    template_name = 'users/includes/request_more_demo_form.html'
    form_class = RequestMoreDemoForm
    success_url = reverse('schooluser_management')

    def form_valid(self, form):
        if form.cleaned_data.get("demo_accounts_num") and form.cleaned_data.get("demo_start_date"):
            form.save()
            messages.success(self.request, 'Requested for more Trial Licenses successfully.')
            # NOTE TO SEND NEW REQUEST EMAIL
        return super(RequestMoreDemoView, self).form_valid(form)


""" ------ CSV Import Views ------ """
import csv
import codecs
from .exports import (license_import_template_csv_export,
    license_export_csv, quiz_sittings_csv_export,
    gamesession_export_csv)

class UploadCsvView(TemplateView):
    template_name = 'users/includes/upload_csv.html'

    def get(self, *args, **kwargs):
        super(UploadCsvView, self).get(*args, **kwargs)
        context = {}
        context['form'] = UploadCsvForm()
        return TemplateResponse(self.request, self.template_name, context)

    def post(self, *args, **kwargs):
        context = {}
        form = UploadCsvForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            messages.success(self.request, 'Licenses are updated successfully.')
        else:
            # print form.errors.as_json()
            for field, error_list in json.loads(form.errors.as_json()).iteritems():
                for error in error_list:
                    messages.warning(self.request, error["message"])
        context['form'] = form
        return HttpResponseRedirect(reverse('schooluser_management'))


def license_import_template_export(request):
    try:
        content = license_import_template_csv_export()
        response = HttpResponse(content, content_type='text/csv')
        response['Content-Disposition'] = \
            'attachment; filename=License-Importing-Template.csv'
        return response
    except:
        return HttpResponseRedirect(reverse('schooluser_management'))


def quiz_sittings_export(request):
    try:
        if request.user.is_authenticated():
            current_user = request.user.schooluser
            trial_accounts = current_user.get_trial_accounts()

            content = quiz_sittings_csv_export(request, trial_accounts)
            response = HttpResponse(content, content_type='text/csv')
            response['Content-Disposition'] = \
                'attachment; filename=Quiz-Sittings-Template.csv'
            return response
        else:
            return HttpResponseRedirect(reverse('schooluser_management'))
    except:
        return HttpResponseRedirect(reverse('schooluser_management'))


def generate_licenses(request, account_type):
    try:
        if request.user.is_superuser or (request.user.is_authenticated() and hasattr(request.user, "schooluser")):
            accounts_to_generate = int(request.GET.get("num", 0))
            schooluser = None
            if request.user.is_superuser:
                schooluser_email = request.GET.get("schooluser", None)
                if schooluser_email:
                    schooluser = SchoolUser.objects.filter(email=schooluser_email).first()
            else:
                schooluser = request.user.schooluser

            if schooluser:
                account_type_name = "Full Access" if account_type == "full" else "Trial"

                if account_type == "full":
                    if accounts_to_generate:
                        for i in range(0, accounts_to_generate):
                            MasterUser.objects.create(schooluser=schooluser, is_trial=False, is_expired_account=False)

                        full_accounts = schooluser.get_full_accounts()
                        for acc in full_accounts.filter(serial_no__isnull=True):
                            acc.save()
                        full_accounts = full_accounts.order_by("serial_no")
                elif account_type == "trial":
                    if accounts_to_generate:
                        for i in range(0, accounts_to_generate):
                            MasterUser.objects.create(schooluser=schooluser, is_trial=True, is_expired_account=False)

                        trial_accounts = schooluser.get_trial_accounts()
                        for acc in trial_accounts.filter(serial_no__isnull=True):
                            acc.save()
                        trial_accounts = trial_accounts.order_by("serial_no")
    except Exception as e:
        print e

    if request.user.is_superuser:
        return HttpResponseRedirect(reverse('schooluser_management') + "?schooluser=%s" % request.GET.get("schooluser"))
    else:
        return HttpResponseRedirect(reverse('schooluser_management'))


def export_licenses_as_csv(request):
    try:
        if request.user.is_authenticated() and hasattr(request.user, "schooluser"):
            schooluser = request.user.schooluser
            accounts = schooluser.masteruser_set.filter(is_active=True).order_by("serial_no")
            if accounts:
                content = license_export_csv(accounts)
            else:
                return HttpResponseRedirect(reverse('schooluser_management'))

            response = HttpResponse(content, content_type='text/csv')
            filename = "%s-License-Export.csv" % schooluser.get_school_slug()
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            # print response
            return response
        else:
            return HttpResponseRedirect(reverse('schooluser_management'))
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('schooluser_management'))


def gamesession_csv_export(request):
    try:
        if request.user.is_authenticated():
            current_user = request.user.schooluser
            accounts = current_user.masteruser_set.all().order_by("serial_no")

            content = gamesession_export_csv(accounts)
            # print content
            response = HttpResponse(content, content_type='text/csv')
            response['Content-Disposition'] = \
                'attachment; filename=%s-Game-Sessions.csv' % current_user.school_id
            return response
        else:
            return HttpResponseRedirect(reverse('schooluser_management'))
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('schooluser_management'))



""" ------ Trial Account Views ------ """
# class TrialAccountCreateView(CreateView):
#     template_name = 'users/includes/trialaccount_create.html'
#     model = MasterUser
#     form_class = MasterUserAccountCreateForm
#     success_url = reverse('modal_submit_success')

#     def get_initial(self):
#         initial = super(TrialAccountCreateView, self).get_initial()
#         initial['schooluser'] = self.request.GET.get("schooluser")
#         initial['is_trial'] = True
#         initial['is_expired_account'] = False

#         return initial

# class TrialAccountCreateView(TemplateView):
#     template_name = 'users/includes/trialaccount_create.html'

#     def post(self, *args, **kwargs):
#         context = {}
#         form = MasterUserAccountCreateForm(self.request.POST)
#         if form.is_valid():
#             form.save()
#             messages.success(self.request, 'Added License successfully.')
#         else:
#             for error in form.errors:
#                 for e_list in form.errors[error].as_data():
#                     for e in e_list:
#                         messages.warning(self.request, e)
#         return HttpResponseRedirect(reverse('schooluser_management'))


def trialaccount_send_activation_email(request):
    email = request.POST.get("email") or request.GET.get("email")
    exist_user = User.objects.filter(email=email).first()
    if exist_user:
        send_activation_email(exist_user)
        # messages.success(request, 'Activation email sent to %s.' % (email))
        data = {"success": ["Activation email sent to %s." % (email)]}
    return json_response(**data)
    # return HttpResponseRedirect(reverse("schooluser_management"))

def trialaccount_deactivate(request):
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        user_obj.is_active = False
        user_obj.save()
        messages.success(request, '%s deactivated.' % (user_obj))
    except:
        pass
    return HttpResponseRedirect(reverse("schooluser_management"))


def trialaccount_activate(request):
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        user_obj.is_active = True
        if hasattr(user_obj, 'masteruser'):
            if user_obj.masteruser and user_obj.masteruser.is_trial:
                user_obj.masteruser.save()
        user_obj.save()
        messages.success(request, '%s activated.' % (user_obj))
    except:
        pass
    return HttpResponseRedirect(reverse("schooluser_management"))


def trialaccount_sendinvitation(request):
    ajax = request.GET.get("ajax")
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        email = user_obj.email
        if email:
            send_invitation_to_download_email(user_obj)
        else:
            raise Exception("Email is empty for %s." % user_obj)
        if ajax == "true":
            data = {"success": ["Invitation link sent to %s." % (email)]}
        else:
            messages.success(request, 'Invitation link sent to %s.' % (user_obj))
    except Exception as e:
        print e

    if ajax == "true":
        return json_response(**data)
    else:
        return HttpResponseRedirect(reverse("schooluser_management"))


def trialaccount_getprogress(request):
    ajax = request.GET.get("ajax")
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        data = {"container_content": "",
                "container_class": "report-contents",
                "next_function": "show_progress"}
        t = loader.get_template('users/includes/report_contents.html')
        c = {'user': user_obj,
            'masteruser': user_obj.masteruser,
            }

        render_response = t.render(c)
        data["container_content"] = render_response

    except Exception as e:
        print e

    if ajax == "true":
        return json_response(**data)
    else:
        return HttpResponseRedirect(reverse("schooluser_management"))


class TrialAccountReportView(DetailView):
    template_name = 'users/license_report.html'
    model = MasterUser
    slug_field = 'user_id'
    slug_url_kwarg = 'user_id'


# def trialaccount_update_username_email(request, pk, *args, **kwargs):
#     masteruser = MasterUser.objects.get(id=pk)
#     form = LicenseUpdateForm(request.POST or None,
#                     initial={"username": masteruser.username,
#                             "email": masteruser.email},
#                     instance=masteruser)
#     if request.method == "POST" and form.is_valid():
#         form.save()
#         # messages.success(request, 'Updated License successfully.')
#         data = {"success": ["Updated License successfully"]}
#         return json_response(**data)

#     elif request.method == "POST" and not form.is_valid():
#         data = {"warning": []}
#         for error in form.errors:
#             for e_list in form.errors[error].as_data():
#                 for e in e_list:
#                     data["warning"].append(e)
#         return json_response(**data)

#     return render(request, 'users/includes/trialaccount_update_username_email_form.html', {
#         'form': form,
#         'masteruser': masteruser
#     })





""" ------ Full Access Account Views ------ """
# class FullAccountCreateView(CreateView):
#     template_name = 'users/includes/fullaccount_create.html'
#     model = MasterUser
#     form_class = MasterUserAccountCreateForm
#     success_url = reverse('modal_submit_success')

#     def get_initial(self):
#         initial = super(FullAccountCreateView, self).get_initial()
#         initial['schooluser'] = self.request.GET.get("schooluser")
#         initial['is_trial'] = False
#         initial['is_expired_account'] = False

#         return initial

# class FullAccountCreateView(TemplateView):
#     template_name = 'users/includes/fullaccount_create.html'

#     def post(self, *args, **kwargs):
#         context = {}
#         form = MasterUserAccountCreateForm(self.request.POST)
#         if form.is_valid():
#             form.save()
#             messages.success(self.request, 'Added License successfully.')
#         else:
#             for error in form.errors:
#                 for e_list in form.errors[error].as_data():
#                     for e in e_list:
#                         messages.warning(self.request, e)
#         return HttpResponseRedirect(reverse('schooluser_management'))

def fullaccount_send_activation_email(request):
    email = request.POST.get("email") or request.GET.get("email")
    exist_user = User.objects.filter(email=email).first()
    if exist_user:
        send_activation_email(exist_user)
        messages.success(request, 'Activation email sent to %s.' % (email))
    return HttpResponseRedirect(reverse("schooluser_management"))

def fullaccount_deactivate(request):
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        user_obj.is_active = False
        user_obj.save()
        messages.success(request, '%s deactivated.' % (user_obj))
    except:
        pass
    return HttpResponseRedirect(reverse("schooluser_management"))


def fullaccount_activate(request):
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        user_obj.is_active = True
        if hasattr(user_obj, 'masteruser'):
            if user_obj.masteruser and not user_obj.masteruser.is_trial:
                user_obj.masteruser.save()
        user_obj.save()
        messages.success(request, '%s activated.' % (user_obj))
    except:
        pass
    return HttpResponseRedirect(reverse("schooluser_management"))


def fullaccount_sendinvitation(request):
    ajax = request.GET.get("ajax")
    user_id = request.POST.get("user_id") or request.GET.get("user_id")
    try:
        user_obj = User.objects.get(id=user_id)
        email = user_obj.email
        if email:
            send_invitation_to_download_email(user_obj)
        else:
            raise Exception("Email is empty for %s." % user_obj)
        if ajax == "true":
            data = {"success": ["Invitation link sent to %s." % (email)]}
        else:
            messages.success(request, 'Invitation link sent to %s.' % (user_obj))
    except Exception as e:
        print e

    if ajax == "true":
        return json_response(**data)
    else:
        return HttpResponseRedirect(reverse("schooluser_management"))


# def fullaccount_update_username_email(request, pk, *args, **kwargs):
#     masteruser = MasterUser.objects.get(id=pk)
#     form = LicenseUpdateForm(request.POST or None,
#                     initial={"username": masteruser.username,
#                             "email": masteruser.email},
#                     instance=masteruser)
#     if request.method == "POST" and form.is_valid():
#         form.save()
#         # messages.success(request, 'Updated License successfully.')
#         data = {"success": ["Updated License successfully"]}
#         return json_response(**data)

#     elif request.method == "POST" and not form.is_valid():
#         data = {"warning": []}
#         for error in form.errors:
#             for e_list in form.errors[error].as_data():
#                 for e in e_list:
#                     data["warning"].append(e)
#         return json_response(**data)

#     return render(request, 'users/includes/fullaccount_update_username_email_form.html', {
#         'form': form,
#         'masteruser': masteruser
#     })


# class RequestAdditionalLicenseView(TemplateView):
#     template_name = 'users/request_additional_license.html'
#     login_required = True

#     def get(self, *args, **kwargs):
#         super(RequestAdditionalLicenseView, self).get(*args, **kwargs)
#         context = {}
#         context['form'] = RequestAdditionalLicenseForm()
#         return TemplateResponse(self.request, self.template_name, context)

#     def post(self, *args, **kwargs):
#         context = {}
#         form = RequestAdditionalLicenseForm(self.request.POST)
#         if form.is_valid():
#             send_request_additional_license_email(self.request.user, form.cleaned_data.get("trial_count"), form.cleaned_data.get("full_count"))
#             messages.success(self.request, 'Requested for additional Licenses.')
#             return HttpResponseRedirect(reverse('schooluser_detail'))
#         context['form'] = form
#         return render(self.request, self.template_name, context)


# class RequestProgramExtensionView(TemplateView):
#     template_name = 'users/request_program_extension.html'

#     def get(self, *args, **kwargs):
#         super(RequestProgramExtensionView, self).get(*args, **kwargs)
#         context = {}
#         context['form'] = RequestProgramExtensionForm()
#         return TemplateResponse(self.request, self.template_name, context)

#     def post(self, *args, **kwargs):
#         context = {}
#         form = RequestProgramExtensionForm(self.request.POST)
#         if form.is_valid():
#             send_request_program_extension_email(self.request.user, form.cleaned_data.get("day_count"))
#             messages.success(self.request, 'Requested for program extension.')
#             return HttpResponseRedirect(reverse('schooluser_detail'))
#         context['form'] = form
#         return render(self.request, self.template_name, context)

# class SchoolUserRegisterTrialView(CreateView):
#     model = SchoolUser
#     form_class = RegisterTrialForm
#     template_name = 'users/register_trial_form.html'
#     success_url = reverse('register_trial_thank_you')

#     def get_initial(self):
#         initial = super(SchoolUserRegisterTrialView, self).get_initial()
#         # initial['account_package'] = self.request.GET.get("account_package")
#         return initial

#     def get_context_data(self, **kwargs):
#         context = super(SchoolUserRegisterTrialView, self).get_context_data(**kwargs)

#         # try:
#         #     context['account_package'] = AccountPackage.objects.get(id=self.request.GET.get("account_package"))
#         # except Exception as e:
#         #     context['account_package'] = "a Trial"
#         return context

#     def form_invalid(self, form):
#         print form.errors
#         return self.render_to_response(self.get_context_data(form=form))

#     def form_valid(self, form):
#         schooluser = form.save()
#         send_request_trial_email_to_system(schooluser)
#         return HttpResponseRedirect(self.success_url)


# class SchoolUserRegisterThankYouView(TemplateView):
#     template_name = 'users/register_trial_thankyou.html'
#     login_required = False


# class InviteLicenseView(FormView):
#     template_name = 'users/includes/invite_license.html'
#     form_class = InviteLicenseForm
#     success_url = reverse('schooluser_management')

#     def get_initial(self):
#         initial = super(InviteLicenseView, self).get_initial()

#         is_trial = True if self.request.GET.get("type") == 'trial' else False
#         initial['is_trial'] = is_trial

#         try:
#             schooluser = SchoolUser.objects.get(id=self.request.GET.get("schooluser"))
#             initial['schooluser'] = schooluser
#         except Exception as e:
#             print e
#         return initial


#     def form_valid(self, form):
#         messages.success(self.request, 'Invited %s successfully.' % form.cleaned_data['full_name'])
#         return super(InviteLicenseView, self).form_valid(form)


#     def form_invalid(self, form):
#         for error in form.errors:
#             for e_list in form.errors[error].as_data():
#                 for e in e_list:
#                     messages.warning(self.request, e)
#         return HttpResponseRedirect(reverse('schooluser_management'))
