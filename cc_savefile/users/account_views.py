import datetime
import pytz
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.core import signing
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse_lazy as reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.views.generic import TemplateView

from .forms import SetPasswordForm, ResendActivationEmailForm
from .utils import send_activation_email, validate_activation_key

REGISTRATION_SALT = getattr(settings, 'REGISTRATION_SALT', 'registration')


from django.dispatch import Signal
# A new user has registered.
user_registered = Signal(providing_args=["user", "request"])

# A user has activated his or her account.
user_activated = Signal(providing_args=["user", "request"])


class PasswordResetBeforeActivationView(TemplateView):
    """
    Custom Activation View that includes the Password Reset form

    """

    template_name = 'registration/password_reset_before_activate.html'

    def get(self, *args, **kwargs):
        """
        Renders the template for Password Reset form

        """
        super(PasswordResetBeforeActivationView, self).get(*args, **kwargs)

        context = {}
        user = self.get_user_from_activation_key(kwargs.get('activation_key'))
        if user is not None:
            context = {'form': SetPasswordForm(user=user),
                        'activation_key': kwargs.get('activation_key'),
                        'user': user}

        return TemplateResponse(self.request, self.template_name, context)


    def post(self, *args, **kwargs):
        user = self.get_user_from_activation_key(kwargs.get('activation_key'))
        form = SetPasswordForm(user, self.request.POST)
        if form.is_valid():
            form.save()
            activated_user = self.activate(*args, **kwargs)
            if activated_user:
                user_activated.send(
                    sender=self.__class__,
                    user=activated_user,
                    request=self.request
                )
                success_url = self.get_success_url(activated_user)
                try:
                    to, args, kwargs = success_url
                    return redirect(to, *args, **kwargs)
                except ValueError:
                    return redirect(success_url)

        context = {'form': form,
                    'activation_key': kwargs.get('activation_key'),
                    'user': user}
        return TemplateResponse(self.request, self.template_name, context)

    def activate(self, *args, **kwargs):
        # This is safe even if, somehow, there's no activation key,
        # because unsign() will raise BadSignature rather than
        # TypeError on a value of None.
        username, email = self.validate_key(kwargs.get('activation_key'))
        if username is not None:
            user = self.get_user(username, email)
            if user is not None:
                if not user.is_active:
                    user.is_active = True
                    user.save()

                    if hasattr(user, 'masteruser'):
                        if user.masteruser:
                            user.masteruser.save()
                    # if hasattr(user, 'schooluser'):
                    #     if user.schooluser and user.schooluser.account_package:
                    #         user.schooluser.start_date = datetime.datetime.now(pytz.utc)
                    #         user.schooluser.end_date = datetime.datetime.now(pytz.utc) + relativedelta(years=user.schooluser.account_package.renewal_years, months=user.schooluser.account_package.renewal_months, days=user.schooluser.account_package.renewal_days)
                    #         user.schooluser.save()

                    return user
        return False

    def get_success_url(self, user):
        return ('registration_activation_complete', (), {})

    def validate_key(self, activation_key):
        """
        Verify that the activation key is valid and within the
        permitted activation time window, returning the username if
        valid or ``None`` if not.

        """
        try:
            username, email = validate_activation_key(activation_key)
            return username, email
        # SignatureExpired is a subclass of BadSignature, so this will
        # catch either one.
        except signing.BadSignature:
            return None, None

    def get_user(self, username, email):
        """
        Given the verified username, look up and return the
        corresponding user account if it exists, or ``None`` if it
        doesn't.

        """
        User = get_user_model()
        lookup_kwargs = {
            User.USERNAME_FIELD: username,
            'email': email,
            # 'is_active': False
        }
        try:
            user = User.objects.get(**lookup_kwargs)
            return user
        except User.DoesNotExist:
            lookup_kwargs = {
                'email': email,
                # 'is_active': False
            }
            try:
                user = User.objects.get(**lookup_kwargs)
                return user
            except User.DoesNotExist:
                return None

            return None

    def get_user_from_activation_key(self, activation_key):
        username, email = self.validate_key(activation_key)
        if username and email:
            user = self.get_user(username, email)
            return user
        return None


class ResendActivationEmailView(TemplateView):
    template_name = 'registration/activation_form.html'

    def get(self, *args, **kwargs):
        """
        Renders the template for Resend Activation Email form

        """
        super(ResendActivationEmailView, self).get(*args, **kwargs)

        context = {}
        context = {'form': ResendActivationEmailForm(),}
        return TemplateResponse(self.request, self.template_name, context)


    def post(self, *args, **kwargs):
        form = ResendActivationEmailForm(self.request.POST)
        if form.is_valid():
            exist_user = get_user_model().objects.filter(email=form.cleaned_data['email']).first()
            if exist_user and not exist_user.is_active:
                send_activation_email(exist_user)
                return HttpResponseRedirect(reverse('users_resend_activation_email_success', kwargs={'mode': 1}))
            elif exist_user and exist_user.is_active:
                return HttpResponseRedirect(reverse('users_resend_activation_email_success', kwargs={'mode': 2}))
            else:
                return HttpResponseRedirect(reverse('users_resend_activation_email_success', kwargs={'mode': 3}))
        context = {'form': form,}
        return TemplateResponse(self.request, self.template_name, context)


class ResendActivationEmailSuccessView(TemplateView):
    template_name = 'registration/activation_email_sent.html'

    def get_context_data(self, **kwargs):
        context = super(ResendActivationEmailSuccessView, self).get_context_data(**kwargs)
        context['mode'] = int(self.kwargs.get('mode', 0))
        return context
