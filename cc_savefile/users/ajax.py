import pytz
from datetime import date, datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.core.urlresolvers import reverse_lazy as reverse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.utils.text import slugify
from cc_savefile.utils import json_response
from .models import MasterUser

# from tc_two.conf import settings as tc_settings

""" ----- General ajax ----- """
def check_redeem_code(request):
    code = request.GET.get("code", None)
    if code:
        masteruser = MasterUser.active_objects.filter(user_id__exact=code.strip()).first()
    else:
        masteruser = None
        
    if masteruser and masteruser.auth_user.is_active:
        data = {'response': "This License has already been activated. Please use another Redeem Code. Contact us at info@chemcaper.com for assistance if you suspect your Redeem Code has been activated without your permission.", "success":False}
    elif masteruser and not masteruser.auth_user.is_active:
        data = {'response': "", "user_data": {"username": masteruser.username, "full_name": masteruser.full_name, "email": masteruser.email}, "success":True}
    elif not masteruser:
        data = {'response': "This Redeem Code does not exist.", "success":False}

    return json_response(**data)
