from __future__ import absolute_import

from celery import shared_task

import logging

from constance import config

logger = logging.getLogger("ccserver")


@shared_task
def logout_inactive_users():
    from .models import MasterUser
    from datetime import date, datetime, timedelta
    from django.utils.timezone import now as datetime_now
    from rest_framework import authtoken

    INACTIVE_DURATION_SECONDS = config.INACTIVE_DURATION_SECONDS
    logged_in_users = MasterUser.active_objects.filter(is_logged_in=True)

    logged_out_users = []
    for masteruser in logged_in_users:
        if masteruser.last_activity:
            timesince_lastactivity = datetime_now() - masteruser.last_activity
            # mins, seconds = divmod(timesince_lastactivity.days * 86400 + timesince_lastactivity.seconds, 60)
            seconds = timesince_lastactivity.days * 86400 + timesince_lastactivity.seconds
        else:
            seconds = INACTIVE_DURATION_SECONDS

        if seconds >= INACTIVE_DURATION_SECONDS:
            # authtoken.models.Token.objects.filter(user=masteruser.auth_user).delete()
            masteruser.is_logged_in = False
            masteruser.save()
            logged_out_users.append(masteruser.auth_user.username)
    message = 'Logged out %s users - %s' % (len(logged_out_users), ", ".join(logged_out_users))
    logger.debug(message)
    return message
