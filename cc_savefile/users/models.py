from __future__ import unicode_literals
import datetime
import pytz
import pprint
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import now as datetime_now
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.sessions.models import Session
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from django_countries.fields import CountryField
from itertools import groupby
from constance import config
from rest_framework.authtoken.models import Token
from timezone_field import TimeZoneField
from dateutil.relativedelta import relativedelta


from quiz.models import Question, Quiz, Sitting, SittingImage, AnswerChoice
from gamesaves.models import GameSession, GameSessionHeartBeat, TrialGameSave, GamePlatform
from .signals import *
from .utils import *
from .base_models import *

pp = pprint.PrettyPrinter(indent=2)

'''
################
NOTE:
is_trial & !is_free_full = Demo account
!is_trial & is_free_full = Full account (Trial)
!is_trial & !is_free_full = Full account (Paid)
################
'''

class MasterUserManager(models.Manager):
    def get_queryset(self):
        return super(MasterUserManager, self).get_queryset().filter(is_active=True)


class MasterUser(TimeStampedModel):
    STUDENT = 1
    TEACHER = 2

    ROLE_LIST = (
        (STUDENT, _("Student")),
        (TEACHER, _("Teacher")),
    )

    user_id = models.CharField(_('User ID'), max_length=100, blank=True, null=True, unique=True)
    auth_user = models.OneToOneField("auth.User", unique=True, related_name='masteruser', editable=False)
    username = models.CharField(_('Username'), max_length=300, blank=True, null=True)
    full_name = models.CharField(_('Full Name'), max_length=300, blank=True, null=True)
    student_id = models.CharField(_('Student ID'), max_length=300, blank=True, null=True)
    classroom = models.CharField(_('Classroom'), max_length=300, blank=True, null=True)
    serial_no = models.IntegerField(_('Serial No.'), blank=True, null=True)
    serial_no_display = models.CharField(_('Serial No.'), max_length=200, blank=True, null=True)
    email = models.EmailField(_('Email'), blank=True, null=True)
    is_active = models.BooleanField(_('Is Usable Account'), default=True)
    is_expired_account = models.BooleanField(_('Is Expired Account'), default=False)
    is_trial = models.BooleanField(_('Is Trial Account'), default=False)
    is_free_full = models.BooleanField(_('Is Free Full License'), default=False)
    schooluser = models.ForeignKey("users.SchoolUser", verbose_name=_('Belongs to School'),
                blank=True, null=True)
    trial_start_date = models.DateTimeField(_('Trial Start Date'),
                blank=True, null=True)
    trial_end_date = models.DateTimeField(_('Trial End Date'),
                blank=True, null=True)
    role = models.IntegerField(_("Role"), choices=ROLE_LIST,
                default=STUDENT)

    gameplatforms = models.ManyToManyField("gamesaves.GamePlatform",
                        verbose_name=_("Game-Platforms"),
                        help_text='Available for which game-platforms (Only do this for Licenses under a school)',
                        related_name="gameplatform_masterusers",
                        blank=True)

    apple_id = models.CharField(_('Apple ID'), max_length=100, blank=True, null=True)
    googleplay_id = models.CharField(_('Google Play ID'), max_length=100, blank=True, null=True)
    gamecenter_id = models.CharField(_('Game Center ID'), max_length=100, blank=True, null=True)
    filament_id = models.CharField(_('Filament Games ID'), max_length=100, blank=True, null=True)
    teachergaming_id = models.CharField(_('Teacher Gaming ID'), max_length=100, blank=True, null=True)
    steam_id = models.CharField(_('Steam ID'), max_length=100, blank=True, null=True)

    invitation_key = models.CharField(_('Invitation to Download key'), max_length=300, blank=True, null=True)
    is_logged_in = models.BooleanField(_('Is Currently Logged In'), default=False)
    last_activity = models.DateTimeField(_('Last Activity'), blank=True, null=True)

    objects = models.Manager() # The default manager.
    active_objects = MasterUserManager()

    class Meta:
        verbose_name = _('User Account (Game)')
        verbose_name_plural = _('User Accounts (Game)')

    def __unicode__(self):
        return "%s (%s)" % (self.username or self.user_id, self.email)

    def get_gameplatforms_as_string(self):
        gameplatforms = self.gameplatforms.all()
        if gameplatforms:
            gameplatforms_list = [gameplatform.platform.title for gameplatform in gameplatforms]
            return_str = " / ".join(gameplatforms_list)
            return return_str
        else:
            return ""

    def is_expired(self):
        if (self.is_trial or self.is_free_full) and self.trial_end_date:
            now = datetime.datetime.now(pytz.utc)
            if self.trial_end_date < now:
                return True
        return False

    def check_is_logged_in(self):
        INACTIVE_DURATION_SECONDS = config.INACTIVE_DURATION_SECONDS
        if self.is_logged_in:
            if self.last_activity:
                timesince_lastactivity = datetime_now() - self.last_activity
                # mins, seconds = divmod(timesince_lastactivity.days * 86400 + timesince_lastactivity.seconds, 60)
                seconds = timesince_lastactivity.days * 86400 + timesince_lastactivity.seconds
            else:
                seconds = INACTIVE_DURATION_SECONDS

            if seconds >= INACTIVE_DURATION_SECONDS:
                return False
            else:
                return True
        else:
            return False


    def get_last_activity(self):
        return_str_list = []
        if not self.last_activity:
            self.save_last_activity()

        if self.last_activity:
            last_activity = self.last_activity
            if last_activity:
                now = datetime.datetime.now(pytz.utc)
                td = now - last_activity
                d = td.days  # days
                h = divmod(td.seconds,3600)  # hours
                m = divmod(h[1],60)  # minutes
                s = m[1]  # seconds

                if d != 0:
                    return_str_list.append("%d days" % d)
                if h[0] != 0.0:
                    return_str_list.append("%d hours" % h[0])
                if m[0] != 0.0:
                    return_str_list.append("%d minutes" % m[0])
                if s != 0.0:
                    return_str_list.append("%d seconds" % s)

        if return_str_list:
            return " ".join(return_str_list)
        else:
            return ""


    def save_last_activity(self, log_session=False):
        masteruser = MasterUser.active_objects.filter(id=self.id)
        if masteruser:
            masteruser.update(last_activity=datetime_now(), is_logged_in=True)

            if log_session:
                token = Token.objects.filter(user=self.auth_user).first()
                if token:
                    gamesession = GameSession.objects.filter(masteruser=masteruser.first(), token_key=token.key)
                    if gamesession:
                        latest_gamesession = gamesession.latest("created")
                        GameSessionHeartBeat.objects.create(gamesession=latest_gamesession)
                    else:
                        latest_gamesession = GameSession.objects.create(masteruser=masteruser.first(), token_key=token.key)
                        GameSessionHeartBeat.objects.create(gamesession=latest_gamesession)

    def show_session_report(self):
        sessions = self.trialgamesave_set.all()
        sittings = self.sitting_set.all()
        data = {"sessions": sessions, "sittings": sittings}
        return data

    def show_report(self):
        sittings_qs = self.sitting_set.all()
        data = []

        grouped = dict()
        for obj in sittings_qs:
            grouped.setdefault(obj.quiz, []).append(obj)

        for quiz, sittings in grouped.items():
            # looping through the grouped sittings
            data_obj = {"quiz": quiz, "report": [], "sittings": len(sittings)}
            for s in sittings:
                if data_obj["report"]:
                    # if report already has data
                    question_list = [r_data["question"].id for r_data in data_obj["report"]]
                    if s.answers:
                        for sitting_data in s.answers:
                            # looping through the json data of each submitted answers
                            if sitting_data["question"] in question_list:
                                # if the question.id already exists in the report,
                                # update the correct answer count by question type
                                question_obj = Question.objects.get(id=sitting_data["question"])
                                for r_data in data_obj["report"]:
                                    if r_data["question"].id == sitting_data["question"]:
                                        if question_obj.q_type == Question.ANSWER:
                                            for a_data in r_data["answers"]:
                                                # if sitting_data["is_correct"]:
                                                a_data["count"] = a_data["count"]+1

                                        elif question_obj.q_type in [Question.SINGLE, Question.MULTI]:
                                            for a_data in r_data["answers"]:
                                                for a in sitting_data["answer"]:
                                                    if a == a_data["answer"].id:
                                                        a_data["count"] = a_data["count"]+1

                                        elif question_obj.q_type == Question.DRAW:
                                            sittingimage = SittingImage.objects.get(id=sitting_data["answer"])
                                            image_obj = {"answer": sittingimage.image}
                                            r_data["answers"].append(image_obj)
                            else:
                                # if the question.id doesn't exist in the report yet,
                                # create the report data for that question
                                question_obj = Question.objects.get(id=sitting_data["question"])
                                new_r_data = {"question": question_obj, "answers":[]}

                                if question_obj.q_type == Question.ANSWER:
                                    if sitting_data["is_correct"]:
                                        a_data = {"answer": sitting_data["answer"], "count": 1}
                                        new_r_data["answers"].append(a_data)
                                elif question_obj.q_type in [Question.SINGLE, Question.MULTI]:
                                    for answerchoice in question_obj.answerchoice_set.all():
                                        a_data = {"answer": answerchoice}
                                        for a in sitting_data["answer"]:
                                            answer_obj = AnswerChoice.objects.get(id=a)
                                            if answer_obj == answerchoice:
                                                a_data['count'] = 1
                                        new_r_data["answers"].append(a_data)

                                elif question_obj.q_type == Question.DRAW:
                                    sittingimage = SittingImage.objects.get(id=sitting_data["answer"])
                                    image_obj = {"answer": sittingimage.image}
                                    new_r_data["answers"].append(image_obj)

                                data_obj["report"].append(new_r_data)

                else:
                    # if this is the first loop, means no report data,
                    # then, create the report data by looping through the answers
                    # of the first sitting
                    if s.answers:
                        for sitting_data in s.answers:
                            question_obj = Question.objects.get(id=sitting_data["question"])
                            new_r_data = {"question": question_obj, "answers":[]}
                            if question_obj.q_type == Question.ANSWER:
                                if sitting_data["is_correct"]:
                                    a_data = {"answer": sitting_data["answer"], "count": 1}
                                    new_r_data["answers"].append(a_data)

                            elif question_obj.q_type in [Question.SINGLE, Question.MULTI]:
                                for answerchoice in question_obj.answerchoice_set.all():
                                    a_data = {"answer": answerchoice, "count": 0}
                                    for a in sitting_data["answer"]:
                                        answer_obj = AnswerChoice.objects.get(id=a)
                                        if answer_obj == answerchoice:
                                            a_data['count'] = 1
                                    new_r_data["answers"].append(a_data)
                                    print a_data
                            elif question_obj.q_type == Question.DRAW:
                                sittingimage = SittingImage.objects.get(id=sitting_data["answer"])
                                image_obj = {"answer": sittingimage.image}
                                new_r_data["answers"].append(image_obj)

                            data_obj["report"].append(new_r_data)

            data.append(data_obj)
        return data

    def generate_serial_no(self):
        if not self.serial_no and self.schooluser:
            if self.serial_no == 0:
                return self.serial_no
            else:
                if self.is_trial:
                    accounts = self.schooluser.get_trial_accounts().exclude(serial_no__isnull=True)
                else:
                    accounts = self.schooluser.get_full_accounts().exclude(serial_no__isnull=True)
                if accounts:
                    last_serial_no = accounts.latest("serial_no")
                    if last_serial_no.serial_no:
                        next_serial_no = last_serial_no.serial_no + 1
                    else:
                        next_serial_no = 1
                else:
                    next_serial_no = 1
                return next_serial_no
        else:
            return self.serial_no


    def save(self, *args, **kwargs):
        if not self.user_id:
            random_string = get_random_string(length=10)
            if self.id:
                exist_masteruser = MasterUser.objects.filter(user_id__exact=random_string).exclude(id=self.id)
            else:
                exist_masteruser = MasterUser.objects.filter(user_id__exact=random_string)

            while exist_masteruser:
                random_string = get_random_string(length=10)
                if self.id:
                    exist_masteruser = MasterUser.objects.filter(user_id__exact=random_string).exclude(id=self.id)
                else:
                    exist_masteruser = MasterUser.objects.filter(user_id__exact=random_string)

            self.user_id = random_string

        if self.username and self.schooluser:
            school_username = "%s@%s" % (self.username, self.schooluser.school_id)
        else:
            school_username = None

        if not hasattr(self, "auth_user"):
            if self.email:
                if school_username:
                    user = User(username=school_username, email=self.email)
                elif not school_username and self.username:
                    user = User(username=self.username, email=self.email)
                else:
                    user = User(username=self.user_id, email=self.email)

                if self.schooluser:
                    user.is_active = False
                user.set_unusable_password()
                user.save()
            else:
                if school_username:
                    user = User(username=school_username)
                elif not school_username and self.username:
                    user = User(username=self.username)
                else:
                    user = User(username=self.user_id)

                if self.schooluser:
                    user.is_active = False
                user.set_unusable_password()
                user.save()

            self.auth_user = user
        else:
            # if this MasterUser belongs to a School, change the auth_user
            # username to school_username format
            if school_username and self.auth_user.username != school_username:
                self.auth_user.username = school_username
                self.auth_user.save()

            if not self.email and self.auth_user.email and self.auth_user.email != "":
                self.email = self.auth_user.email
            else:
                if self.email and self.email != self.auth_user.email:
                    self.auth_user.email = self.email
                    self.auth_user.save()

        if self.schooluser and self.auth_user and self.auth_user.is_active:
            if self.is_trial:
                if self.trial_start_date:
                    self.trial_end_date = self.trial_start_date + relativedelta(months=+config.TRIAL_EXPIRE_MONTHS)
            else:
                if self.is_free_full:
                    if self.trial_start_date:
                        self.trial_end_date = self.trial_start_date + relativedelta(months=+config.TRIAL_EXPIRE_MONTHS)
                else:
                    self.trial_start_date = None
                    self.trial_end_date = None


        if self.schooluser:
            self.serial_no = self.generate_serial_no()
            if self.is_trial:
                self.serial_no_display = "%sD%s" % (self.schooluser.school_id, str(self.serial_no).zfill(4))
            else:
                self.serial_no_display = "%s%s" % (self.schooluser.school_id, str(self.serial_no).zfill(4))

        super(MasterUser, self).save(*args, **kwargs)
        # self.save_last_activity()



class AccountPackage(TimeStampedModel):
    title = models.CharField(_('Package Name'), max_length=200, blank=True, null=True)
    trial_accounts_num = models.IntegerField(_('No. of Trial Accounts'), blank=True, null=True, default=20)
    full_accounts_num = models.IntegerField(_('No. of Full Accounts'), blank=True, null=True, default=20)
    renewal_years = models.IntegerField(_('Years until renewal'), blank=True, null=True, default=1)
    renewal_months = models.IntegerField(_('Months until renewal'), blank=True, null=True, default=0)
    renewal_days = models.IntegerField(_('Days until renewal'), blank=True, null=True, default=0)
    rank = models.IntegerField(_('Rank'), blank=True, null=True, default=0)
    is_active = models.BooleanField(_('Is Active'), default=True)

    class Meta:
        ordering = ('rank',)

    def __unicode__(self):
        return "%s" % self.title

    def show_renewal_duration(self):
        renewal_str = ""
        if self.renewal_years:
            renewal_str += "%s Year " % self.renewal_years if self.renewal_years == 1 else "%s Years " % self.renewal_years
        if self.renewal_months:
            renewal_str += "%s Month " % self.renewal_months if self.renewal_months == 1 else "%s Months " % self.renewal_months
        if self.renewal_days:
            renewal_str += "%s Day " % self.renewal_days if self.renewal_days == 1 else "%s Days " % self.renewal_days
        return renewal_str


class DemoLicenseRequest(TimeStampedModel):
    FOLLOWUP = 1
    DECLINED = 2
    APPROVED = 3

    STATUS_LIST = (
        (FOLLOWUP, _("Follow-up")),
        (DECLINED, _("Declined")),
        (APPROVED, _("Approved")),
    )

    demo_schooluser = models.ForeignKey("users.SchoolUser", verbose_name=_('SchoolUser'),
                blank=True, null=True)
    demo_accounts_num = models.IntegerField(_('No. of Demo Trial Accounts Requested'),
                blank=True, null=True)
    demo_start_date = models.DateTimeField(_('Trial Start Date (Demo)'),
                blank=True, null=True)
    demo_end_date = models.DateTimeField(_('Trial End Date (Demo)'),
                blank=True, null=True)
    demo_status = models.IntegerField(_("Status"), choices=STATUS_LIST,
                default=FOLLOWUP, blank=True, null=True)
    is_generated = models.BooleanField(_("Is Generated"), default=False)

    def save(self, *args, **kwargs):
        # If request is just created
        if not self.id:
            schoolname = self.demo_schooluser.school if self.demo_schooluser.school else self.demo_schooluser.full_name
            send_more_demo_license_request_email(self, schoolname)

        if self.demo_start_date:
            self.demo_end_date = self.demo_start_date + relativedelta(months=+config.TRIAL_EXPIRE_MONTHS)
        if self.demo_status == self.APPROVED and not self.is_generated:
            gameplatforms = GamePlatform.objects.filter(game__id=1, platform__id=7)
            for i in range(0, self.demo_accounts_num):
                masteruser = MasterUser.objects.create(schooluser=self.demo_schooluser,
                    is_trial=True, is_expired_account=False,
                    trial_start_date=self.demo_start_date,
                    trial_end_date=self.demo_end_date)
                masteruser.gameplatforms.add(*gameplatforms)
            self.is_generated = True

            if self.id:
                demo_license_approve_email(self)
        super(DemoLicenseRequest, self).save(*args, **kwargs)


class FreeFullLicenseRequest(TimeStampedModel):
    FOLLOWUP = 1
    DECLINED = 2
    APPROVED = 3

    STATUS_LIST = (
        (FOLLOWUP, _("Follow-up")),
        (DECLINED, _("Declined")),
        (APPROVED, _("Approved")),
    )

    freefull_schooluser = models.ForeignKey("users.SchoolUser", verbose_name=_('SchoolUser'),
                blank=True, null=True)
    freefull_accounts_num = models.IntegerField(_('No. of Full Trial Accounts Requested'),
                blank=True, null=True)
    freefull_start_date = models.DateTimeField(_('Trial Start Date (Full)'),
                blank=True, null=True)
    freefull_end_date = models.DateTimeField(_('Trial End Date (Full)'),
                blank=True, null=True)
    freefull_status = models.IntegerField(_("Status"), choices=STATUS_LIST,
                default=FOLLOWUP, blank=True, null=True)
    is_generated = models.BooleanField(_("Is Generated"), default=False)

    def save(self, *args, **kwargs):
        # If request is just created
        if not self.id:
            schoolname = self.freefull_schooluser.school if self.freefull_schooluser.school else self.freefull_schooluser.full_name
            send_more_freefull_license_request_email(self, schoolname)

        if self.freefull_start_date:
            self.freefull_end_date = self.freefull_start_date + relativedelta(months=+config.TRIAL_EXPIRE_MONTHS)
        if self.freefull_status == self.APPROVED and not self.is_generated:
            gameplatforms = GamePlatform.objects.filter(game__id=1, platform__id__in=[8,9,10,11])
            for i in range(0, self.freefull_accounts_num):
                masteruser = MasterUser.objects.create(schooluser=self.freefull_schooluser,
                    is_trial=False, is_free_full=True, is_expired_account=False,
                    trial_start_date=self.freefull_start_date,
                    trial_end_date=self.freefull_end_date)
                masteruser.gameplatforms.add(*gameplatforms)
            self.is_generated = True

            if self.id:
                freefull_license_approve_email(self)
        super(FreeFullLicenseRequest, self).save(*args, **kwargs)


class SchoolUser(TimeStampedModel):
    FOLLOWUP = 1
    WAITING = 2
    DECLINED = 3
    BLACKLISTED = 4
    APPROVED = 5

    STATUS_LIST = (
        (FOLLOWUP, _("Follow-up")),
        (WAITING, _("Waiting List")),
        (DECLINED, _("Declined")),
        (BLACKLISTED, _("Blacklisted")),
        (APPROVED, _("Approved")),
    )

    NORMAL = 1
    ACADEMY = 2
    PROJ = 3
    SMALL = 4
    SCHOOL_IN_SCHOOL = 5
    HOME_SCHOOL = 6
    ONLINE_SCHOOL = 7
    OTHERS = 8

    ORG_LIST = (
        (NORMAL, _("Departmental (Normal)")),
        (ACADEMY, _("Academy")),
        (PROJ, _("Project Based Learning")),
        (SMALL, _("Small Learning Communities")),
        (SCHOOL_IN_SCHOOL, _("School within a School")),
        (HOME_SCHOOL, _("Home School")),
        (ONLINE_SCHOOL, _("Online School")),
        (OTHERS, _("Others")),
    )

    GOOGLE = 1
    PERSON = 2

    SOURCE_LIST = (
        (GOOGLE, _("Google")),
        (PERSON, _("Person")),
    )

    auth_user = models.OneToOneField("auth.User", unique=True,
                on_delete=models.SET_NULL, related_name='schooluser', null=True, editable=False)

    # Credentials
    full_name = models.CharField(_('Full Name'), max_length=400,
                blank=True, null=True)
    email = models.EmailField(_('Email'),
                blank=True, null=True)
    school = models.CharField(_('School Name'), max_length=200,
                blank=True, null=True)
    state = models.CharField(_('State'), max_length=300,
                blank=True, null=True)
    country = CountryField(blank_label='(select country)')

    position = models.CharField(_('Position'), max_length=300,
                blank=True, null=True)
    position_fk = models.ForeignKey("users.Position", verbose_name=_('Position'),
                blank=True, null=True)
    timezone = TimeZoneField(default=settings.TIME_ZONE, null=True, blank=True)

    # School Info
    school_type = models.ForeignKey("users.SchoolType", verbose_name=_('Type of School'),
                blank=True, null=True)
    curriculums = models.ManyToManyField("users.Curriculum", verbose_name=_("Curriculum"),
                    related_name="curriculum_schools", blank=True)

    # School User subscription details
    total_students_num = models.IntegerField(_('Total Number of Students'),
                blank=True, null=True)
    school_id = models.CharField(_('School ID'), max_length=300,
                blank=True, null=True)
    game_download_url = models.URLField(_('Game Download URL'),
                blank=True, null=True)
    android_download_url = models.URLField(_('Android Version Download URL'),
                blank=True, null=True)

    # Obsolete
    trial_accounts_num = models.IntegerField(_('No. of Demo Trial Accounts Owned'),
                blank=True, null=True)
    full_accounts_num = models.IntegerField(_('No. of Full Trial Accounts Owned'),
                blank=True, null=True)
    status = models.IntegerField(_("Status"), choices=STATUS_LIST,
                default=FOLLOWUP, blank=True, null=True)
    is_approved = models.BooleanField(_('Is Approved'),
                default=False)
    requested_trial_accounts_num = models.IntegerField(_('No. of Demo Trial Accounts Requested'),
                blank=True, null=True)
    requested_full_accounts_num = models.IntegerField(_('No. of Full Trial Accounts Requested'),
                blank=True, null=True)
    trial_start_date = models.DateTimeField(_('Trial Start Date (Demo)'),
                blank=True, null=True)
    trial_end_date = models.DateTimeField(_('Trial End Date (Demo)'),
                blank=True, null=True)
    full_start_date = models.DateTimeField(_('Trial Start Date (Full)'),
                blank=True, null=True)
    full_end_date = models.DateTimeField(_('Trial End Date (Full)'),
                blank=True, null=True)
    start_date = models.DateTimeField(_('Subscription Start Date'),
                blank=True, null=True)
    end_date = models.DateTimeField(_('Subscription End Date'),
                blank=True, null=True)
    first_name = models.CharField(_('Contact Person\'s First Name'), max_length=200,
                blank=True, null=True)
    last_name = models.CharField(_('Contact Person\'s Last Name'), max_length=200,
                blank=True, null=True)
    schoolgroup = models.CharField(_('School Group Name'), max_length=200,
                blank=True, null=True)
    address1 = models.CharField(_('Address Line 1'), max_length=500,
                blank=True, null=True)
    address2 = models.CharField(_('Address Line 2'), max_length=500,
                blank=True, null=True)
    postcode = models.CharField(_('Postcode'), max_length=80,
                blank=True, null=True)
    account_package = models.ForeignKey("users.AccountPackage", verbose_name=_('Account Package'),
                blank=True, null=True)
    organisation_model = models.IntegerField(_("Organisation Model"), choices=ORG_LIST,
                default=NORMAL, blank=True, null=True)
    level_of_edu = models.ManyToManyField("users.LevelOfEducation", verbose_name=_("Level of Education Available"),
                related_name="levelofedu_schools", blank=True)
    facilities = models.ManyToManyField("users.Facility", verbose_name=_("Facilities"),
                related_name="facility_schools", blank=True)
    extracurricular = models.ManyToManyField("users.ExtraCurricular", verbose_name=_("Extra-Curricular"),
                related_name="fextracurricular_schools", blank=True)
    events = models.ManyToManyField("users.Event", verbose_name=_("Events/Conventions"),
                    related_name="event_schools", blank=True)
    examinations = models.ManyToManyField("users.Examination", verbose_name=_("Examination"),
                    related_name="examination_schools", blank=True)
    trial_students_num = models.IntegerField(_('Number of Students for Trial'),
                blank=True, null=True)
    total_teachers_num = models.IntegerField(_('Total Number of Teachers'),
                blank=True, null=True)
    trial_teachers_num = models.IntegerField(_('Number of Teachers for Trial'),
                blank=True, null=True)
    intended_trial_allocated_time = models.IntegerField(_('Allocated Time for Trial Session (minutes)'),
                blank=True, null=True)
    source = models.IntegerField(_("Where did you find out about this program from?"), choices=SOURCE_LIST,
                blank=True, null=True)
    source_text = models.CharField(_("Where did you find out about this program from?"), max_length=500,
                blank=True, null=True)

    def __unicode__(self):
        return "%s (%s)" % (self.school, self.email)

    def show_full_address(self):
        address_list = []
        if self.address1:
            address_list.append(self.address1)
        if self.address2:
            address_list.append(self.address2)
        if self.postcode:
            address_list.append(self.postcode)
        if self.state:
            address_list.append(self.state)
        if self.country:
            address_list.append(self.country.name)

        address_str = ", ".join(address_list)
        return address_str

    def get_school_slug(self):
        school_words_list = self.school.split(' ')
        school_slug = "-".join(school_words_list)
        return school_slug.title()

    def get_curriculums_str(self):
        string = ""
        if self.curriculums.all():
            curriculums_list = self.curriculums.all().values_list("title", flat=True)
            string = ",".join(curriculums_list)
        return string

    def generate_school_id(self):
        if self.school and self.country:
            running_number = 1
            school_words_list = self.school.split(' ')
            school_initials = ""
            for word in school_words_list:
                school_initials += word[0].upper()
            school_id = "%s%s%s" % (self.country.code, school_initials, running_number)

            school_id_exist = True
            while school_id_exist:
                if self.id:
                    exist_schooluser = SchoolUser.objects.filter(school_id__exact=school_id).exclude(id=self.id)
                else:
                    exist_schooluser = SchoolUser.objects.filter(school_id__exact=school_id)

                if exist_schooluser:
                    running_number += 1
                    school_id = "%s%s%s" % (self.country.code, school_initials, running_number)
                else:
                    school_id_exist = False

            return school_id
        else:
            return None

    def is_profile_complete(self):
        complete = True
        if not self.full_name or not self.school or not self.state or not self.country or not self.position_fk or not self.school_type or not self.curriculums.all() or not self.total_students_num:
            complete = False
        return complete

    def save(self, *args, **kwargs):
        if not self.school_id:
            self.school_id = self.generate_school_id()

        if self.full_name:
            shortened_name = self.full_name[:30]
        else:
            shortened_name = ""

        if not self.auth_user:
            # Create User if there are no existing Users that are
            # using this email
            exist_user = User.objects.filter(email=self.email)
            if exist_user and hasattr(exist_user, "schooluser") and exist_user.schooluser:
                pass
            elif exist_user and hasattr(exist_user, "masteruser") and exist_user.masteruser:
                # allow create new user if user already belongs to MasterUser
                user = User(email=self.email)
                user.set_unusable_password()
                user.first_name = shortened_name
                user.is_active = False
                user.save()
                self.auth_user = user
                send_activation_email(user)
            else:
                if exist_user:
                    # If another user existed but not tied to SchoolUser or MasterUser
                    exist_user_obj = exist_user.first()
                    exist_user_obj.set_unusable_password()
                    exist_user_obj.first_name = shortened_name
                    exist_user_obj.is_active = False
                    exist_user_obj.save()
                    self.auth_user = exist_user_obj
                    send_activation_email(self.auth_user)
                else:
                    # If no such User exists
                    user = User(email=self.email)
                    user.set_unusable_password()
                    user.first_name = shortened_name
                    user.is_active = False
                    user.save()
                    self.auth_user = user
                    send_activation_email(user)

        super(SchoolUser, self).save(*args, **kwargs)


    """ all accounts """
    def get_all_accounts(self):
        all_accounts = self.masteruser_set.filter(is_active=True).order_by('-auth_user__is_active')
        return all_accounts

    def get_all_accounts_invited(self):
        all_accounts = self.get_all_accounts()
        if all_accounts:
            invited = all_accounts.filter(full_name__isnull=False, email__isnull=False)
        else:
            invited = all_accounts
        return invited

    """ full accounts """
    def get_full_accounts(self):
        full_accounts = self.masteruser_set.filter(is_trial=False).order_by('-auth_user__is_active')
        return full_accounts

    def get_full_accounts_invited(self):
        full_accounts = self.get_full_accounts()
        if full_accounts:
            invited = full_accounts.filter(full_name__isnull=False, email__isnull=False)
        else:
            invited = full_accounts
        return invited

    def get_full_accounts_activated(self):
        full_accounts = self.get_full_accounts()
        if full_accounts:
            activated = full_accounts.filter(auth_user__is_active=True)
        else:
            activated = full_accounts
        return activated

    def get_full_accounts_remaining(self):
        full_accounts = self.get_full_accounts()
        if full_accounts:
            invited_ids = self.get_full_accounts_invited().values_list("id", flat=True)
            activated_ids = self.get_full_accounts_activated().values_list("id", flat=True)
            remaining = full_accounts.exclude(id__in=invited_ids).exclude(id__in=activated_ids)
        else:
            remaining = full_accounts
        return remaining

    def get_full_accounts_left(self):
        full_accounts_left = self.full_accounts_num - self.get_full_accounts().count()
        return full_accounts_left

    """ trial accounts """
    def get_trial_accounts(self):
        trial_accounts = self.masteruser_set.filter(is_trial=True).order_by('-auth_user__is_active')
        return trial_accounts

    def get_trial_accounts_invited(self):
        trial_accounts = self.get_trial_accounts()
        if trial_accounts:
            invited = trial_accounts.filter(full_name__isnull=False, email__isnull=False)
        else:
            invited = trial_accounts
        return invited

    def get_trial_accounts_activated(self):
        trial_accounts = self.get_trial_accounts()
        if trial_accounts:
            activated = trial_accounts.filter(auth_user__is_active=True)
        else:
            activated = trial_accounts
        return activated

    def get_trial_accounts_remaining(self):
        trial_accounts = self.get_trial_accounts()
        if trial_accounts:
            invited_ids = self.get_trial_accounts_invited().values_list("id", flat=True)
            activated_ids = self.get_trial_accounts_activated().values_list("id", flat=True)
            remaining = trial_accounts.exclude(id__in=invited_ids).exclude(id__in=activated_ids)
        else:
            remaining = trial_accounts
        return remaining

    def get_trial_accounts_left(self):
        trial_accounts_left = self.trial_accounts_num - self.get_trial_accounts().count()
        return trial_accounts_left



post_save.connect(user_postsave, sender=User)

user_logged_in.connect(login_user)
user_logged_out.connect(logout_user)


from django.core.validators import validate_email
from django.core.exceptions import ValidationError
@receiver(pre_save, sender=User)
def sync_email_to_username(sender, instance, *args, **kwargs):
    if not instance.username:
        try:
            validate_email(instance.email)
            instance.username = instance.email
            instance.save()
        except ValidationError:
            pass











'''
For reference only.
Sample report data:
[
    {"quiz": <Quiz: obj>,
    "sittings": 4
    "report": [
            {"question": <Question: obj>,
            "answers": [
                {"answer": <AnswerChoice: obj>, "count": 2},
                {"answer": <AnswerChoice: obj>, "count": 3}
                ]
            },
            {"question": <Question: obj>,
            "answers": [
                {"answer": "Answer text", "count": 1},
                ]
            },
            {"question": <Question: obj>,
            "answers": [
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                ]
            },
        ]
    },
    {"quiz": <Quiz: obj>,
    "sittings": 4
    "report": [
            {"question": <Question: obj>,
            "answers": [
                {"answer": <AnswerChoice: obj>, "count": 2},
                {"answer": <AnswerChoice: obj>, "count": 3}
                ]
            },
            {"question": <Question: obj>,
            "answers": [
                {"answer": "Answer text", "count": 1},
                ]
            },
            {"question": <Question: obj>,
            "answers": [
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                {"answer": <ImageFieldFile: image-url>},
                ]
            },
        ]
    },
]
'''
# pp.pprint(data)
