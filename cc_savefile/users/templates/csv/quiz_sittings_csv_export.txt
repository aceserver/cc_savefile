{% for quiz in quizzes %}"{{ quiz.quiz.title }}"
{% for h in quiz.header_list %}"{{ h }}",{% endfor %}{% for q in quiz.question_list %}"{{ q.content }}"{% if not forloop.last %},{% endif %}{% endfor %}
{% for sitting in quiz.sitting_answers %}"{{ sitting.username }}","{{ sitting.email }}","{{ sitting.active }}","{{ sitting.date }}","{{ sitting.time }}",{% for answer in sitting.answers %}"{{ answer }}"{% if not forloop.last %},{% endif %}{% endfor %}
{% endfor %}
""
{% endfor %}
