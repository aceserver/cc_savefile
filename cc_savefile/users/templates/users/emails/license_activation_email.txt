You are one step closer to activating your ChemCaper license!
Hi {{ masteruser.full_name }},
You have just redeemed your ChemCaper redeem code: {{ masteruser.user_id }}
To activate your license, click on the link below.
{{ activation_url }}
*Please activate your account within {{ expiration_days }} days.

{% if masteruser.schooluser and masteruser.schooluser.game_download_url %}
Once your license is activated, you may download the Windows version of ChemCaper from this download link:{{ masteruser.schooluser.game_download_url }}
{% endif %}

{% if masteruser.schooluser %} 
Once your license is activated, you may download the game from the link(s) below:
{% for gameplatform in masteruser.gameplatforms.all %}
	{{ gameplatform.platform.title }}: {{ gameplatform.download_url }}
{% endfor %}

{% if masteruser.schooluser.game_download_url %}
	{{ masteruser.schooluser.game_download_url }}
{% endif %}
{% endif %}

After installing ChemCaper, just launch the game and login using your username ({{ masteruser.username }}) to start playing!

Need help? Just drop us an email at support@chemcaper.com

