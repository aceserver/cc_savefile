Your request for ChemCaper Full Version Trial Licenses has been approved

Hi {{ license_request_obj.freefull_schooluser.full_name }},

You have been given {{ license_request_obj.freefull_accounts_num }} ChemCaper Full Version Trial Licenses to be started on {{ license_request_obj.freefull_start_date|date:'d/m/Y' }}.

Start experiencing ChemCaper by inviting users through the Dashboard.

Need help? Just drop us an email at support@chemcaper.com