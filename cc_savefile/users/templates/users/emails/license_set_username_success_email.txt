You have just activated your ChemCaper license!

Hi {{ masteruser.full_name }},

{% if masteruser.schooluser and masteruser.schooluser.game_download_url %}
If you have not downloaded the game yet, you may download the Windows version of ChemCaper from this download link:{{ masteruser.schooluser.game_download_url }}
{% endif %}

After installing ChemCaper, just launch the game and login using your username ({{ masteruser.username }}) to start playing!

Need help? Just drop us an email at support@chemcaper.com