You have requested for more ChemCaper Demo Version Licenses

Hi {{ license_request_obj.demo_schooluser.full_name }},

You have just requested for {{ license_request_obj.demo_accounts_num }} ChemCaper Demo Licenses to be started on {{ license_request_obj.demo_start_date|date:'d/m/Y' }}.

Please wait at least 24 hours for our team to approve your request.

Need help? Just drop us an email at support@chemcaper.com