You have received a registration for ChemCaper EDU.
User: {{ schooluser.get_full_name }}
Email: {{ schooluser.email }}
{% if schooluser %}School: {{ schooluser.school }}{% endif %}
Check out the registration details in the admin panel.