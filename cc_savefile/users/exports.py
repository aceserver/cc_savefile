import pprint
# import pytz
from django.template import loader, Context
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.timezone import localtime

from quiz.models import Sitting, SittingImage, Question, Quiz

def license_import_template_csv_export():
    try:
        c = Context({
                "license": []
            })
        return render_to_string('csv/license_import_template.txt', c, )
    except Exception as e:
        print e
        return None


def license_export_csv(license_list):
    try:
        c = Context({
                "license_list": license_list,
            })
        return render_to_string('csv/license_export_csv_template.txt', c, )
    except Exception as e:
        print e
        return None


def quiz_sittings_csv_export(request, trial_accounts):
    pp = pprint.PrettyPrinter(indent=2)
    try:
        sittings = Sitting.objects.filter(masteruser__in=trial_accounts)
        quizzes = Quiz.objects.filter(id__in=sittings.values_list("quiz", flat=True))
        c = {"quizzes": []}

        for quiz in quizzes:
            quiz_data = {"quiz": quiz,
                        "header_list": ["username","email","active","date","time"],
                        "question_list": [],
                        "sitting_answers": []
                }

            quiz_sittings = sittings.filter(quiz=quiz)

            for s in quiz_sittings:
                for data in s.show_answers():
                    if not data["question"] in quiz_data["question_list"]:
                        quiz_data["question_list"].append(data["question"])

            for s in quiz_sittings:
                created_date = localtime(s.created)
                sitting_data = {"username": s.masteruser.username,
                    "email": s.masteruser.email,
                    "active": "Active" if s.masteruser.auth_user.is_active else "Inactive",
                    "date": created_date.strftime("%d/%m/%Y"),
                    "time": created_date.strftime("%I:%M %p"),
                    "answers": []}

                for question in quiz_data["question_list"]:
                    answer_data = s.find_answer(question)
                    if answer_data != "":
                        if question.q_type == Question.DRAW:
                            answer = str(request.build_absolute_uri(answer_data["answer"].url))
                        elif question.q_type == Question.ANSWER:
                            answer = str(answer_data["answer"])
                        else:
                            answer_str_list = [str(a.content) for a in answer_data["answer"]]
                            answer = "/".join(answer_str_list)
                    else:
                        answer = answer_data
                    sitting_data["answers"].append(answer)

                quiz_data["sitting_answers"].append(sitting_data)

            c["quizzes"].append(quiz_data)

        # pp.pprint(c)

        context = Context(c)
        return render_to_string('csv/quiz_sittings_csv_export.txt', context, )
    except Exception as e:
        print e
        return None



def gamesession_export_csv(license_list):
    try:
        c = Context({
                "license_list": license_list,
            })
        return render_to_string('csv/gamesession_csv_export_template.txt', c, )
    except Exception as e:
        print e
        return None

