from django.contrib.auth import (authenticate, get_user_model, password_validation)
from django.core.validators import validate_email
from django.utils.dateformat import DateFormat
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.exceptions import ValidationError
from djoser import constants, utils, settings
from rest_framework import serializers, authtoken
from usernames import is_safe_username
from gamesaves.models import Game, Platform, GamePlatform
import logging

logger = logging.getLogger("ccserver")

from ..models import MasterUser

User = get_user_model()


class MasterUserLoginSerializer(serializers.Serializer):
    user_name = serializers.CharField(required=False, style={'input_type': 'user_name'})
    password = serializers.CharField(required=False, style={'input_type': 'password'})
    game = serializers.CharField(required=False, style={'input_type': 'game'})
    platform = serializers.CharField(required=False, style={'input_type': 'platform'})

    error_message = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        'trial_account': 'This is a trial account. You have no access to login to the full version of the game.',
        'logged_in': 'This account is already logged in. Please login using another username.',
        # 'invalid_credentials': constants.INVALID_CREDENTIALS_ERROR,
        'invalid_credentials': 'This username does not exist. Please login with another username.',
        'password_incorrect': 'Incorrect password. Please try again.',
        'no_access': 'You do not have access to this game and platform.',
    }

    def __init__(self, *args, **kwargs):
        super(MasterUserLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None
        self.game = None
        self.platform = None

    def attempt_login(self, user):
        if user.masteruser.check_is_logged_in():
            raise serializers.ValidationError(self.error_message['logged_in'])
        else:
            if authtoken.models.Token.objects.filter(user=user):
                authtoken.models.Token.objects.filter(user=user).delete()
        return user

    def validate(self, attrs):
        try:
            self.user = authenticate(user_name=attrs.get('user_name'), password=attrs.get('password'))

            if self.user:
                if not self.user.is_active:
                    raise serializers.ValidationError(self.error_message['inactive_account'])

                    # TEMP DEPRECEATED
                    # elif self.user.masteruser.is_trial:
                    #     raise serializers.ValidationError(self.error_message['trial_account'])
                else:
                    # Check if MasterUser can log in to this game-platform
                    self.game = Game.objects.get(id=attrs.get('game'))
                    self.platform = Platform.objects.get(id=attrs.get('platform'))
                    gameplatform_obj = GamePlatform.objects.filter(game=self.game, platform=self.platform).first()

                    if self.user.masteruser.schooluser:
                        gameplatforms = self.user.masteruser.gameplatforms.all()
                        if gameplatform_obj and gameplatforms and gameplatform_obj in gameplatforms:
                            self.attempt_login(self.user)
                        else:
                            raise serializers.ValidationError(self.error_message['no_access'])
                    else:
                        if not self.platform.is_edu:
                            self.attempt_login(self.user)
                        else:
                            raise serializers.ValidationError(self.error_message['no_access'])
                return attrs
            else:
                raise serializers.ValidationError(self.error_message['password_incorrect'])
        except Exception as e:
            error_msg = '%s' % e
            raise serializers.ValidationError(error_msg)


class MasterUserTrialLoginSerializer(serializers.Serializer):
    """For MasterUsers that belongs to a school, and is a trial account"""
    user_name = serializers.CharField(required=False, style={'input_type': 'user_name'})
    school = serializers.CharField(required=False, style={'input_type': 'school'})

    error_message = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        # 'invalid_credentials': constants.INVALID_CREDENTIALS_ERROR,
        'invalid_credentials': 'This username does not exist. Please login with another username.',
        'logged_in': 'This account is already logged in. Please login using another username.',
        'different_school': 'This account cannot be used to log in to this school.',
    }

    def __init__(self, *args, **kwargs):
        super(MasterUserTrialLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):
        self.user = authenticate(user_name=attrs.get('user_name'), school=attrs.get('school'))
        if self.user:
            if not self.user.is_active:
                # self.user.is_active = True
                # if hasattr(self.user, 'masteruser'):
                #     if self.user.masteruser and self.user.masteruser.is_trial:
                #         self.user.masteruser.save()
                # self.user.save()
                raise serializers.ValidationError(self.error_message['inactive_account'])
            else:
                if hasattr(self.user, 'masteruser'):
                    if self.user.masteruser.schooluser.school_id != attrs.get('school'):
                        raise serializers.ValidationError(self.error_message['different_school'])

                    if self.user.masteruser and self.user.masteruser.check_is_logged_in():
                        raise serializers.ValidationError(self.error_message['logged_in'])

                    elif self.user.masteruser and not self.user.masteruser.check_is_logged_in():
                        if authtoken.models.Token.objects.filter(user=self.user):
                            authtoken.models.Token.objects.filter(user=self.user).delete()
            return attrs
        else:
            raise serializers.ValidationError(self.error_message['invalid_credentials'])


class MasterUserSchoolFullLoginSerializer(serializers.Serializer):
    """For MasterUsers that belongs to a school, and is a full access account"""
    user_name = serializers.CharField(required=False, style={'input_type': 'user_name'})
    school = serializers.CharField(required=False, style={'input_type': 'school'})

    error_message = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        # 'inactive_account': 'User account is disabled',
        # 'invalid_credentials': constants.INVALID_CREDENTIALS_ERROR,
        'invalid_credentials': 'This username does not exist. Please login with another username.',
        'logged_in': 'This account is already logged in. Please login using another username.',
        'trial': 'This account cannot be used to log in to the Full Access version.',
        'different_school': 'This account cannot be used to log in to this school.',
    }

    def __init__(self, *args, **kwargs):
        super(MasterUserSchoolFullLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):
        self.user = authenticate(user_name=attrs.get('user_name'), school=attrs.get('school'))
        if self.user:
            if not self.user.is_active:
                # self.user.is_active = True
                # if hasattr(self.user, 'masteruser'):
                #     if self.user.masteruser and not self.user.masteruser.is_trial:
                #         self.user.masteruser.save()
                # self.user.save()
                
                raise serializers.ValidationError(self.error_message['inactive_account'])
            else:
                if hasattr(self.user, 'masteruser'):
                    if self.user.masteruser.is_trial:
                        raise serializers.ValidationError(self.error_message['trial'])

                    if self.user.masteruser.schooluser.school_id != attrs.get('school'):
                        raise serializers.ValidationError(self.error_message['different_school'])

                    if self.user.masteruser and self.user.masteruser.check_is_logged_in():
                        raise serializers.ValidationError(self.error_message['logged_in'])

                    elif self.user.masteruser and not self.user.masteruser.check_is_logged_in():
                        if authtoken.models.Token.objects.filter(user=self.user):
                            authtoken.models.Token.objects.filter(user=self.user).delete()
            return attrs
        else:
            raise serializers.ValidationError(self.error_message['invalid_credentials'])


class MasterUserTeacherGamingLoginSerializer(serializers.Serializer):
    """For MasterUsers that belongs to a school, and is a full access account"""
    user_name = serializers.CharField(required=False, style={'input_type': 'user_name'})
    classroom = serializers.CharField(required=False, style={'input_type': 'classroom'})

    error_message = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        'invalid_credentials': 'This username does not exist. Please login with another username.',
        'logged_in': 'This account is already logged in. Please login using another username.',
    }

    def __init__(self, *args, **kwargs):
        super(MasterUserTeacherGamingLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):
        self.user = authenticate(user_name=attrs.get('user_name'), classroom=attrs.get('classroom'))
        if self.user:
            if not self.user.is_active:
                raise serializers.ValidationError(self.error_message['inactive_account'])
            else:
                if hasattr(self.user, 'masteruser'):
                    if self.user.masteruser and self.user.masteruser.check_is_logged_in():
                        raise serializers.ValidationError(self.error_message['logged_in'])

                    elif self.user.masteruser and not self.user.masteruser.check_is_logged_in():
                        if authtoken.models.Token.objects.filter(user=self.user):
                            authtoken.models.Token.objects.filter(user=self.user).delete()
            return attrs
        else:
            raise serializers.ValidationError(self.error_message['invalid_credentials'])



class MasterUserSteamLoginSerializer(serializers.Serializer):
    steam_id = serializers.CharField(required=False, style={'input_type': 'steam_id'})

    error_message = {
        'inactive_account': constants.INACTIVE_ACCOUNT_ERROR,
        'invalid_credentials': 'This username does not exist. Please login with another username.',
        'logged_in': 'This account is already logged in. Please login using another username.',
    }

    def __init__(self, *args, **kwargs):
        super(MasterUserSteamLoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):
        self.user = authenticate(steam_id=attrs.get('steam_id'))
        if self.user:
            if not self.user.is_active:
                raise serializers.ValidationError(self.error_message['inactive_account'])
            else:
                if hasattr(self.user, 'masteruser'):
                    if self.user.masteruser and self.user.masteruser.check_is_logged_in():
                        raise serializers.ValidationError(self.error_message['logged_in'])
                    elif self.user.masteruser and not self.user.masteruser.check_is_logged_in():
                        if authtoken.models.Token.objects.filter(user=self.user):
                            authtoken.models.Token.objects.filter(user=self.user).delete()
            return attrs
        else:
            raise serializers.ValidationError(self.error_message['invalid_credentials'])


class MasterUserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'},
                                     write_only=True,
                                     validators=settings.get('PASSWORD_VALIDATORS'))

    error_message = {
        'exist_username': 'This username is already taken.',
        'invalid_username': 'This username is not allowed.',
        'exist_email': 'This email is already taken.',
        'invalid_email': 'This email is not allowed.',
        'invalid_password': 'This password is not allowed.',
    }

    class Meta:
        model = User
        fields = ('username', 'email','password',)

    def validate_username(self, value):
        username = value
        if username:
            if not is_safe_username(username):
                raise serializers.ValidationError(self.error_message['invalid_username'])
            else:
                masteruser_username_exist = MasterUser.objects.filter(username=username)
                if masteruser_username_exist:
                    raise serializers.ValidationError(self.error_message['exist_username'])
        return value

    def validate_email(self, value):
        email = value
        if email:
            try:
                validate_email(email)
                masteruser_email_exist = MasterUser.objects.filter(email=email)
                if masteruser_email_exist:
                    raise serializers.ValidationError(self.error_message['exist_email'])
            except ValidationError:
                raise serializers.ValidationError(self.error_message['invalid_email'])
        return value


    def validate_password(self, value):
        password = value
        try:
            password_validation.validate_password(password)
        except ValidationError:
            raise serializers.ValidationError(self.error_message['invalid_password'])
        return value

    def create(self, validated_data):
        try:
            masteruser = MasterUser.objects.create(username=validated_data['username'], email=validated_data['email'])
            masteruser.auth_user.is_active = False
            masteruser.auth_user.set_password(validated_data['password'])
            masteruser.auth_user.save()
            return masteruser.auth_user
        except Exception as e:
            print "Create user error:", e
            return None


class MasterUserSerializer(serializers.ModelSerializer):
    user_id          = serializers.CharField(required=False, style={'input_type': 'user_id'})
    email            = serializers.CharField(required=False, style={'input_type': 'email'})
    apple_id         = serializers.CharField(required=False, style={'input_type': 'apple_id'})
    googleplay_id    = serializers.CharField(required=False, style={'input_type': 'googleplay_id'})
    gamecenter_id    = serializers.CharField(required=False, style={'input_type': 'gamecenter_id'})
    filament_id      = serializers.CharField(required=False, style={'input_type': 'filament_id'})
    teachergaming_id = serializers.CharField(required=False, style={'input_type': 'teachergaming_id'})
    steam_id         = serializers.CharField(required=False, style={'input_type': 'steam_id'})
    is_expired       = serializers.BooleanField(required=False)
    is_trial         = serializers.BooleanField(required=False)

    class Meta:
        model = MasterUser
        fields = ('user_id', 'email', 'is_expired', 'is_trial', 'apple_id', 'googleplay_id',
            'gamecenter_id', 'filament_id', 'teachergaming_id', 'steam_id')


class MasterUserUpdatePlatformIdSerializer(serializers.ModelSerializer):
    error_message = {
        'existing_steam': 'A MasterUser with this Steam ID already existed.',
        'existing_apple_id': 'A MasterUser with this Apple ID already existed.',
        'existing_googleplay_id': 'A MasterUser with this Google Play ID already existed.',
        'existing_gamecenter_id': 'A MasterUser with this Game Center ID already existed.',
        'existing_filament_id': 'A MasterUser with this Filament ID already existed.',
        'existing_teachergaming_id': 'A MasterUser with this Teacher Gaming ID already existed.',
    }

    class Meta:
        model = MasterUser
        fields = ('apple_id', 'googleplay_id', 'gamecenter_id',
                'filament_id', 'teachergaming_id', 'steam_id')

    def __init__(self, *args, **kwargs):
        super(MasterUserUpdatePlatformIdSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        steam_id = attrs.get('steam_id')
        if steam_id:
            masteruser_exist = MasterUser.objects.filter(steam_id=steam_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_steam'])

        apple_id = attrs.get('apple_id')
        if apple_id:
            masteruser_exist = MasterUser.objects.filter(apple_id=apple_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_apple_id'])

        googleplay_id = attrs.get('googleplay_id')
        if googleplay_id:
            masteruser_exist = MasterUser.objects.filter(googleplay_id=googleplay_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_googleplay_id'])

        gamecenter_id = attrs.get('gamecenter_id')
        if gamecenter_id:
            masteruser_exist = MasterUser.objects.filter(gamecenter_id=gamecenter_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_gamecenter_id'])

        filament_id = attrs.get('filament_id')
        if filament_id:
            masteruser_exist = MasterUser.objects.filter(filament_id=filament_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_filament_id'])

        teachergaming_id = attrs.get('teachergaming_id')
        if teachergaming_id:
            masteruser_exist = MasterUser.objects.filter(teachergaming_id=teachergaming_id).exclude(id=self.instance.id)
            if masteruser_exist:
                raise serializers.ValidationError(self.error_message['existing_teachergaming_id'])


        return attrs


class MasterUserSetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(style={'input_type': 'password'},
                                     write_only=True,
                                     validators=settings.get('PASSWORD_VALIDATORS'))

    error_message = {
        'invalid_password': 'This password is not allowed.',
    }

    def validate_password(self, value):
        password = value
        try:
            password_validation.validate_password(password)
        except ValidationError:
            raise serializers.ValidationError([self.error_message['invalid_password']])
        return value

