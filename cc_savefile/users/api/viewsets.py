import json
import ast
from django.http import Http404, HttpResponse
from django.db import transaction
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from rest_framework import status, viewsets, generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, detail_route, list_route

from djoser import serializers, utils, settings, signals

from cc_savefile.utils import json_response, bulk_logentry_op
from cc_savefile.auth_backends import CustomTokenAuthentication as TokenAuthentication

from constance import config

from ..models import MasterUser
from .serializers import (
    MasterUserLoginSerializer,
    MasterUserTrialLoginSerializer,
    MasterUserSchoolFullLoginSerializer,
    MasterUserTeacherGamingLoginSerializer,
    MasterUserSteamLoginSerializer,
    MasterUserRegistrationSerializer,
    MasterUserSerializer,
    MasterUserUpdatePlatformIdSerializer,
    MasterUserSetPasswordSerializer
)

import logging

logger = logging.getLogger("ccserver")


class MasterUserViewSet(viewsets.ModelViewSet):
    '''
    MasterUser API before login.
    '''
    queryset = MasterUser.objects.all()
    serializer_class = MasterUserSerializer
    permission_classes = (permissions.AllowAny,)

    @list_route(methods=['post'])
    def create_anonymous(self, request):
        masteruser = MasterUser.objects.create(is_trial=False, is_expired=False)
        data = {"user_id": masteruser.user_id}
        return Response(
            data=data,
            status=status.HTTP_200_OK,
        )

    @list_route(methods=['post'])
    def login(self, request):
        self.serializer_class = MasterUserLoginSerializer
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token = utils.login_user(self.request, serializer.user)
            token_serializer_class = serializers.serializers_manager.get('token')
            serializer.user.masteruser.save_last_activity()
            return_data = token_serializer_class(token).data
            return_data["user_id"] = serializer.user.masteruser.user_id
            return Response(
                data=return_data,
                status=status.HTTP_200_OK,
            )
        else:
            # print serializer.errors
            # print serializer.errors['non_field_errors']
            err = ast.literal_eval(serializer.errors['non_field_errors'][0])
            # err = serializer.errors['non_field_errors'][0]
            err_dict = {"non_field_errors": err}
            return Response(
                data={"auth_token": "", "error": err_dict, "user_id": ""},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def authenticate_token(self, request):
        self.authentication_classes = (TokenAuthentication, )
        self.permission_classes = (permissions.IsAuthenticated,)

        try:
            masteruser = self.get_current_user()
            if masteruser.user_id == request.data.get("user_id"):
                return_data = {"success": True, "error": ""}
            else:
                return_data = {"success": False, "error": "User ID doesn't match Token"}
            return Response(
                data=return_data,
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            return_data = {"success": True, "error": e}
            return Response(
                data=return_data,
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def trial_login(self, request):
        self.serializer_class = MasterUserTrialLoginSerializer
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token = utils.login_user(self.request, serializer.user)
            token_serializer_class = serializers.serializers_manager.get('token')
            serializer.user.masteruser.save_last_activity()
            return Response(
                data=token_serializer_class(token).data,
                status=status.HTTP_200_OK,
            )
        else:
            err = ast.literal_eval(serializer.errors['non_field_errors'][0])
            # err = serializer.errors['non_field_errors'][0]
            err_dict = {"non_field_errors": err}
            return Response(
                data={"auth_token": "", "error": err_dict},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def school_full_login(self, request):
        self.serializer_class = MasterUserSchoolFullLoginSerializer
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token = utils.login_user(self.request, serializer.user)
            token_serializer_class = serializers.serializers_manager.get('token')
            serializer.user.masteruser.save_last_activity()
            return Response(
                data=token_serializer_class(token).data,
                status=status.HTTP_200_OK,
            )
        else:
            # For some odd reasons, the ValidationError returned here is
            # ["error message"]
            # Whereas the normal login ValidationError returns
            # ["['error message']"]
            try:
                err = ast.literal_eval(serializer.errors['non_field_errors'][0])
            except Exception as e:
                err = serializer.errors['non_field_errors']
            # err = serializer.errors['non_field_errors'][0]
            err_dict = {"non_field_errors": err}
            return Response(
                data={"auth_token": "", "error": err_dict},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def teachergaming_login(self, request):
        self.serializer_class = MasterUserTeacherGamingLoginSerializer
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token = utils.login_user(self.request, serializer.user)
            token_serializer_class = serializers.serializers_manager.get('token')
            serializer.user.masteruser.save_last_activity()
            return Response(
                data=token_serializer_class(token).data,
                status=status.HTTP_200_OK,
            )
        else:
            err = ast.literal_eval(serializer.errors['non_field_errors'][0])
            # err = serializer.errors['non_field_errors'][0]
            err_dict = {"non_field_errors": err}
            return Response(
                data={"auth_token": "", "error": err_dict},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def steam_login(self, request):
        self.serializer_class = MasterUserSteamLoginSerializer
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token = utils.login_user(self.request, serializer.user)
            token_serializer_class = serializers.serializers_manager.get('token')
            serializer.user.masteruser.save_last_activity()
            return Response(
                data=token_serializer_class(token).data,
                status=status.HTTP_200_OK,
            )
        else:
            return Response(
                data={"auth_token": "", "error": serializer.errors},
                status=status.HTTP_200_OK,
            )


    @list_route(methods=['post'])
    def register(self, request):
        from users.utils import send_acelogin_activation_email

        self.serializer_class = MasterUserRegistrationSerializer
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.create(serializer.validated_data)
            if user:
                send = signals.user_registered.send(sender=self.serializer_class, user=user, request=self.request)
                send_acelogin_activation_email(user.masteruser)
                return Response(
                        data={"error": "", "success": True},
                        status=status.HTTP_200_OK,
                    )
            else:
                return Response(
                        data={"error": {"non_field_errors":["Error occured while creating User."]}, "success": False},
                        status=status.HTTP_200_OK,
                    )
        else:
            return Response(
                data={"error": serializer.errors, "success": False},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def register_external(self, request):
        from users.utils import get_activation_key

        self.serializer_class = MasterUserRegistrationSerializer
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.create(serializer.validated_data)
            if user:
                send = signals.user_registered.send(sender=self.serializer_class, user=user, request=self.request)
                activation_key = get_activation_key(user)
                return Response(
                        data={"error": "", "success": True, "activation_key": activation_key, 'expiration_days': config.LINK_ACTIVATION_DAYS, "username": user.masteruser.username},
                        status=status.HTTP_200_OK,
                    )
            else:
                return Response(
                        data={"error": {"non_field_errors":["Error occured while creating User."]}, "success": False, "activation_key": "", "expiration_days": "", "username": ""},
                        status=status.HTTP_200_OK,
                    )
        else:
            return Response(
                data={"error": serializer.errors, "success": False, "activation_key": "", "expiration_days": "", "username": ""},
                status=status.HTTP_200_OK,
            )

    @list_route(methods=['post'])
    def resend_activation_key(self, request):
        from users.utils import get_activation_key

        data = request.data
        masteruser_email_exist = MasterUser.active_objects.filter(auth_user__email=data.get("email")).first()

        if masteruser_email_exist:
            if not masteruser_email_exist.auth_user.is_active:
                activation_key = get_activation_key(masteruser_email_exist.auth_user)
                return Response(
                        data={"error": "", "success": True, "activation_key": activation_key, "expiration_days": config.LINK_ACTIVATION_DAYS, "username": masteruser_email_exist.username},
                        status=status.HTTP_200_OK,
                    )
            else:
                return Response(
                        data={"error": {"non_field_errors":["This account is already activated. You may login to your game using your username (%s)" % masteruser_email_exist.username]}, "success": False, "activation_key": "", "expiration_days": "", "username": masteruser_email_exist.username},
                        status=status.HTTP_200_OK,
                    )
        else:
            return Response(
                        data={"error": {"non_field_errors":["There are no users with this email."]}, "success": False, "activation_key": "", "expiration_days": "", "username": ""},
                        status=status.HTTP_200_OK,
                    )


    @list_route(methods=['post'])
    def send_forgot_password_key(self, request):
        from users.utils import get_activation_key

        data = request.data
        masteruser_email_exist = MasterUser.active_objects.filter(email=data.get("email")).first()

        if masteruser_email_exist:
            if not masteruser_email_exist.auth_user.is_active:
                return Response(
                        data={"error": {"email":["Please activate your account first."]}, "success": False},
                        status=status.HTTP_200_OK,
                    )
            else:
                forgotpassword_key = get_activation_key(masteruser_email_exist.auth_user, salt='iforgotmypassword')
                return Response(
                        data={"error": "", "success": True, "forgotpassword_key": forgotpassword_key, "username": masteruser_email_exist.username},
                        status=status.HTTP_200_OK,
                    )
        else:
            return Response(
                        data={"error": {"email":["There are no users with this email."]}, "success": False},
                        status=status.HTTP_200_OK,
                    )

    @list_route(methods=['post'])
    def check_user_exist(self, request):
        from users.utils import validate_activation_key

        data = request.data
        keystring = data.get("keystring")
        function = data.get("function")

        if function == "set_password":
            salt = "iforgotmypassword"
            check_max_age = False
        elif function == "activate":
            salt = None
            check_max_age = True

        username, email = validate_activation_key(keystring, salt=salt, check_max_age=check_max_age)
        if username and email:
            masteruser_exist = MasterUser.active_objects.filter(auth_user__email=email, auth_user__username=username).first()
        else:
            masteruser_exist = None

        if masteruser_exist:
            return Response(
                    data={"error": "", "success": True, "full_name": masteruser_exist.full_name, "username": masteruser_exist.username},
                    status=status.HTTP_200_OK,
                )
        else:
            return Response(
                        data={"error": "Invalid link", "success": False},
                        status=status.HTTP_200_OK,
                    )


    @list_route(methods=['post'])
    def set_password(self, request):
        from users.utils import validate_activation_key

        self.serializer_class = MasterUserSetPasswordSerializer
        serializer = self.get_serializer(data=request.data)
        keystring = request.data.get("keystring")
        username, email = validate_activation_key(keystring, salt="iforgotmypassword", check_max_age=False)

        if serializer.is_valid():
            if username and email:
                masteruser_exist = MasterUser.active_objects.filter(auth_user__email=email, auth_user__username=username).first()
            else:
                masteruser_exist = None

            if masteruser_exist:
                masteruser_exist.auth_user.set_password(serializer.validated_data.get("password"))
                masteruser_exist.auth_user.save()

                return Response(
                        data={"error": "", "success": True},
                        status=status.HTTP_200_OK,
                    )
            else:
                return Response(
                            data={"error": {"email":["Invalid set password link. Please use the set password link provided in your email."]}, "success": False},
                            status=status.HTTP_200_OK,
                        )
        else:
            return Response(
                        data={"error": serializer.errors, "success": False},
                        status=status.HTTP_200_OK,
                    )


    @list_route(methods=['post'])
    def activate_license(self, request):
        from users.utils import validate_activation_key

        data = request.data
        activation_key = data.get("activation_key")
        username, email = validate_activation_key(activation_key)
        if username and email:
            masteruser_exist = MasterUser.active_objects.filter(auth_user__email=email, auth_user__username=username).first()

            if masteruser_exist:
                if not masteruser_exist.auth_user.is_active:
                    masteruser_exist.auth_user.is_active = True
                    masteruser_exist.auth_user.save()
                    masteruser_exist.save()

                    return Response(
                            data={"error": "", "success": True, "is_already_activated": False},
                            status=status.HTTP_200_OK,
                        )

                else:
                    return Response(
                            data={"error": "", "success": False, "is_already_activated": True},
                            status=status.HTTP_200_OK,
                        )
            else:
                return Response(
                            data={"error": "User does not exist.", "success": False, "is_already_activated": False},
                            status=status.HTTP_200_OK,
                        )
        else:
            return Response(
                        data={"error": "Invalid activation key", "success": False, "is_already_activated": False},
                        status=status.HTTP_200_OK,
                    )



    def send_activation_email(self, user):
        email_factory = utils.UserActivationEmailFactory.from_request(self.request, user=user)
        email = email_factory.create()
        email.send()

    def send_confirmation_email(self, user):
        email_factory = utils.UserConfirmationEmailFactory.from_request(self.request, user=user)
        email = email_factory.create()
        email.send()

    def get_current_user(self, *args, **kwargs):
        return self.request.user.masteruser

    @list_route(methods=['post'])
    def update_platform_id(self, request):
        self.authentication_classes = (TokenAuthentication, )
        self.permission_classes = (permissions.IsAuthenticated,)
        self.serializer_class = MasterUserUpdatePlatformIdSerializer

        partial = True
        try:
            masteruser = self.get_current_user()
            if masteruser:
                serializer = self.get_serializer(masteruser, data=request.data, partial=partial)

                if serializer.is_valid():
                    serializer.save()

                    if getattr(masteruser, '_prefetched_objects_cache', None):
                        # If 'prefetch_related' has been applied to a queryset, we need to
                        # refresh the instance from the database.
                        masteruser = self.get_object()
                        serializer = self.get_serializer(masteruser)
                        masteruser.save_last_activity()
                    return Response(data=serializer.data, status=status.HTTP_200_OK,)
                else:
                    error_message = serializer.errors
                    return Response(data={"error": error_message}, status=status.HTTP_200_OK,)
            else:
                return Response(data={"error": "Please log in as a valid user to perform this action."}, status=status.HTTP_405_METHOD_NOT_ALLOWED,)
        except Exception as e:
            return Response(data={"error": e}, status=status.HTTP_200_OK,)

    @list_route(methods=['get', 'post'])
    def detail(self, request):
        self.authentication_classes = (TokenAuthentication, )
        self.permission_classes = (permissions.IsAuthenticated,)
        self.serializer_class = MasterUserSerializer

        try:
            masteruser = self.get_current_user()
            serializer = self.get_serializer(masteruser)

            return Response(data=serializer.data, status=status.HTTP_200_OK,)
        except Exception as e:
            return Response(data={"error": "Please log in as a valid user to perform this action."}, status=status.HTTP_405_METHOD_NOT_ALLOWED,)

    @list_route(methods=['post'])
    def save_last_activity(self, request):
        self.authentication_classes = (TokenAuthentication, )
        self.permission_classes = (permissions.IsAuthenticated,)

        try:
            masteruser = self.get_current_user()
            if masteruser:
                masteruser.save_last_activity(log_session=True)

            return Response(data={"message": "Updated last_activity."}, status=status.HTTP_200_OK,)
        except Exception as e:
            return Response(data={"error": "Please log in as a valid user to perform this action. %s" % e}, status=status.HTTP_405_METHOD_NOT_ALLOWED,)



