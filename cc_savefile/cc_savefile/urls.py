"""cc_savefile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static

from .views import (IndexPage,
    ModalFormSubmitSuccess, InvitationToDownloadGameView,
    FAQPage)
from users.forms import EmailAuthenticationForm

urlpatterns = [
    url(r'^$', IndexPage.as_view(), name='home'),
    url(r'^modal-submit-success/$', ModalFormSubmitSuccess.as_view(), name='modal_submit_success'),
    url(r'^invitation/$', InvitationToDownloadGameView.as_view(), name='invitation_to_download_game'),
    url(r'^faq/$', FAQPage.as_view(), name='faq'),

    url(r'^', include('users.urls')),

    url(r'^', include('gamesaves.urls')),

    url(r'^', include('quiz.urls')),

    url(r'^', include('store.urls')),

    url(r'^select2/', include('django_select2.urls')),

    url(r'^auth/', include('djoser.urls.authtoken')),

    # Custom login form using email as label for username
    url(r'^accounts/login/$', auth_views.login,
        {'authentication_form':EmailAuthenticationForm, 'template_name': 'registration/login.html'}, name='auth_login'),

    # Custom password reset form using html email template
    url(r'^accounts/password/reset/$',
        auth_views.password_reset,
        {'post_reset_redirect': 'auth_password_reset_done',
         'email_template_name': 'registration/password_reset_email.txt',
         'html_email_template_name': 'registration/password_reset_email.html'},
        name='auth_password_reset'),

    url(r'^accounts/', include('registration.backends.hmac.urls')),

    # url(r'session_security/', include('session_security.urls')),

    url(r'^nested_admin/', include('nested_admin.urls')),

    # url(r'^admin/django-ses/', include('django_ses.urls')),

    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    # Adds the media URL to be used in development
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )


from django.conf.urls import (
handler400, handler403, handler404, handler500
)

handler400 = 'cc_savefile.views.bad_request'
handler403 = 'cc_savefile.views.permission_denied'
handler404 = 'cc_savefile.views.page_not_found'
handler500 = 'cc_savefile.views.server_error'