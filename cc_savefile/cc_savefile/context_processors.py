def cc_savefile_context(request):
    from django.conf import settings
    context = {}

    context['DEBUG'] = getattr(settings, "DEBUG", False)
    context['PAYPAL_MODE'] = "sandbox" if getattr(settings, "TEST", False) else "production"
    context['LOCALHOST'] = "%s://%s/" % ('https' if request.is_secure() else 'http',
        request.get_host())

    return context