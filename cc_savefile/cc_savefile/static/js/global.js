$(function () {
    /*
        AJAX csrf
    */
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    /** START Popover script **/
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
    /** END Popover script **/

    /** START Messages script **/
    var $message_container = $(document).find('.messages');
    function showMessages () {
        $.each($message_container.find('li'), function(index) {
            $(this).delay(500*index).fadeIn(500);
        });
    }

    function hideMessages () {
        $.each($message_container.find('li'), function(index) {
            $(this).delay(1000*index).fadeOut(1000, function(){
                $(this).remove();
            });
        });
    }

    function showHideMessages () {
        if ($message_container.length > 0) {
            var message_count = $message_container.find("li").length;
            showMessages();
            setTimeout(hideMessages, (1000*message_count + 3000) );
        } else {
            clearTimeout(hideMessages);
        }
    }

    function generateMessagesInPage(message_dict) {
        var $header = $(document).find("header#top");
        if ($message_container.length <= 0) {
            var html = "<ul class='messages pull-right'>";
            $.each( message_dict, function( state, msg ) {
                if ($.isArray(msg)) {
                    for (var i = 0; i < msg.length; i++){
                        html += "<li class='" + state + "'>" + msg[i] + "</li>";
                    }
                } else {
                    html += "<li class='" + state + "'>" + msg + "</li>";
                }
            });
            html += "</ul>";
            $(html).insertAfter($header);
        } else {
            var html = "";
            $.each( message_dict, function( state, msg ) {
                if ($.isArray(msg)) {
                    for (var i = 0; i < msg.length; i++){
                        html += "<li class='" + state + "'>" + msg[i] + "</li>";
                    }
                } else {
                    html += "<li class='" + state + "'>" + msg + "</li>";
                }
            });
            $message_container.append(html);
        }
        $message_container = $(document).find('.messages');
        showHideMessages();
    }

    showHideMessages();
    /** END Messages script **/


    $(document).on("click", "a[data-toggle='ajax_button']", function (e) {
        /**
         * Handles ajax get/post when called via data-toggle="ajax_button"
         *
         * e = event object (passed by jQuery)
         * this = the calling element
         */
        e.preventDefault();

        var $source_obj = $(this),
            source_data = $source_obj.data(),
            a_href  = source_data.href || $source_obj.attr('href');

        if (a_href && a_href !== '' && a_href.indexOf('#') !== 0) {
            $("#loading-message").fadeIn(100);
            $.get(a_href, {'ajax': true}, function (response) {
                if (response['success'] != undefined || response['warning'] != undefined) {
                    generateMessagesInPage(response);
                }

                if (response['next_function'] != undefined) {
                    var next_function = response['next_function'];
                    var container_class = response['container_class'];
                    var container_content = response['container_content'];

                    $[next_function](container_class, container_content);
                }
                $("#loading-message").fadeOut(100);
            });
        }
    });


    $(document).on("click", "a[data-toggle='ajax_modal']", function (e) {
        /**
         * Handles modal when called via data-toggle="ajax_modal"
         *
         * e = event object (passed by jQuery)
         * this = the calling element
         */
        e.preventDefault();

        var $source_obj = $(this),
            source_data = $source_obj.data(),
            modal_href  = source_data.href || $source_obj.attr('href');

        if (modal_href && modal_href !== '' && modal_href.indexOf('#') !== 0) {
            $.get(modal_href, function (html) {
                var modal_id    = source_data.modalid || '',
                    modal_class = source_data.modalclass || '',
                    $modal      =  $('<div class="modal fade ajax-modal" data-backdrop="static" id="' + modal_id + '" class="' + modal_class + '">' + html + '</div>');

                window.setTimeout(function () {
                    $modal.modal('show');
                }, 50);
            });
        }
    });

    $(document).on('hidden.bs.modal', '.ajax-modal', function(){
        $(this).remove();
    });

    $(document).on("click", "input#modalformsubmit", function(e){
        e.preventDefault();
        var $this = $(this),
            $formObj = $(this).parents(".modal").find("form"),
            submitString = $formObj.serialize(),
            formURL = $formObj.attr("action"),
            $modal = $this.closest('.modal');

        var _post_form = function () {
            $this.prop("disabled", "disabled");
            $.post(formURL, submitString, function (data) {
                var $data = $(data);
                // console.log($data);
                // console.log(data);
                // console.log(typeof data);
                // get the class name and ID

                if ( $data.find(".field-errors-container").length ) {
                    /** If form has errors, reload the modal form with errors shown **/
                    $modal.html(data);
                    $modal.find('.formview-form-container').show();

                    /** re-fire the event **/
                    $modal.data('noajax', true).trigger('shown.bs.modal');
                } else {
                    /** If form is submitted successfully, reload the page **/
                    if (typeof(data) == "object") {
                        if (data['success'] != undefined || data['warning'] != undefined) {
                            generateMessagesInPage(data);
                        }
                    } else {
                        $modal.html(data);
                    }
                    window.setTimeout(function () {
                        $modal.modal('hide');
                        window.location.reload();
                    }, 1500);
                }
            });
        };

        _post_form();
    });

    $(document).on("click", "input.ajaxformsubmit", function(e){
        e.preventDefault();
        // console.log("clicked submit");
        var $this = $(this),
            $formObj = $(this).parents("form"),
            submitString = $formObj.serialize(),
            formURL = $formObj.attr("action"),
            form_redirectURL = $formObj.data("redirect"),
            $formcontainer = $formObj.parents(".form-container");

        $formObj.find("input").each(function(e){
            $(this).prop('disabled', 'disabled');
        })

        var _post_form = function () {
            $.post(formURL, submitString, function (data) {
                var $data = $(data);
                // console.log($data);
                // console.log(data);
                // console.log(typeof data);
                // get the class name and ID

                if ( $data.find(".field-errors-container").length > 0 ) {
                    $formcontainer.html(data);
                }
                else if ( $data.find(".errorlist").length > 0 ) {
                    $formcontainer.html(data);
                } else {
                    /** If form is submitted successfully, redirect to success url **/
                    window.location.href = form_redirectURL;
                }
            });
        };

        _post_form();
    });

    /** START index signup form clear autocomplete **/
    if ($(document).find("#index-content")) {
        $("#signup-form").trigger('reset');
    }
    /** END index signup form clear autocomplete **/


    /** START Redirect countdown after activation **/
    var $account_activated_container = $(document).find("#account_activated");
    var timer = $account_activated_container.find(".redirect_timer");
    var count = 6;
    var redirect = "/";

    function countDown(){
        if(count > 0){
            count--;
            timer.html(count);
            setTimeout(countDown, 1000);
        } else {
            window.location.href = redirect;
        }
    }

    if ($account_activated_container.length > 0) {
        countDown();
    } else {
        clearTimeout(countDown);
    }
    /** END Redirect countdown after activation **/


    /** START Form scripts **/
    var $form = $(document).find("form");
    if ($form.length > 0) {
        var $fieldsets   = $form.find("fieldset");
        var max_page     = $form.find('fieldset').length;
        var current_page = 1;

        var form_actions;

        form_actions = {
            _form_switchpage : function (pagenum) {
                /*
                    No page number given, automatically read the hash or switch to first page
                    Note: new hash format: #f1 (simple, lol);
                */
                if (typeof pagenum === 'undefined') {
                    var hash = window.location.hash;

                    pagenum = 0;

                    if (hash.indexOf('#f') === 0) {
                        pagenum = parseInt(hash.substring(2), 10) || 0;
                    }
                }
                // console.log(pagenum);
                if (pagenum === 0 || isNaN(pagenum)) pagenum = 1;

                var $fieldset_page = $form.find('fieldset[data-page="' + pagenum + '"]');
                var $circle_page = $("#register-trial-form").find('.circle[data-page="' + pagenum + '"]');
                    // $sidebar_page  = $form_sidebar_pages.find('a[data-formpage="' + pagenum + '"]');

                // if ($fieldset_page.length && $sidebar_page.length) {
                if ($fieldset_page.length) {
                    $form.find('fieldset').not($fieldset_page).hide();
                    $("#register-trial-form").find('.circle').not($circle_page).removeClass("active");
                    // $form_sidebar_pages.find('.list-group-item').not($sidebar_page).removeClass('list-group-item-info').find('.fa').removeClass('fa-caret-left');

                    $fieldset_page.show();
                    $circle_page.addClass("active");
                    // $sidebar_page.addClass('list-group-item-info').find('.fa').addClass('fa-caret-left');

                    // window.location.hash = '#f' + pagenum;
                    window.location.replace('#f' + pagenum);
                    current_page = pagenum;

                    /*
                        Based on the page number, show or hide the footer buttons
                    */
                    // console.log(pagenum);
                    // console.log(max_page);
                    $form.find('.btn-form-prev').toggle( pagenum > 1 );
                    $form.find('.btn-form-submit').toggle( pagenum == max_page );
                    $form.find('.btn-form-next').toggle( pagenum < max_page );
                }
            }
        }

        window.fs = form_actions;


        if ($fieldsets.length > 0) {
            var $next_btn = $form.find(".btn-form-next");

            $form.on("click", ".btn-form-next", function (e) {
                e.preventDefault();
                if (current_page < max_page) {
                    fs._form_switchpage(current_page + 1);
                }
            }).on('click', '.btn-form-prev', function (e) {
                e.preventDefault();
                if (current_page > 1) {
                    fs._form_switchpage(current_page - 1);
                }
            }).on('click', '.circle', function (e) {
                e.preventDefault();
                current_page = $(this).data("page");
                fs._form_switchpage(current_page);
            });

            if($form.find("#id_source").length > 0) {
                if ($form.find("#id_source").val() == 1){
                    $form.find("#id_source_text").parents(".optional").hide();
                } else if ($form.find("#id_source").val() == 2) {
                    $form.find("#id_source_text").parents(".optional").show();
                } else {
                    $form.find("#id_source_text").parents(".optional").hide();
                }

                $form.on("change", "#id_source", function(e){
                    if ($(this).val() == 1){
                        $form.find("#id_source_text").parents(".optional").hide();
                    } else {
                        $form.find("#id_source_text").parents(".optional").show();
                    }
                });
            }

            $.each($fieldsets, function(e){
                // console.log("checking fieldset ")
                // console.log($(this));
                if ($(this).find(".field-errors-container").length > 0) {
                    // console.log("error on page " + $(this).data("page"));
                    current_page = $(this).data("page");
                    fs._form_switchpage (current_page);
                    return false;
                }
            });
            // console.log("current_page: " + current_page);
            fs._form_switchpage (current_page);
        }

        $form.on("click", ".btn-form-submit", function (e) {
            e.preventDefault();
            // console.log($(this));
            // console.log($(this).id);
            if ($(this).attr("id") == "quiz-submit") {

            } else {
                // console.log("click form submit");
                $form.submit();
            }

        });

        function onSubmit(token) {
            console.log(token);
            $form.submit();
        }

        /** SELECT2 Enhancement to search based on matching 1st char instead of "containing chars" **/
        function matchStart (term, text) {
            if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                return true;
            }

            return false;
        }

        $form.find(".select-container").each(function(e){
            var $container = $(this);
            var $select = $container.find("select");
            // $select.select2();
            $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
                $select.select2({
                    matcher: oldMatcher(matchStart)
                });
                $container.find(".select2").each(function(e){
                    $(this).css("width", "100%");
                });
            });
        });
        /** SELECT2 Enhancement to search based on matching 1st char instead of "containing chars" **/

    }
    /** END Form scripts **/


    /** START tab scripts **/
    var page_url = window.location.href;
    if (page_url.indexOf("#") >= 0) {
        var activeTab = page_url.substring(page_url.indexOf("#") + 1);
        $(".tab-pane").removeClass("active in");
        $('a[href="#'+ activeTab +'"]').tab('show');
    }

    $('a[data-toggle="tab"]').on('click', function (e) {
        var target = this.href.split('#');
        if (target.length > 1) {
            $('a[href="#'+ target[1] +'"]').tab('show');
        }

    });
    /** END tab scripts **/


    /** START License pagination **/
    if ($(".pagination").length > 0){
        $(".pagination").each(function(e){
            var $pagination = $(this);
            var $pagination_id = "#" + $(this).attr("id");
            var $page_suffix = "#" + $(this).data("pagesuffix");
            var $btn_prevpage = $pagination.find(".btn-page-prev");
            var $btn_nextpage = $pagination.find(".btn-page-next");
            var $btn_left_concat = $pagination.find(".page-item-concat-left");
            var $btn_right_concat = $pagination.find(".page-item-concat-right");
            var active_page = $pagination.find("li.active").data("page");
            var total_pages = parseInt($pagination.data("totalpages"));

            $(document).on("click", $pagination_id+" .btn-page", function(e){
                active_page = $(this).parents(".page-item").data("page");
                togglePrevNext($pagination, $btn_prevpage, $btn_nextpage, $btn_left_concat, $btn_right_concat, $page_suffix, active_page, total_pages);
            })
            .on("click", $pagination_id+" .btn-page-prev", function(e){
                active_page = $(this).data("gotopage");
                $btn_prevpage.parents("li").removeClass("active");
                $pagination.find(".page-item[data-page='"+active_page+"']").addClass("active");

                togglePrevNext($pagination, $btn_prevpage, $btn_nextpage, $btn_left_concat, $btn_right_concat, $page_suffix, active_page, total_pages);
            })
            .on("click", $pagination_id+" .btn-page-next", function(e){
                active_page = $(this).data("gotopage");
                $btn_nextpage.parents("li").removeClass("active");
                $pagination.find(".page-item[data-page='"+active_page+"']").addClass("active");

                togglePrevNext($pagination, $btn_prevpage, $btn_nextpage, $btn_left_concat, $btn_right_concat, $page_suffix, active_page, total_pages);
            });

            togglePrevNext($pagination, $btn_prevpage, $btn_nextpage, $btn_left_concat, $btn_right_concat, $page_suffix, active_page, total_pages);
        });


        function togglePrevNext($pagination, $btn_prevpage, $btn_nextpage, $btn_left_concat, $btn_right_concat, $page_suffix, active_page, total_pages) {
            var prev_page = parseInt(active_page) - 1;
            var next_page = parseInt(active_page) + 1;

            $pagination.find('.page-item').each(function(e){
                if (parseInt($(this).data("page")) == active_page || parseInt($(this).data("page")) == prev_page || parseInt($(this).data("page")) == next_page) {
                    $(this).removeClass("hide");
                } else {
                    if (active_page == 1 && parseInt($(this).data("page")) == 3) {
                        $(this).removeClass("hide");
                    } else {
                        $(this).addClass("hide");
                    }
                }
            });

            if (prev_page == 0) {
                $btn_prevpage.parents(".prev-next-btn").addClass("disabled");
                $btn_prevpage.parents(".prev-next-btn").addClass("hide");
                $btn_prevpage.attr("data-gotopage", 1);
                $btn_prevpage.data("gotopage", 1);

                $btn_left_concat.addClass("hide");
            } else {
                $btn_prevpage.parents(".prev-next-btn").removeClass("disabled");
                $btn_prevpage.parents(".prev-next-btn").removeClass("hide");
                $btn_prevpage.attr("href", $page_suffix + prev_page);
                $btn_prevpage.attr("data-gotopage", prev_page);
                $btn_prevpage.data("gotopage", prev_page);
            }

            var first_visible_page_item = $pagination.find(".page-item:visible").first();
            // console.log(first_visible_page_item.data("page"));
            if ( parseInt(first_visible_page_item.data("page")) > 1) {
                $btn_left_concat.removeClass("hide");
            } else {
                $btn_left_concat.addClass("hide");
            }

            if (active_page == total_pages) {
                $btn_nextpage.parents(".prev-next-btn").addClass("disabled");
                $btn_nextpage.parents(".prev-next-btn").addClass("hide");

                $btn_right_concat.hide();
            } else {
                $btn_nextpage.parents(".prev-next-btn").removeClass("disabled");
                $btn_nextpage.parents(".prev-next-btn").removeClass("hide");
                $btn_nextpage.attr("href", $page_suffix + next_page);
                $btn_nextpage.attr("data-gotopage", next_page);
                $btn_nextpage.data("gotopage", next_page);
            }

            var last_visible_page_item = $pagination.find(".page-item:visible").last();
            // console.log(last_visible_page_item.data("page"));
            if ( (total_pages - parseInt(last_visible_page_item.data("page")) ) > 0) {
                $btn_right_concat.removeClass("hide");
            } else {
                $btn_right_concat.addClass("hide");
            }


            $pagination.find("li:not(.hide)").each(function(e){
                $(this).find("a").removeClass("left-li");
                $(this).find("a").removeClass("right-li");
            });
            $pagination.find("li:not(.hide)").last().find("a").addClass("right-li");
            $pagination.find("li:not(.hide)").first().find("a").addClass("left-li");

        }
    }
    /** END License pagination **/


    /** START License report **/
    $.show_progress = function show_progress(container_class, container_content) {
        $("." + container_class).html(container_content);
        $(".report-container").show();
        var $report = $(".report-container");
        if($report.find(".license_update_form_container").length > 0) {
            var $license_update_form_container = $report.find(".license_update_form_container");
            $.get($license_update_form_container.data("href"), function(response){
                $license_update_form_container.html(response);
            })

            $(document).on("click", "#license-update-form-submit-btn", function(e){
                e.preventDefault();
                var $this = $(this),
                    $formObj = $(document).find("#license_update_form"),
                    submitString = $formObj.serialize(),
                    formURL = $formObj.attr("action");

                var _post_form = function () {
                    $.post(formURL, submitString, function (data) {
                        if (data['success'] != undefined || data['warning'] != undefined) {
                            generateMessagesInPage(data);
                            $.each($formObj.find(".field-errors-container"), function(e){
                                $(this).remove();
                            });
                        } else {
                            var $data = $(data);
                            // console.log($data);
                            // console.log(data);
                            // console.log(typeof data);
                            // get the class name and ID

                            if ( $data.find(".field-errors-container").length ) {
                                /** If form has errors, reload the modal form with errors shown **/
                                $license_update_form_container.html(data);
                            } else {
                                /** If form is submitted successfully, reload the page **/

                                $license_update_form_container.html(data);
                                // window.location.reload();
                            }
                        }

                    });
                };

                _post_form();
            });

        }
    }

    $(document).on("click", ".close-report", function(e){
        $(".report-container").hide();
    })
    /** END License report **/

    /** START License generate **/
    if ($(document).find("#license_management").length > 0) {
        function changeGenerateURL($btn, $input) {
            var url_generate = $btn.data("href");
            var new_url_generate = url_generate + "&num=" + $input.val();
            $btn.attr("href", new_url_generate);
            $btn.prop("href", new_url_generate);
            console.log($btn.attr("href"));
            console.log(url_generate);
            console.log(new_url_generate);
        }
        $(document).on("change", "#input_num_to_generate_full", function(e){
            changeGenerateURL($("#btn_generate_full"), $("#input_num_to_generate_full"));
        });


        $(document).on("change", "#input_num_to_generate_trial", function(e){
            changeGenerateURL($("#btn_generate_trial"), $("#input_num_to_generate_trial"));
        });

        if ($("#input_num_to_generate_full").length > 0) {
            changeGenerateURL($("#btn_generate_full"), $("#input_num_to_generate_full"));
            changeGenerateURL($("#btn_generate_trial"), $("#input_num_to_generate_trial"));
        }

    }

    /** END License generate **/


    /** START License show hide collapse **/
    $(document).find('.license-moreinfo.collapse').on('show.bs.collapse', function () {
        var id = $(this).attr('id');
        var $toggle = $(document).find(".license-cell-toggle-collapse[data-target='#"+id+"']");
        $toggle.find(".glyphicon").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    });

    //The reverse of the above on hidden event:

    $(document).find('.license-moreinfo.collapse').on('hide.bs.collapse', function () {
        var id = $(this).attr('id');
        var $toggle = $(document).find(".license-cell-toggle-collapse[data-target='#"+id+"']");
        $toggle.find(".glyphicon").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    });
    /** END License show hide collapse **/

    // $(document).on("click", ".license-cell-invite-btn", function(e){
    //     var tabtarget = $(this).data("tabtarget");
    //     var user_id = $(this).data("userid");
    //     // console.log($(".invite-buttons-container").find("a[href='"+tabtarget+"']"));
    //     var $tab = $(".invite-buttons-container").find("a[href='"+tabtarget+"']");
    //     $tab.tab('show');
    //     $(tabtarget).find("#id_user_id").val(user_id);
    // });

    // $(document).on("click", "a[href='#tab_single_invite_all']", function(e){
    //     $("#tab_single_invite_all").find("#id_user_id").val("");
    // });

    /** STORE **/
    if ($(document).find("#cart-page").length > 0) {
        var total = $("#total_price").data('amount');
        var items_dict_list = [];
        var current_username = $("#current_user").val();

        function updateTotalPrice ($input){
            var buycount = parseInt($input.val());
            var cartitem_id = parseInt($input.data('cartitem'));
            var para = {"qty": buycount, "cartitem": cartitem_id};
            $.get(UPDATE_CART_ITEM_URL, para, function(response){
                $("#cart_table").html(response);
                total = $("#total_price").data('amount');
                // updatePaymentData();
            });
        }

        $(document).on("change", ".input-cart-qty", function(){
            updateTotalPrice($(this));
        });

        function updateCart ($input){
            var url = $input.data('href');

            $.get(url, function(response){
                $("#cart_table").html(response);
            });
        }

        $(document).on("click", ".remove-cartitem-btn", function(e){
            e.preventDefault();
            updateCart($(this));
        });

        paypal.Button.render({
            env: PAYPAL_MODE, // 'production' or 'sandbox',

            commit: true, // Show a 'Pay Now' button

            payment: function() {
                return paypal.request.post(CREATE_PAYMENT_URL).then(function(data) {
                    return data.paymentID;
                });
            },

            onAuthorize: function(data, actions) {
                console.log(data);
                if (data.paymentID == undefined) {
                    window.location.href = data.returnUrl;
                } else {
                    return paypal.request.post(EXECUTE_PAYMENT_URL, {
                        paymentID: data.paymentID,
                        payerID:   data.payerID
                    }).then(function(response) {

                        // The payment is complete!
                        // You can now show a confirmation message to the customer

                        console.log(response);
                        window.location.href = response.redirect_url;
                    });
                }
                
            }

        }, '#paypal-button');

        // TODO
        // Create Transaction when completed checkout
        // Update total when qty numbers change
        // Remove CartItem button
    };

    /** STORE **/
});