$(function () {
    var $redeemForm = $(document).find("#redeem_form");

    if ($redeemForm.length > 0) {
        var $input_redeem_code = $("#id_redeem_code");
        if ($redeemForm.find(".field-errors-container").length > 0){
            $("#id_username").parents(".required").removeClass("hide");
            $("#id_email").parents(".required").removeClass("hide");
            $("#id_full_name").parents(".required").removeClass("hide");
            // $("#id_student_id").parents(".optional").removeClass("hide");
            // $("#id_classroom").parents(".optional").removeClass("hide");
            $("#submit-button").show();
        } else {
            $("#submit-button").hide();
        }

        function checkRedeemCode(){
            if ($input_redeem_code.val() != "") {
                $.get(CHECK_REDEEM_CODE, {"code": $input_redeem_code.val()}, function(data){
                    var success = data["success"]
                    var response = data["response"]
                    if (success) {
                       $("#redeem_code_response_success").removeClass("hide");
                       $("#redeem_code_response_fail").addClass("hide");
                       $("#id_username").parents(".required").removeClass("hide");
                       $("#id_email").parents(".required").removeClass("hide");
                       $("#id_full_name").parents(".required").removeClass("hide");

                       $("#id_username").val(data["user_data"]["username"]);
                       $("#id_email").val(data["user_data"]["email"]);
                       $("#id_full_name").val(data["user_data"]["full_name"]);
                       // $("#id_student_id").parents(".optional").removeClass("hide");
                       // $("#id_classroom").parents(".optional").removeClass("hide");
                       $("#submit-button").show();
                    } else {
                        $("#redeem_code_response_success").addClass("hide");
                        $("#redeem_code_response_fail").removeClass("hide");
                        $("#redeem_code_response_fail").html("<i class='fa fa-times' style='color:red;'></i> " + response);
                        $("#id_username").parents(".required").addClass("hide");
                        $("#id_email").parents(".required").addClass("hide");
                        $("#id_full_name").parents(".required").addClass("hide");
                        // $("#id_student_id").parents(".optional").addClass("hide");
                        // $("#id_classroom").parents(".optional").addClass("hide");
                        $("#submit-button").hide();
                    }
                });
            }
        }

        $(document).on("change", "#id_redeem_code", function(e){
            checkRedeemCode();
        });

        $input_redeem_code.keypress(function (e) {
            if(e.which == 13) {
                checkRedeemCode();
                return false;
            }
        });
    }

});