$(function () {
    var $quizForm = $(document).find("#quiz-sitting-form");

    if ($quizForm.length > 0) {
        var cvText_list = {}
        $.each($(document).find(".canvasDataUrl"), function(){
            cvText_list[$(this).data("qid")] = $(this);
        });
        // var cvImage = $(document).find(".canvasCompletedImage");
        var clearBtn_list = $(document).find(".canvasClearBtn");
        // var submitBtn = $(document).find(".canvasSubmitBtn");

        var drawing = false;
        var drawing_qid = 0;
        var mousePos = { x:0, y:0 };
        var lastPos = mousePos;
        var is_drawn = false;
        var is_drawn_list = {};

        var canvas;
        var canvas_list = {};
        var ctx = {};

        // document.body.style.overflow = "hidden";

        $(document).on("click", "[data-toggle='cc_modal']", function(e){
            var modalID = $(this).data("target");
            var question_text = $(this).parents(".question_container").find(".question_header").html();
            var image_html = $(this).parents(".image_container").html();
            $(modalID).find(".modal-title").html(question_text);
            $(modalID).find(".modal-body").html(image_html);
            $(modalID).modal('toggle');
        });

    //     $(document)
    // .on('show.bs.modal', '.modal', function (e) {
    //     $(this).appendTo($('body'));
    // })


        $quizForm.on("click", "#quiz-submit", function(e){
            e.preventDefault();
            $.each(is_drawn_list, function(key, value){
                if (value) {
                    var dataUrl = canvas_list[key].node.toDataURL();
                    // console.log(dataUrl);
                    cvText_list[key].val(dataUrl);
                    // console.log(cvText_list[key]);
                }
            });
            // if (is_drawn) {
            //     var dataUrl = canvas.node.toDataURL();
            //     cvText.innerHTML = dataUrl;
            // }

            var post_data = $quizForm.serialize();
            $.post($(location).attr('href'), post_data, function(data){
                var response = data["response"];
                var has_toggled_fieldset = false;
                // console.log(response);
                if (response == undefined || response == null || response == "") {

                } else {
                    for (i=0; i < response.length; i++) {
                        // console.log(response[i]);
                        // for (var key in response[i]){
                            // console.log(key);
                            // console.log(response[i][key]);
                        // }
                        var $question_container = $quizForm.find("input[name='question_" + response[i]["order"] + "_pk_" + response[i]["question"] + "']").parents(".question_container");
                        if ($question_container.length <= 0) {
                            $question_container = $quizForm.find("textarea[name='question_" + response[i]["order"] + "_pk_" + response[i]["question"] + "']").parents(".question_container");
                        }
                        var $question = $question_container.find(".question_header");
                        var $question_row = $question_container.find(".question_row");
                        // $question_container.parent().hide();

                        if (response[i]["is_answered"] == true) {
                            $.each($question_container.find(".error_empty"), function(){
                               $(this).remove();
                            })

                            // question.removeClass("question_wrong");
                            // question.addClass("question_correct");
                        } else {
                            if ($question_container.find(".error_empty").length <= 0) {
                                $( "<div class='error_empty'>Please answer this question.</div>" ).insertAfter($question_row);
                            }

                            if (!has_toggled_fieldset) {
                                var page = $question_container.parent().data("page");
                                fs._form_switchpage (page);
                                has_toggled_fieldset = true;
                            }


                            // question.removeClass("question_correct");
                            // question.addClass("question_wrong");
                        }
                    }
                }

                var redirect = data["redirect"];
                if (redirect == undefined || redirect == null || redirect == "") {
                } else {
                    window.location.href = redirect;
                }
            });
        });

        // Get a regular interval for drawing to the screen
        window.requestAnimFrame = (function (callback) {
            return window.requestAnimationFrame ||
                        window.webkitRequestAnimationFrame ||
                        window.mozRequestAnimationFrame ||
                        window.oRequestAnimationFrame ||
                        window.msRequestAnimaitonFrame ||
                        function (callback) {
                            window.setTimeout(callback, 1000/60);
                        };
        })();

        // Set up the canvas
        function createCanvas(parent, width, height) {
            var canvas = {};
            canvas.node = document.createElement('canvas');
            canvas.context = canvas.node.getContext('2d');
            canvas.node.width = width || 100;
            canvas.node.height = height || 100;
            parent.append(canvas.node);
            return canvas;
        }

        function initCanvas(container, width, height, qid) {
            canvas = createCanvas(container, width, height);
            canvas_list[qid] = canvas;
            is_drawn_list[qid] = false;
            ctx[qid] = canvas.context;
            ctx[qid].strokeStyle = "#222222";
            ctx[qid].lineWidth = 2;

            // define a custom fillCircle method
            ctx[qid].clearTo = function() {
                // ctx.fillStyle = fillColor;
                // ctx.fillRect(0, 0, width, height);
                ctx[qid].clearRect(0, 0, width, height);
                // is_drawn = false;
                is_drawn_list[qid] = false;
            };
            ctx[qid].clearTo();

            // Set up the UI
            $.each(clearBtn_list, function(i){
                var $btn = $(this);
                var btn_qid = $btn.data("qid");
                $quizForm.on("click", ".canvasClearBtn[data-qid='"+btn_qid+"']", function (e) {
                    clearCanvas(btn_qid);
                    // cvText_list[btn_qid].innerHTML = "Data URL for your signature will go here!";
                    // cvText.innerHTML = "Data URL for your signature will go here!";
                    // cvImage.setAttribute("src", "");
                });
            });

            // submitBtn.addEventListener("click", function (e) {
            //     var dataUrl = canvas.node.toDataURL();
            //     cvText.innerHTML = dataUrl;
            //     // cvImage.setAttribute("src", dataUrl);
            // }, false);

            // Set up mouse events for drawing
            canvas_list[qid].node.addEventListener("mousedown", function (e) {
                drawing = true;
                drawing_qid = qid;
                lastPos = getMousePos(canvas_list[qid].node, e);
                // is_drawn = true;
                is_drawn_list[qid] = true;
                // console.log(ctx);
            }, false);
            canvas_list[qid].node.addEventListener("mouseup", function (e) {
                drawing = false;
                drawing_qid = 0;
            }, false);
            canvas_list[qid].node.addEventListener("mousemove", function (e) {
                mousePos = getMousePos(canvas_list[qid].node, e);
            }, false);

            // Set up touch events for mobile, etc
            canvas_list[qid].node.addEventListener("touchstart", function (e) {
                mousePos = getTouchPos(canvas_list[qid].node, e);
                var touch = e.touches[0];
                var mouseEvent = new MouseEvent("mousedown", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas_list[qid].node.dispatchEvent(mouseEvent);
            }, false);
            canvas_list[qid].node.addEventListener("touchend", function (e) {
                var mouseEvent = new MouseEvent("mouseup", {});
                canvas_list[qid].node.dispatchEvent(mouseEvent);
            }, false);
            canvas_list[qid].node.addEventListener("touchmove", function (e) {
                var touch = e.touches[0];
                var mouseEvent = new MouseEvent("mousemove", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas_list[qid].node.dispatchEvent(mouseEvent);
            }, false);

            // Prevent scrolling when touching the canvas
            document.body.addEventListener("touchstart", function (e) {
                if (e.target == canvas_list[qid].node) {
                    e.preventDefault();
                }
            }, false);
            document.body.addEventListener("touchend", function (e) {
                if (e.target == canvas_list[qid].node) {
                    e.preventDefault();
                }
            }, false);
            document.body.addEventListener("touchmove", function (e) {
                if (e.target == canvas_list[qid].node) {
                    e.preventDefault();
                }
            }, false);
        }

        // Get the position of the mouse relative to the canvas
        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            };
        }

        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            };
        }

        // Draw to the canvas
        function renderCanvas() {
            if (drawing) {
                document.body.style.overflow = "hidden";
                ctx[drawing_qid].moveTo(lastPos.x, lastPos.y);
                ctx[drawing_qid].lineTo(mousePos.x, mousePos.y);
                ctx[drawing_qid].stroke();
                lastPos = mousePos;
            } else {
                document.body.style.overflow = "scroll";
            }
        }

        function clearCanvas(qid) {
            // canvas.node.width = canvas.node.width;
            canvas_list[qid].node.width = canvas_list[qid].node.width;
            ctx[qid].clearTo();
            ctx[qid].lineWidth = 2;
            console.log("clear canvas");
        }

        // Allow for animation
        (function drawLoop () {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        // var canvasDiv = document.getElementById("canvasDiv");
        var canvasDiv_list = $(document).find(".canvasDiv");
        $.each(canvasDiv_list, function(e){
            console.log(e);
            console.log($(this));
            initCanvas($(this), 300, 300, $(this).data("qid"));
        });
    }

});