import json
import datetime
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.encoding import force_unicode, smart_text, force_text
from django.http import HttpResponse
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None

def json_response(verbose=False, **kwargs):
    """A generic json httpresponse.
    """
    # Forces lazy translation
    if kwargs.get("messages"):
        kwargs["messages"] = [force_unicode(msg) for msg in kwargs["messages"]]

    if kwargs.get("errors"):
        kwargs["errors"] = [force_unicode(error) for error in kwargs["errors"]]

    # Removes None
    kwargs = dict((k,v) for k,v in kwargs.iteritems() if v is not None)

    # vprint(kwargs, verbose)

    return HttpResponse(json.dumps(kwargs, default=dthandler), content_type='application/json')


def bulk_logentry_op(user_id, action_flag, object_list, message=""):
    """Bulk create log entries. (AWESOME-ness!)

    Args:
        user_id: PK of user performing action.
        action_flag: ADDITION, CHANGE, DELETION.
        object_list: Objects to perform operation on.
        message: Operation message. ``Default=""``.

    .. note::
        In case of DELETION, this method must be called before the deletion.
    """
    _entries = []
    for obj in object_list:
        _entries.append(
            LogEntry(
                action_time = timezone.now(),
                user_id = user_id,
                content_type = ContentType.objects.get_for_model(obj),
                object_id = smart_text(obj.pk),
                object_repr = force_text(obj)[:200],
                action_flag = action_flag,
                change_message = _("%s" % message) if message else u" "
            )
        )
    LogEntry.objects.bulk_create(_entries)


