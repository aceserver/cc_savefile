from django import template
from django.core.urlresolvers import reverse_lazy as reverse
register = template.Library()


@register.simple_tag
def get_verbose_name(object):
    return object._meta.verbose_name


@register.simple_tag
def get_verbose_field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    print instance
    try:
        return instance._meta.get_field(field_name).verbose_name.title()
    except:
        return field_name.title()


@register.filter
def is_normal_nav_urls(path):
    allowed_urls = [reverse('home'), reverse('faq'), reverse('signup_login'),
        'license/activate/', reverse('license_redeem'),
        reverse('register_thank_you'), reverse('registration_activation_complete'),
        reverse('users_resend_activation_email_form'), reverse('auth_password_reset'),
        reverse('auth_password_reset_done'), 'accounts/password/reset/confirm/',
        reverse('auth_password_reset_complete'), 'users/resend-activation-email-success/',
        reverse('store'), reverse('license_set_username_thankyou'), 
        'set-username/'
        ]

    result = False
    for url in allowed_urls:
        if str(url) != '/':
            if str(url) in str(path):
                result = True
        else:
            if str(url) == str(path):
                result = True
    return result


@register.filter
def convert_fk_to_str(field, value):
    obj = field.queryset.get(id=value)
    return "%s" % obj

@register.filter
def get_type(value):
    return type(value)

@register.filter
def convert_queryset_to_str(qs):
    string = ""
    if qs:
        qs_str_list = qs.values_list("title", flat=True)
        string = ",".join(qs_str_list)
    return string

