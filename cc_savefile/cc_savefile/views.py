from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from django.template import loader
from django.core.urlresolvers import reverse_lazy as reverse
from django.views.generic import CreateView, TemplateView
from django.conf import settings
from django.core import signing

from users.models import MasterUser, AccountPackage
from gamesaves.models import Platform, Game

from users.forms import SignUpPilotForm, EmailAuthenticationForm

from .utils import json_response

# Create your views here.
class IndexPage(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context['signup_form'] = SignUpPilotForm()
        context['login_form'] = EmailAuthenticationForm()
        return context

class FAQPage(TemplateView):
    template_name = "faq.html"


class ModalFormSubmitSuccess(TemplateView):
    template_name = "modal_template_success.html"


class InvitationToDownloadGameView(TemplateView):
    template_name = "download_game.html"

    def get_context_data(self, **kwargs):
        context = super(InvitationToDownloadGameView, self).get_context_data(**kwargs)
        key = self.request.GET.get("key")
        if key:
            try:
                username = signing.loads(
                    key,
                    salt='invitation',
                    max_age=settings.LINK_ACTIVATION_DAYS * 86400
                )
                context["status"] = 'success'
            except signing.SignatureExpired:
                context["status"] = 'expired'
            except signing.BadSignature:
                context["status"] = 'failed'
        else:
            context["status"] = 'failed'
        return context



# HTTP Error 400
def bad_request(request):
    response = render(request, '400.html', status=400)
    return response


# HTTP Error 403
def permission_denied(request):
    response = render(request, '403.html', status=403)
    return response

# HTTP Error 404
def page_not_found(request):
    response = render(request, '404.html', status=404)
    return response

# HTTP Error 500
def server_error(request):
    response = render(request, '500.html', status=500)
    return response