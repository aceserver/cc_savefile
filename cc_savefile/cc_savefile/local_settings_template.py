DEBUG = True
TEST = True
COMPRESS_ENABLED = False
RUN_TASKS = True
SITE_ID = 1 # change this id if Site object is different

ALLOWED_HOSTS = ['192.168.24.95', '127.0.0.1', 'localhost']

INSTALLED_APPS += ['django_celery_beat', 'djcelery_email']

DATABASES = {
   "default": {
       "ENGINE": "django.db.backends.postgresql_psycopg2",
       "NAME": "cc_savefile_live",
       "USER": "admin",
       "PASSWORD": "",
       "HOST": "localhost",
       "PORT": "",
       "AUTOCOMMIT": True,
   }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },
    'cache_machine': {
        'BACKEND': 'caching.backends.dummy.DummyCache',
    },
    'select2': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

MAINTENANCE_MODE = False
CELERY_CACHE_BACKEND = "dummy"

EMAIL_PORT = 1025
if RUN_TASKS:
    EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

