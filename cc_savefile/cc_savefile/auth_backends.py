from __future__ import unicode_literals
import logging
from django.conf import settings
from django.db.models import Q
from django.utils.http import base36_to_int
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password, is_password_usable
from users.models import MasterUser

logger = logging.getLogger("ccserver")

DEFAULT_USER_PASSWORD = getattr(settings, "DEFAULT_USER_PASSWORD", "")


class MasterUserBackend(object):
    """
    Authenticates against MasterUser.user_id
    """
    def authenticate(self, request, user_id=None, steam_id=None, user_name=None, password=None, school=None, classroom=None, **kwargs):
        return_user = None
        logger.debug("MasterUserBackend {} {} {} {}".format(user_id, steam_id, user_name, password))

        if user_id:
            logger.debug(user_id)
            try:
                masteruser = MasterUser.active_objects.filter(user_id__iexact=user_id).first()

                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    return_user = masteruser.auth_user
            except (IndexError, AttributeError):
                logger.error("problem while authenticating")
                pass

        if steam_id:
            try:
                masteruser = MasterUser.active_objects.filter(steam_id__iexact=steam_id).first()

                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    return_user = masteruser.auth_user
                else:
                    # if Steam User does not exist in server, create one
                    user_username = "%s" % (steam_id)
                    new_user = get_user_model()(username=user_username)
                    new_user.is_active = True
                    new_user.set_unusable_password()
                    new_user.save()

                    if not masteruser:
                        new_masteruser = MasterUser.objects.create(username=user_username, steam_id=user_username, auth_user=new_user)
                    else:
                        masteruser.auth_user = new_user
                        masteruser.save()
                        new_masteruser = masteruser
                    return_user = new_masteruser.auth_user

            except (IndexError, AttributeError):
                logger.error("problem while authenticating")
                pass

        if user_name and password:
            # print user_name, password
            try:
                masteruser = MasterUser.active_objects.filter(username__iexact=user_name).first()
                logger.debug(masteruser)
                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    if masteruser.auth_user.check_password(password):
                        return_user = masteruser.auth_user

            except (IndexError, AttributeError):
                logger.error("problem while authenticating")
                pass

        if user_name and classroom:
            try:
                masteruser = MasterUser.active_objects.filter(username__iexact=user_name, classroom=classroom).first()

                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    return_user = masteruser.auth_user
                else:
                    # if TeacherGaming User does not exist in server, create one
                    user_username = "%s@%s" % (user_name, classroom)
                    new_user = get_user_model()(username=user_username)
                    new_user.is_active = True
                    new_user.set_unusable_password()
                    new_user.save()

                    if not masteruser:
                        new_masteruser = MasterUser.objects.create(username=user_name, classroom=classroom, teachergaming_id=user_username, auth_user=new_user)
                    else:
                        masteruser.auth_user = new_user
                        masteruser.save()
                        new_masteruser = masteruser
                    return_user = new_masteruser.auth_user

            except (IndexError, AttributeError):
                logger.error("probs while authenticating")
                pass

        if user_name and school:
            try:
                masteruser = MasterUser.active_objects.filter(username__iexact=user_name, schooluser__school_id=school).first()

                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    return_user = masteruser.auth_user
            except (IndexError, AttributeError):
                logger.error("probs while authenticating")
                pass

        if user_name and not school and not classroom and not password:
            try:
                masteruser = MasterUser.active_objects.filter(username__iexact=user_name).first()

                if masteruser and hasattr(masteruser, "auth_user") and masteruser.auth_user:
                    return_user = masteruser.auth_user
            except (IndexError, AttributeError):
                logger.error("probs while authenticating")
                pass

        return return_user

    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class CCSaveFileBackend(object):
    """
    Extends Django's ``ModelBackend`` to allow login via username,
    email, or verification token.

    Args are either ``username`` and ``password``, or ``uidb36``
    and ``token``. In either case, ``is_active`` can also be given.

    For login, is_active is not given, so that the login form can
    raise a specific error for inactive users.
    For password reset, True is given for is_active.
    For signup verficiation, False is given for is_active.
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        User = get_user_model()
        logger.debug("CCSaveFileBackend {} {}".format(username, password))
        if username:
            username_or_email = Q(username=username) | Q(email=username)
            try:
                user = User.objects.filter(username_or_email, **kwargs).first()
            except User.DoesNotExist:
                pass
            else:
                if user and user.check_password(password):
                    return user
        else:
            if 'uidb36' in kwargs:
                kwargs["id"] = base36_to_int(kwargs.pop("uidb36"))
                token = kwargs.pop("token")
                try:
                    user = User.objects.get(**kwargs)
                except User.DoesNotExist:
                    pass
                else:
                    if user and default_token_generator.check_token(user, token):
                        return user

        User().set_password(password)

    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions
from django.utils.translation import ugettext_lazy as _
class CustomTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        model = self.get_model()
        try:
            token = model.objects.select_related('user').get(key=key)
        except model.DoesNotExist:
            # custom invalid token error message
            raise exceptions.AuthenticationFailed(_('You have been logged out due to inactivity. You will now be redirected back to the Main Menu.'))

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (token.user, token)
