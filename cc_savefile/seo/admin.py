from django.contrib import admin
from .models import PageMetaTags


class PageMetaTagsAdmin(admin.ModelAdmin):
    list_display = ("page_id", "og_title", "og_description", "og_types",
                    "og_keywords", "created", "modified")
    fields = ("page_id", "og_title", "og_description", "og_types",
              "og_keywords", "og_image")
    readonly_fields = ("page_id", )

    def has_add_permission(self, request):
        return False

admin.site.register(PageMetaTags, PageMetaTagsAdmin)
