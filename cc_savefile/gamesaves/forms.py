import json
from django import forms
from django.utils.translation import ugettext_lazy as _

from users.models import MasterUser
from .models import GameSave, TrialGameSave

class GameSaveForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.masteruser = kwargs.pop('masteruser', None)
        self.is_active = kwargs.pop('is_active', None)
        super(GameSaveForm, self).__init__(*args, **kwargs)

    json_data = forms.CharField(max_length=90000)

    class Meta:
        model = GameSave
        fields = ("masteruser", "platform", "game", "slot",
            "data_version", "json_data", "description", "is_active",)

    def clean_json_data(self):
        jdata = self.cleaned_data['json_data']

        try:
            #validate json_data
            #loads string as json
            json_data = json.loads(jdata)
        except Exception as e:
            raise forms.ValidationError("Invalid data in json_data: %s" % e)
        return json_data

    # def clean_masteruser(self):
    #     data = self.cleaned_data['masteruser']
    #     try:
    #         masteruser = MasterUser.objects.get(user_id__iexact=data)
    #         return masteruser
    #     except:
    #         raise forms.ValidationError("Cannot find Master User %s" % data)


    def save(self):
        data = self.cleaned_data
        gamesave = GameSave(masteruser = self.masteruser,
                            platform   = data['platform'],
                            game       = data['game'],
                            slot       = data['slot'],
                            data_version = data['data_version'],
                            json_data    = data['json_data'],
                            description  = data['description'],
                            is_active    = self.is_active,
                            )
        gamesave.save()
        return gamesave


class TrialGameSaveForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.masteruser = kwargs.pop('masteruser', None)
        # self.is_active = kwargs.pop('is_active', None)
        super(TrialGameSaveForm, self).__init__(*args, **kwargs)

    json_data = forms.CharField(max_length=90000)

    class Meta:
        model = TrialGameSave
        fields = ("masteruser", "platform", "game", "slot",
            "data_version", "json_data", "description",)

    def clean_json_data(self):
        jdata = self.cleaned_data['json_data']

        try:
            #validate json_data
            #loads string as json
            json_data = json.loads(jdata)
        except Exception as e:
            raise forms.ValidationError("Invalid data in json_data: %s" % e)
        return json_data


    def save(self):
        data = self.cleaned_data
        gamesave = TrialGameSave(masteruser = self.masteruser,
                            platform   = data['platform'],
                            game       = data['game'],
                            slot       = data['slot'],
                            data_version = data['data_version'],
                            json_data    = data['json_data'],
                            description  = data['description'],
                            is_active    = True,
                            is_completed = False,
                            )
        gamesave.save()
        return gamesave