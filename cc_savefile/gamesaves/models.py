from __future__ import unicode_literals

import pytz
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.postgres.fields import JSONField

class Platform(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=100, blank=True, null=True)
    is_edu = models.BooleanField(_('Is EDU Platform'), default=False)
    is_active = models.BooleanField(_('Is Active'), default=True)
    
    class Meta:
        verbose_name = _('Platform')
        verbose_name_plural = _('Platforms')

    def __unicode__(self):
        return self.title


class Game(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=100, blank=True, null=True)
    is_active = models.BooleanField(_('Is Active'), default=True)

    class Meta:
        verbose_name = _('Game')
        verbose_name_plural = _('Games')

    def __unicode__(self):
        return self.title

class GamePlatform(TimeStampedModel):
    game = models.ForeignKey("gamesaves.Game", blank=True, null=True)
    platform = models.ForeignKey("gamesaves.Platform", blank=True, null=True)
    download_url = models.URLField(_('Game Download URL'),
                blank=True, null=True)

    class Meta:
        verbose_name = _('Game-Platform')
        verbose_name_plural = _('Game-Platforms')

    def __unicode__(self):
        return "%s: %s" % (self.game.title, self.platform.title)


class GameSave(TimeStampedModel):
    num = models.IntegerField(_('Save Game Number'), default=0, blank=True, null=True)
    masteruser = models.ForeignKey("users.MasterUser", blank=True, null=True)
    platform = models.ForeignKey("gamesaves.Platform", blank=True, null=True)
    game = models.ForeignKey("gamesaves.Game", blank=True, null=True)
    data_version = models.CharField(_('Data Version'), max_length=50, blank=True, null=True)
    slot = models.IntegerField(_('Save Game Slot'), default=0, blank=True, null=True)
    description = models.CharField(_('Game Save Description'), help_text="The short description/title of this Game Save e.g. Location, current quest", max_length=200, blank=True, null=True)
    json_data = JSONField(_('Most Current JSON Data'), blank=True, null=True)
    prev_json_data = JSONField(_('Last Successful JSON Data'), blank=True, null=True)
    is_active = models.BooleanField(_('Is Active'), default=True)

    class Meta:
        verbose_name = _('Game Save')
        verbose_name_plural = _('Game Saves')

    def __unicode__(self):
        return "%s - %s %s #%s" % (self.masteruser, self.game, self.platform, self.slot)

    def save(self, *args, **kwargs):
        if not self.num:
            self.num = self.generate_num()
            self.deactivate_previous_gamesaves()
        super(GameSave, self).save(*args, **kwargs)

    def generate_num(self):
        # print "masteruser has gamesave_set?", hasattr(self.masteruser, "gamesave_set")
        if hasattr(self.masteruser, "gamesave_set"):
            existing_gamesaves = self.masteruser.gamesave_set.filter(game=self.game, slot=self.slot)
        else:
            existing_gamesaves = None

        if existing_gamesaves:
            latest_gamesave = existing_gamesaves.latest("num")
            if latest_gamesave.id != self.id:
                num = existing_gamesaves.latest("num").num + 1
            else:
                num = latest_gamesave.num
        else:
            num = 1

        return num

    def deactivate_previous_gamesaves(self):
        if self.num and hasattr(self.masteruser, "gamesave_set"):
            existing_gamesaves = self.masteruser.gamesave_set.filter(game=self.game, slot=self.slot, num__lt=self.num)
            if existing_gamesaves:
                existing_gamesaves.update(is_active=False)



class TrialGameSave(TimeStampedModel):
    num = models.IntegerField(_('Save Game Number'), default=0, blank=True, null=True)
    masteruser = models.ForeignKey("users.MasterUser", blank=True, null=True)
    platform = models.ForeignKey("gamesaves.Platform", blank=True, null=True)
    game = models.ForeignKey("gamesaves.Game", blank=True, null=True)
    data_version = models.CharField(_('Data Version'), max_length=50, blank=True, null=True)
    slot = models.IntegerField(_('Save Game Slot'), default=0, blank=True, null=True)
    description = models.CharField(_('Game Save Description'), help_text="The short description/title of this Game Save e.g. Location, current quest", max_length=200, blank=True, null=True)
    json_data = JSONField(_('Most Current JSON Data'), blank=True, null=True)
    prev_json_data = JSONField(_('Last Successful JSON Data'), blank=True, null=True)
    is_active = models.BooleanField(_('Is Active'), default=True)
    is_completed = models.BooleanField(_('Is Completed'), default=False)

    class Meta:
        verbose_name = _('Demo Game Save')
        verbose_name_plural = _('Demo Game Saves')

    def __unicode__(self):
        return "%s - %s %s Slot %s #%s" % (self.masteruser, self.game, self.platform, self.slot, self.num)

    def generate_num(self):
        # print "masteruser has trialgamesave_set?", hasattr(self.masteruser, "trialgamesave_set")
        if hasattr(self.masteruser, "trialgamesave_set"):
            existing_gamesaves = self.masteruser.trialgamesave_set.filter(game=self.game, slot=self.slot)
        else:
            existing_gamesaves = None

        if existing_gamesaves:
            latest_gamesave = existing_gamesaves.latest("num")
            if latest_gamesave.id != self.id:
                num = existing_gamesaves.latest("num").num + 1
            else:
                num = latest_gamesave.num
        else:
            num = 1

        return num

    def deactivate_previous_gamesaves(self):
        if self.num and hasattr(self.masteruser, "trialgamesave_set"):
            existing_gamesaves = self.masteruser.trialgamesave_set.filter(game=self.game, slot=self.slot, num__lt=self.num)
            if existing_gamesaves:
                existing_gamesaves.update(is_active=False)


    def save(self, *args, **kwargs):
        if not self.num:
            self.num = self.generate_num()
            self.deactivate_previous_gamesaves()
        super(TrialGameSave, self).save(*args, **kwargs)


class GameSession(TimeStampedModel):
    masteruser = models.ForeignKey("users.MasterUser", blank=True, null=True)
    token_key = models.CharField(_("Token Key"), max_length=40)

    class Meta:
        verbose_name = _('User Game Session')
        verbose_name_plural = _('User Game Session')

    def get_session_duration(self):
        heartbeat_list = self.gamesessionheartbeat_set.all()
        total_seconds = (heartbeat_list.count() - 1) * 20
        return total_seconds

    def get_session_minutes_display(self):
        total_seconds = self.get_session_duration()
        return_str_list = []
        h = divmod(total_seconds,3600)  # hours
        m = divmod(h[1],60)  # minutes
        s = m[1]  # seconds

        if h[0] != 0.0:
            return_str_list.append("%d hours" % h[0])
        if m[0] != 0.0:
            return_str_list.append("%d minutes" % m[0])
        if s != 0.0:
            return_str_list.append("%d seconds" % s)

        if return_str_list:
            return " ".join(return_str_list)
        else:
            return ""

    def show_created(self):
        schooluser = self.masteruser.schooluser
        if schooluser:
            local_tz = schooluser.timezone
            local_dt = self.created.replace(tzinfo=pytz.utc).astimezone(local_tz)
            return local_dt.strftime("%d/%m/%Y %H:%M:%S %Z%z")
        else:
            return timezone.localtime(self.created).strftime("%d/%m/%Y %H:%M:%S %Z%z")



class GameSessionHeartBeat(TimeStampedModel):
    gamesession = models.ForeignKey("gamesaves.GameSession", blank=True, null=True)

    class Meta:
        verbose_name = _('User Game Session Heartbeat')
        verbose_name_plural = _('User Game Session Heartbeat')
        ordering = ['-created',]

