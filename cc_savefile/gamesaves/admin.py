import pytz
from django.contrib import admin
from django.utils import timezone

from .models import (Platform, Game, GamePlatform, GameSave, TrialGameSave, GameSession, GameSessionHeartBeat)
from nested_admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline

class GameSaveAdmin(admin.ModelAdmin):
    list_display = ('masteruser', 'platform', 'game', 'slot', 'is_active', 'created', 'modified')
    list_filter = ["platform", "game", "is_active", "data_version",]
    search_fields = ["masteruser__user_id", "masteruser__email",
                    "masteruser__apple_id", "masteruser__googleplay_id",
                    "masteruser__gamecenter_id", "masteruser__filament_id",
                    "masteruser__teachergaming_id", "platform__title", "game__title",
                    "data_version", "slot"]
    list_editable = ["is_active"]


class TrialGameSaveAdmin(admin.ModelAdmin):
    list_display = ('masteruser', 'platform', 'game', 'slot', "num", 'is_active', 'is_completed', 'created', 'modified')
    list_filter = ["platform", "game", "is_active", "is_completed", "data_version",]
    search_fields = ["masteruser__user_id", "masteruser__email",
                    "masteruser__apple_id", "masteruser__googleplay_id",
                    "masteruser__gamecenter_id", "masteruser__filament_id",
                    "masteruser__teachergaming_id", "platform__title", "game__title",
                    "data_version", "slot", "num",]
    list_editable = ["is_active", "is_completed"]


class GameSessionHeartBeatInline(NestedTabularInline):
    model = GameSessionHeartBeat
    extra = 0
    readonly_fields = ('show_created',)

    def show_created(self, obj):
        schooluser = obj.gamesession.masteruser.schooluser
        if schooluser:
            local_tz = schooluser.timezone
            local_dt = obj.created.replace(tzinfo=pytz.utc).astimezone(local_tz)
            return local_dt.strftime("%d/%m/%Y %H:%M:%S %Z%z")
        else:
            return timezone.localtime(obj.created).strftime("%d/%m/%Y %H:%M:%S %Z%z")


class GameSessionAdmin(NestedModelAdmin):
    list_display = ('masteruser', 'session_duration_seconds', 'created', 'token_key', )
    # list_filter = ["masteruser", ]
    search_fields = ["masteruser__user_id", "masteruser__email",
                    "masteruser__apple_id", "masteruser__googleplay_id",
                    "masteruser__gamecenter_id", "masteruser__filament_id",
                    "masteruser__teachergaming_id", "token_key"]

    inlines = [
        GameSessionHeartBeatInline,
        ]

    def session_duration_seconds(self, obj):
        return obj.get_session_duration()

class GamePlatformAdmin(admin.ModelAdmin):
    list_display = ('game', 'platform', )
    list_filter = ["game", "platform",]
    search_fields = ["platform__title", "game__title"]

class PlatformAdmin(admin.ModelAdmin):
    list_display = ('title', "id", 'is_edu', 'is_active' )
    list_editable = ["is_edu", "is_active"]


admin.site.register(Platform, PlatformAdmin)
admin.site.register(Game)
admin.site.register(GameSave, GameSaveAdmin)
admin.site.register(TrialGameSave, TrialGameSaveAdmin)
admin.site.register(GameSession, GameSessionAdmin)
admin.site.register(GamePlatform, GamePlatformAdmin)