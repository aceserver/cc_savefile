# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-06 06:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamesaves', '0010_gameplatform_download_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamesave',
            name='num',
            field=models.IntegerField(blank=True, default=0, null=True, verbose_name='Save Game Number'),
        ),
    ]
