# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-08-21 05:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gamesaves', '0007_auto_20170705_1421'),
    ]

    operations = [
        migrations.CreateModel(
            name='GamePlatform',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('game', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gamesaves.Game')),
                ('platform', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gamesaves.Platform')),
            ],
            options={
                'verbose_name': 'Game-Platform',
                'verbose_name_plural': 'Game-Platforms',
            },
        ),
    ]
