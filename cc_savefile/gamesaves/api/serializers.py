from rest_framework import serializers
from django.utils.dateformat import DateFormat
from django.contrib.auth import authenticate
from djoser import constants
from ..models import GameSave, TrialGameSave

from users.models import MasterUser


class GameSaveSerializer(serializers.ModelSerializer):
    masteruser = serializers.PrimaryKeyRelatedField(queryset=MasterUser.active_objects.all())

    class Meta:
        model = GameSave
        fields = ('masteruser', 'platform', 'game', 'data_version', 'slot',
                  'json_data', 'is_active', )


class TrialGameSaveSerializer(serializers.ModelSerializer):
    masteruser = serializers.PrimaryKeyRelatedField(queryset=MasterUser.active_objects.all())

    class Meta:
        model = TrialGameSave
        fields = ('masteruser', 'platform', 'game', 'data_version', 'slot',
                  'json_data', 'is_active', )
