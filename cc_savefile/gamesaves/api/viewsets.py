import json
from django.http import Http404, HttpResponse
from django.db import transaction
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from rest_framework import status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, detail_route, list_route

from ..models import GameSave, Game, Platform, TrialGameSave
from ..forms import GameSaveForm, TrialGameSaveForm
from .serializers import GameSaveSerializer, TrialGameSaveSerializer

from users.models import MasterUser
from cc_savefile.utils import json_response, bulk_logentry_op
from cc_savefile.auth_backends import CustomTokenAuthentication as TokenAuthentication


'''--------------- GameSaveViewSet ---------------'''
class GameSaveViewSet(viewsets.ModelViewSet):
    '''
    An API to retrieve GameSaves.
    '''
    queryset = GameSave.objects.all()
    serializer_class = GameSaveSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        _qs = super(GameSaveViewSet, self).get_queryset()

        return _qs


    @list_route(methods=['post'])
    @transaction.atomic
    def create_new_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Create new GameSave with the slot provided.
        # -----------------------------------------------------------
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = Game.objects.get(id=data.get("game"))
            platform = Platform.objects.get(id=data.get("platform"))
            slot = data.get("slot")
            data_version = data.get("data_version")
            json_data = data.get("json_data")
            description = data.get("description")

            errors = []

            # Save Point to enable rollback in case of problems when saving
            sid = transaction.savepoint()

            # find gamesave of that slot
            if game and slot:
                try:
                    form = GameSaveForm(data, masteruser=masteruser, is_active=True)

                    if form.is_valid():
                        gamesave_obj = form.save()
                        transaction.savepoint_commit(sid)
                        masteruser.save_last_activity()

                        bulk_logentry_op(request.user.id, ADDITION, [gamesave_obj], "Created new trial game save.")
                    else:
                        errors = [form.errors]
                        transaction.savepoint_rollback(sid)

                except Exception as e:
                    errors.append ("%s" % e)
                    print "Error in creating new GameSave. %s" % e
                    transaction.savepoint_rollback(sid)

            response = {"error"   : errors,
                    "success"     : False if errors else True,
                    "data"        : gamesave_obj.json_data if gamesave_obj else {},
                    "prev_data"   : gamesave_obj.prev_json_data if gamesave_obj else {},
                    "data_version": gamesave_obj.data_version,
                    "gamesave"    : "%s" % gamesave_obj,
                    "game"        : {"id": game.id, "title": game.title},
                    "masteruser"  : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                    "platform"    : {"id": platform.id, "title": platform.title},
                    "slot"        : gamesave_obj.slot,
                    "num"         : gamesave_obj.num,
                    "description" : gamesave_obj.description,
                    "is_active"   : gamesave_obj.is_active,
                    }

        except Exception as e:
            errors = ["Failed to save game. %s" % e]
            response = {"error"         : errors,
                        "success"       : False,
                        "data"          : {},
                        "prev_data"     : {},
                        "data_version"  : "",
                        "gamesave"      : "",
                        "game": {"id"   : "", "title": ""},
                        "masteruser"    : {"user_id": "", "email": ""},
                        "platform"      : {"id": "", "title": ""},
                        "slot"          : "",
                        "num"           : "",
                        "description"   : "",
                        "is_active"     : False
                        }

        return json_response(**response)


    @list_route(methods=['post'])
    @transaction.atomic
    def perform_save(self, request):
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = Game.objects.get(id=data.get("game"))
            platform = Platform.objects.get(id=data.get("platform"))
            slot = data.get("slot")
            data_version = data.get("data_version")
            json_data = data.get("json_data")
            description = data.get("description")

            gamesave_obj = GameSave.objects.none()

            errors = []

            # Save Point to enable rollback in case of problems when saving
            sid = transaction.savepoint()

            # find gamesave of that slot
            if game and slot:
                gamesaves = masteruser.gamesave_set.filter(game=game, slot=slot, is_active=True)
                if gamesaves:
                    try:
                        gamesave_obj = gamesaves.latest("created")
                        jdata = json.loads(json_data)
                        gamesave_obj.json_data = jdata
                        gamesave_obj.platform = platform
                        gamesave_obj.data_version = data_version
                        gamesave_obj.description = description
                        gamesave_obj.is_active = True
                        gamesave_obj.save()

                        masteruser.save_last_activity()

                        bulk_logentry_op(request.user.id, CHANGE, [gamesave_obj], "Updated game save.")

                        transaction.savepoint_commit(sid)

                    except Exception as e:
                        errors.append ("%s" % e)
                        transaction.savepoint_rollback(sid)
                else:
                    try:
                        form = GameSaveForm(data, masteruser=masteruser, is_active=True)

                        if form.is_valid():
                            gamesave_obj = form.save()
                            transaction.savepoint_commit(sid)

                            masteruser.save_last_activity()

                            bulk_logentry_op(request.user.id, ADDITION, [gamesave_obj], "Created new game save.")
                        else:
                            errors = [form.errors]
                            transaction.savepoint_rollback(sid)

                    except Exception as e:
                        errors.append ("%s" % e)
                        transaction.savepoint_rollback(sid)

            response = {"error"   : errors,
                    "success"     : False if errors else True,
                    "data"        : gamesave_obj.json_data if gamesave_obj else {},
                    "prev_data"   : gamesave_obj.prev_json_data if gamesave_obj else {},
                    "data_version": data_version,
                    "gamesave"    : "%s" % gamesave_obj,
                    "game"        : {"id": game.id, "title": game.title},
                    "masteruser"  : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                    "platform"    : {"id": platform.id, "title": platform.title},
                    "slot"        : slot,
                    "description" : description,
                    "is_active"   : gamesave_obj.is_active
                    }

        except Exception as e:
            errors = ["Failed to save game. %s" % e]
            response = {"error"         : errors,
                        "success"       : False,
                        "data"          : {},
                        "prev_data"     : {},
                        "data_version"  : "",
                        "gamesave"      : "",
                        "game": {"id"   : "", "title": ""},
                        "masteruser"    : {"user_id": "", "email": ""},
                        "platform"      : {"id": "", "title": ""},
                        "slot"          : "",
                        "description"   : "",
                        "is_active"     : False
                        }


        return json_response(**response)


    @list_route(methods=['post'])
    def load_save(self, request):
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")
            game_obj = Game.objects.get(id=game)
            response = {"error": "Failed to get Save File. Save File not found for %s Slot #%s." % (game_obj, slot),
                    "success"  : False,
                    "data"     : {},
                    "prev_data": {}
                    }

            gamesaves = masteruser.gamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                latest_gamesave = gamesaves.latest("created")

                masteruser.save_last_activity()

                response  = {"error" : "",
                        "success"    : True,
                        "data"       : latest_gamesave.json_data,
                        "prev_data"  : latest_gamesave.prev_json_data,
                        "gamesave"   : "%s" % latest_gamesave,
                        "game"       : {"id": latest_gamesave.game.id, "title": latest_gamesave.game.title},
                        "masteruser" : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                        "platform"   : {"id": latest_gamesave.platform.id, "title": latest_gamesave.platform.title},
                        "slot"       : latest_gamesave.slot,
                        "description": latest_gamesave.description if latest_gamesave.description else "",
                        "is_active"  : latest_gamesave.is_active
                        }

            return json_response(**response)
        except Exception as e:
            response = {"error"  : "Failed to get Save File. %s" % e,
                    "success"    : False,
                    "data"       : {},
                    "prev_data"  : {},
                    "gamesave"   : "",
                    "game"       : "",
                    "masteruser" : "",
                    "platform"   : "",
                    "slot"       : "",
                    "description": "",
                    "is_active"  : "",
                    }
            return json_response(**response)


    @list_route(methods=['post'])
    def get_save_slots(self, request):
        data = request.POST
        return_data = []
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")

            response = {"error": "Failed to get Save File Slots. No Save Files found.",
                    "success"  : False,
                    "data"     : return_data,
                    }

            gamesaves = masteruser.gamesave_set.filter(game__id=game, is_active=True)
            if gamesaves:
                masteruser.save_last_activity()

                for gs in gamesaves:
                    gs_data = {"slot"         : gs.slot,
                                "modified"    : gs.modified.strftime("%d/%m/%Y %I:%M:%S %p"),
                                "description" : gs.description,
                                "json_data"   : gs.json_data,
                                "prev_json_data": gs.prev_json_data}
                    return_data.append(gs_data)

                response  = {"error": "",
                        "success"   : True,
                        "data"      : return_data,
                        }

            return json_response(**response)
        except Exception as e:
            response = {"error": "Failed to get Save File Slots. %s" % e,
                    "success"   : False,
                    "data"      : return_data,
                    }
            return json_response(**response)


    @list_route(methods=['post'])
    def load_success_backup_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Run this if game can load from json_data successfully.
        # It will backup json_data into prev_json_data.
        # If failed to load game from json_data, use prev_json_data,
        # but don't run this function.
        # Just run perform_save() in the next save.
        # -----------------------------------------------------------

        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")

            response = {"message": "Failed to backup Save File. Save File not found.",
                        "success": False,
                        }

            gamesaves = masteruser.gamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                with transaction.atomic():
                    latest_gamesave = gamesaves.latest("created")
                    latest_gamesave.prev_json_data = latest_gamesave.json_data
                    latest_gamesave.save()

                    masteruser.save_last_activity()

                    bulk_logentry_op(masteruser.auth_user.id, CHANGE, [latest_gamesave], "Backed-up json_data to prev_json_data.")

                    response = {"message": "Backed-up %s successfully!" % latest_gamesave,
                                "success": True,
                                }

            return json_response(**response)

        except Exception as e:
            response = {"message": "Failed to backup Save File. %s" % e,
                        "success": False,
                        }
            return json_response(**response)


    @list_route(methods=['post'])
    def delete_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Deleting save game will only turn it into inactive.
        # -----------------------------------------------------------

        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")

            response = {"message": "Failed to delete Save File. Save File not found.",
                        "success": False,
                        }

            gamesaves = masteruser.gamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                with transaction.atomic():
                    gamesave_list = [gs for gs in gamesaves]
                    latest_gamesave = gamesaves.latest("created")
                    gamesaves.update(is_active=False)
                    # print gamesave_list

                    masteruser.save_last_activity()

                    bulk_logentry_op(masteruser.auth_user.id, CHANGE, gamesave_list, "Set GameSave to inactive.")

                    response = {"message": "Deleted %s successfully!" % latest_gamesave,
                                "success": True,
                                }

            return json_response(**response)

        except Exception as e:
            response = {"message": "Failed to delete Save File. %s" % e,
                        "success": False,
                        }
            return json_response(**response)


'''--------------- TrialGameSaveViewSet ---------------'''
class TrialGameSaveViewSet(viewsets.ModelViewSet):
    '''
    An API to retrieve TrialGameSaves.
    '''
    queryset = TrialGameSave.objects.all()
    serializer_class = TrialGameSaveSerializer
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        _qs = super(TrialGameSaveViewSet, self).get_queryset()
        return _qs


    @list_route(methods=['post'])
    @transaction.atomic
    def perform_save(self, request):
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = Game.objects.get(id=data.get("game"))
            platform = Platform.objects.get(id=data.get("platform"))
            slot = data.get("slot")
            data_version = data.get("data_version")
            json_data = data.get("json_data")
            description = data.get("description")

            gamesave_obj = TrialGameSave.objects.none()

            errors = []

            # Save Point to enable rollback in case of problems when saving
            sid = transaction.savepoint()

            # find gamesave of that slot
            if game and slot:
                # print hasattr( masteruser, "trialgamesave_set")
                if hasattr(masteruser, "trialgamesave_set"):
                    gamesaves = masteruser.trialgamesave_set.filter(game=game,
                                            slot=slot, is_active=True, is_completed=False)
                else:
                    gamesaves = None
                # print gamesaves
                if gamesaves:
                    # if Trial Game Save with slot is found, continue saving
                    # in the same Trial Game Save session
                    try:
                        gamesave_obj = gamesaves.latest("num")
                        jdata = json.loads(json_data)
                        gamesave_obj.json_data = jdata
                        gamesave_obj.platform = platform
                        gamesave_obj.data_version = data_version
                        gamesave_obj.description = description
                        gamesave_obj.is_active = True
                        gamesave_obj.is_completed = False
                        gamesave_obj.save()

                        masteruser.save_last_activity()

                        bulk_logentry_op(request.user.id, CHANGE, [gamesave_obj], "Updated game save.")

                        transaction.savepoint_commit(sid)

                    except Exception as e:
                        errors.append ("%s" % e)
                        transaction.savepoint_rollback(sid)
                else:
                    # if can't find correct TrialGameSave with
                    # the slot provided, then create new Trial Game Save session.
                    # print "no gamesaves, creating new session"
                    try:
                        form = TrialGameSaveForm(data, masteruser=masteruser)

                        if form.is_valid():
                            gamesave_obj = form.save()
                            transaction.savepoint_commit(sid)

                            masteruser.save_last_activity()

                            bulk_logentry_op(request.user.id, ADDITION, [gamesave_obj], "Created new trial game save.")
                        else:
                            errors = [form.errors]
                            transaction.savepoint_rollback(sid)

                    except Exception as e:
                        errors.append ("%s" % e)
                        print "Error in creating new TrialGameSave. %s" % e
                        transaction.savepoint_rollback(sid)

            response = {"error"   : errors,
                    "success"     : False if errors else True,
                    "data"        : gamesave_obj.json_data if gamesave_obj else {},
                    "prev_data"   : gamesave_obj.prev_json_data if gamesave_obj else {},
                    "data_version": data_version,
                    "gamesave"    : "%s" % gamesave_obj,
                    "game"        : {"id": game.id, "title": game.title},
                    "masteruser"  : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                    "platform"    : {"id": platform.id, "title": platform.title},
                    "slot"        : gamesave_obj.slot,
                    "num"         : gamesave_obj.num,
                    "description" : gamesave_obj.description,
                    "is_active"   : gamesave_obj.is_active,
                    "is_completed": gamesave_obj.is_completed
                    }

        except Exception as e:
            errors = ["Failed to save game. %s" % e]
            response = {"error"         : errors,
                        "success"       : False,
                        "data"          : {},
                        "prev_data"     : {},
                        "data_version"  : "",
                        "gamesave"      : "",
                        "game": {"id"   : "", "title": ""},
                        "masteruser"    : {"user_id": "", "email": ""},
                        "platform"      : {"id": "", "title": ""},
                        "slot"          : "",
                        "num"           : "",
                        "description"   : "",
                        "is_active"     : False,
                        "is_completed"  : False,
                        }

        return json_response(**response)


    @list_route(methods=['post'])
    @transaction.atomic
    def create_new_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Create new TrialGameSave with the slot provided.
        # -----------------------------------------------------------
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = Game.objects.get(id=data.get("game"))
            platform = Platform.objects.get(id=data.get("platform"))
            slot = data.get("slot")
            data_version = data.get("data_version")
            json_data = data.get("json_data")
            description = data.get("description")

            errors = []

            # Save Point to enable rollback in case of problems when saving
            sid = transaction.savepoint()

            # find gamesave of that slot
            if game and slot:
                try:
                    form = TrialGameSaveForm(data, masteruser=masteruser)

                    if form.is_valid():
                        gamesave_obj = form.save()
                        transaction.savepoint_commit(sid)
                        masteruser.save_last_activity()

                        bulk_logentry_op(request.user.id, ADDITION, [gamesave_obj], "Created new trial game save.")
                    else:
                        errors = [form.errors]
                        transaction.savepoint_rollback(sid)

                except Exception as e:
                    errors.append ("%s" % e)
                    print "Error in creating new TrialGameSave. %s" % e
                    transaction.savepoint_rollback(sid)

            response = {"error"   : errors,
                    "success"     : False if errors else True,
                    "data"        : gamesave_obj.json_data if gamesave_obj else {},
                    "prev_data"   : gamesave_obj.prev_json_data if gamesave_obj else {},
                    "data_version": gamesave_obj.data_version,
                    "gamesave"    : "%s" % gamesave_obj,
                    "game"        : {"id": game.id, "title": game.title},
                    "masteruser"  : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                    "platform"    : {"id": platform.id, "title": platform.title},
                    "slot"        : gamesave_obj.slot,
                    "num"         : gamesave_obj.num,
                    "description" : gamesave_obj.description,
                    "is_active"   : gamesave_obj.is_active,
                    "is_completed": gamesave_obj.is_completed
                    }

        except Exception as e:
            errors = ["Failed to save game. %s" % e]
            response = {"error"         : errors,
                        "success"       : False,
                        "data"          : {},
                        "prev_data"     : {},
                        "data_version"  : "",
                        "gamesave"      : "",
                        "game": {"id"   : "", "title": ""},
                        "masteruser"    : {"user_id": "", "email": ""},
                        "platform"      : {"id": "", "title": ""},
                        "slot"          : "",
                        "num"           : "",
                        "description"   : "",
                        "is_active"     : False,
                        "is_completed"  : False,
                        }

        return json_response(**response)


    @list_route(methods=['post'])
    def load_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Load TrialGameSave of the slot provided with the latest num.
        # -----------------------------------------------------------
        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")
            game_obj = Game.objects.get(id=game)
            response = {"error": "Failed to get Save File. Save File not found for %s Slot #%s." % (game_obj, slot),
                    "success"  : False,
                    "data"     : {},
                    "prev_data": {}
                    }

            gamesaves = masteruser.trialgamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                latest_gamesave = gamesaves.latest("num")

                masteruser.save_last_activity()

                response  = {"error" : "",
                        "success"    : True,
                        "data"       : latest_gamesave.json_data,
                        "prev_data"  : latest_gamesave.prev_json_data,
                        "gamesave"   : "%s" % latest_gamesave,
                        "game"       : {"id": latest_gamesave.game.id, "title": latest_gamesave.game.title},
                        "masteruser" : {"user_id": masteruser.user_id, "email": masteruser.auth_user.email},
                        "platform"   : {"id": latest_gamesave.platform.id, "title": latest_gamesave.platform.title},
                        "slot"       : latest_gamesave.slot,
                        "num"        : latest_gamesave.num,
                        "description": latest_gamesave.description if latest_gamesave.description else "",
                        "is_active"  : latest_gamesave.is_active,
                        "is_completed"  : latest_gamesave.is_completed
                        }

            return json_response(**response)
        except Exception as e:
            response = {"error"  : "Failed to get Save File. %s" % e,
                    "success"    : False,
                    "data"       : {},
                    "prev_data"  : {},
                    "gamesave"   : "",
                    "game"       : "",
                    "masteruser" : "",
                    "platform"   : "",
                    "slot"       : "",
                    "num"        : "",
                    "description": "",
                    "is_active"  : "",
                    "is_completed"  : "",
                    }
            return json_response(**response)


    @list_route(methods=['post'])
    def get_save_slots(self, request):
        data = request.POST
        return_data = []
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")

            response = {"error": "Failed to get Save File Slots. No Save Files found.",
                    "success"  : False,
                    "data"     : return_data,
                    }

            gamesaves = masteruser.trialgamesave_set.filter(game__id=game, is_active=True, is_completed=False)
            if gamesaves:
                for gs in gamesaves:
                    masteruser.save_last_activity()

                    gs_data = {"slot"           : gs.slot,
                                "num"           : gs.num,
                                "modified"      : gs.modified.strftime("%d/%m/%Y %I:%M:%S %p"),
                                "description"   : gs.description,
                                "is_completed"  : gs.is_completed,
                                "json_data"     : gs.json_data,
                                "prev_json_data": gs.prev_json_data}
                    return_data.append(gs_data)

                response  = {"error": "",
                        "success"   : True,
                        "data"      : return_data,
                        }

            return json_response(**response)
        except Exception as e:
            response = {"error": "Failed to get Save File Slots. %s" % e,
                    "success"   : False,
                    "data"      : return_data,
                    }
            return json_response(**response)


    @list_route(methods=['post'])
    def load_success_backup_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Run this if game can load from json_data successfully.
        # It will backup json_data into prev_json_data.
        # If failed to load game from json_data, use prev_json_data,
        # but don't run this function.
        # Just run perform_save() in the next save.
        # -----------------------------------------------------------

        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")

            response = {"message": "Failed to backup Save File. Save File not found.",
                        "success": False,
                        }

            gamesaves = masteruser.trialgamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                with transaction.atomic():
                    latest_gamesave = gamesaves.latest("num")
                    latest_gamesave.prev_json_data = latest_gamesave.json_data
                    latest_gamesave.save()

                    masteruser.save_last_activity()

                    bulk_logentry_op(masteruser.auth_user.id, CHANGE, [latest_gamesave], "Backed-up json_data to prev_json_data.")

                    response = {"message": "Backed-up %s successfully!" % latest_gamesave,
                                "success": True,
                                }

            return json_response(**response)

        except Exception as e:
            response = {"message": "Failed to backup Save File. %s" % e,
                        "success": False,
                        }
            return json_response(**response)


    @list_route(methods=['post'])
    def mark_as_completed(self, request):
        # ----------------------- NOTE: -----------------------------
        # Update TrialGameSave of the slot provided with the latest
        # num as is_completed = True.
        # -----------------------------------------------------------

        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")

            response = {"message": "Failed to mark Save File as completed. Save File not found.",
                        "success": False,
                        }

            gamesaves = masteruser.trialgamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                with transaction.atomic():
                    latest_gamesave = gamesaves.latest("num")
                    latest_gamesave.is_completed = True
                    latest_gamesave.save()

                    masteruser.save_last_activity()

                    bulk_logentry_op(masteruser.auth_user.id, CHANGE, [latest_gamesave], "Set TrialGameSave as completed.")

                    response = {"message": "Marked %s as completed successfully!" % latest_gamesave,
                                "success": True,
                                }

            return json_response(**response)

        except Exception as e:
            response = {"message": "Failed to mark Save File as completed. %s" % e,
                        "success": False,
                        }
            return json_response(**response)



    @list_route(methods=['post'])
    def delete_save(self, request):
        # ----------------------- NOTE: -----------------------------
        # Deleting save game will only turn it into inactive.
        # -----------------------------------------------------------

        data = request.POST
        try:
            masteruser = self.request.user.masteruser
            game = data.get("game")
            slot = data.get("slot")
            # num = data.get("num")

            response = {"message": "Failed to delete Save File. Save File not found.",
                        "success": False,
                        }

            gamesaves = masteruser.trialgamesave_set.filter(game__id=game, slot=slot, is_active=True)
            if gamesaves:
                with transaction.atomic():
                    # if num:
                    #     latest_gamesave = gamesaves.get(num=num)
                    # else:
                    latest_gamesave = gamesaves.latest("num")
                    latest_gamesave.is_active = False
                    latest_gamesave.save()

                    masteruser.save_last_activity()

                    bulk_logentry_op(masteruser.auth_user.id, CHANGE, [latest_gamesave], "Set TrialGameSave to inactive.")

                    response = {"message": "Deleted %s successfully!" % latest_gamesave,
                                "success": True,
                                }

            return json_response(**response)

        except Exception as e:
            response = {"message": "Failed to delete Save File. %s" % e,
                        "success": False,
                        }
            return json_response(**response)


