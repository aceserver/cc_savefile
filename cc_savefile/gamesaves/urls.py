from django.conf.urls import include, url
from django.contrib.auth.views import logout
from rest_framework import routers

from .api.viewsets import GameSaveViewSet, TrialGameSaveViewSet

router = routers.DefaultRouter()

router.register(r'gamesave', GameSaveViewSet)
router.register(r'trialgamesave', TrialGameSaveViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/browseable/auth', include(
        'rest_framework.urls', namespace='rest_framework')),
]
