# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-26 10:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_auto_20170426_1024'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='q_type',
            field=models.IntegerField(blank=True, choices=[(1, 'Short Answer'), (2, 'Single Choice'), (3, 'Multiple Choice'), (4, 'Drawing')], default=1, null=True, verbose_name='Type of question'),
        ),
    ]
