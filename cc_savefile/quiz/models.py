from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.postgres.fields import JSONField

class Quiz(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=500, blank=True, null=True)
    is_active = models.BooleanField(_('Is Active'), default=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Quiz')
        verbose_name_plural = _('Quizzes')


class Question(TimeStampedModel):
    ANSWER = 1
    SINGLE = 2
    MULTI = 3
    DRAW = 4

    TYPE_LIST = (
        (ANSWER, _("Short Answer")),
        (SINGLE, _("Single Choice")),
        (MULTI, _("Multiple Choice")),
        (DRAW, _("Drawing")),
    )

    quiz = models.ForeignKey("quiz.Quiz", verbose_name=_('Quiz'),
                    blank=True, null=True)
    content = models.TextField(_('Question'), blank=True, null=True)
    q_type = models.IntegerField(_("Type of question"), choices=TYPE_LIST,
                default=ANSWER, blank=True, null=True)
    answer_text = models.CharField(_('Answer'), max_length=1000, blank=True, null=True)
    image = models.ImageField(upload_to = 'question_images', null=True, blank=True)
    order = models.IntegerField(_('Ordering'), blank=True, null=True, default=0)

    class Meta:
        ordering = ('order', )

    def __unicode__(self):
        return "%s - %s" % (self.content, self.get_q_type_display())



class AnswerChoice(TimeStampedModel):
    question = models.ForeignKey("quiz.Question", verbose_name=_('Question'),
                    blank=True, null=True)
    content = models.CharField(_('Answer'), max_length=1000, blank=True, null=True)
    image = models.ImageField(upload_to = 'answer_choice_images', null=True, blank=True)
    is_correct_answer = models.BooleanField(_('Is Correct Answer'), default=False)
    order = models.IntegerField(_('Ordering'), blank=True, null=True, default=0)

    def __unicode__(self):
        return "%s - %s" % (self.question, self.content)

    class Meta:
        ordering = ('order', )


class AnswerAttributeValue(TimeStampedModel):
    answer = models.ForeignKey("quiz.AnswerChoice", verbose_name=_('Answer'),
                    blank=True, null=True)
    quiz_attribute = models.ForeignKey("quiz.QuizAttribute", verbose_name=_('Quiz Attribute'),
                    blank=True, null=True)
    value = models.DecimalField(_('Value'), default=1, decimal_places=2, max_digits=10)
    is_active = models.BooleanField(_('Is Active'), default=True)
    order = models.IntegerField(_('Ordering'), blank=True, null=True, default=0)

    def __unicode__(self):
        return "%s - %s (%s)" % (self.answer, self.quiz_attribute, self.value)

    class Meta:
        ordering = ('order', )


class QuizAttribute(TimeStampedModel):
    title = models.CharField(_('Title'), max_length=500, blank=True, null=True)
    description = models.TextField(_('Description'), max_length=1000, blank=True, null=True)
    is_active = models.BooleanField(_('Is Active'), default=True)
    order = models.IntegerField(_('Ordering'), blank=True, null=True, default=0)

    def __unicode__(self):
        return "%s" % self.title

    class Meta:
        ordering = ('order', )


class Sitting(TimeStampedModel):
    '''
    Model to store quiz sitting of the MasterUser.
    '''
    masteruser = models.ForeignKey("users.MasterUser", blank=True, null=True)
    quiz = models.ForeignKey("quiz.Quiz", verbose_name=_('Quiz'),
                    blank=True, null=True)
    answers = JSONField(_('Answers'), blank=True, null=True)
    trialgamesave = models.ForeignKey("gamesaves.TrialGameSave", verbose_name=_('Trial Game Save Session'),
                    blank=True, null=True)
    is_completed = models.BooleanField(_('Is Completed'), default=False)


    def show_answers (self):
        data = []
        for q_data in self.answers:
            question_order = q_data["order"] + 1
            question_id = q_data["question"]
            answer = q_data["answer"]
            is_correct = q_data["is_correct"]
            question_obj = Question.objects.get(id=question_id)
            if question_obj.q_type == Question.ANSWER:
                obj = {"order": question_order, "question": question_obj, "answer": answer, "is_correct": is_correct}
            elif question_obj.q_type in [Question.SINGLE, Question.MULTI]:
                obj = {"order": question_order, "question": question_obj, "answer": [AnswerChoice.objects.get(id=a) for a in answer], "choices": question_obj.answerchoice_set.all(), "is_correct": is_correct}
            if question_obj.q_type == Question.DRAW:
                sittingimage = SittingImage.objects.get(id=answer)
                obj = {"order": question_order, "question": question_obj, "answer": sittingimage.image, "is_correct": is_correct}
            data.append(obj)
        return data

    def find_answer(self, question):
        data = ""
        for q_data in self.show_answers():
            question_obj = q_data["question"]
            if question == question_obj:
                data = q_data
        return data


class SittingImage(TimeStampedModel):
    '''
    Model to store uploaded images per sitting, linked to a Question.
    '''
    sitting = models.ForeignKey("quiz.Sitting", blank=True, null=True)
    question = models.ForeignKey("quiz.Question", verbose_name=_('Question'),
                    blank=True, null=True)
    image = models.ImageField(upload_to = 'answer_images', null=True, blank=True)