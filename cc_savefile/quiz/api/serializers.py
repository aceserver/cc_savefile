from rest_framework import serializers
from django.utils.dateformat import DateFormat
from django.contrib.auth import authenticate
from djoser import constants
from ..models import Quiz, Sitting

from users.models import MasterUser


class SittingSerializer(serializers.ModelSerializer):
    masteruser = serializers.PrimaryKeyRelatedField(queryset=MasterUser.active_objects.all())

    class Meta:
        model = Sitting
        fields = ('masteruser', )
