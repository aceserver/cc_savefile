import json
from django.http import Http404, HttpResponse
from django.db import transaction
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION

from rest_framework import status, viewsets, generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, detail_route, list_route, authentication_classes

from ..models import Quiz, Question, AnswerChoice, Sitting, SittingImage
from .serializers import SittingSerializer

from users.models import MasterUser
from gamesaves.models import TrialGameSave
from cc_savefile.utils import json_response, bulk_logentry_op
from cc_savefile.auth_backends import CustomTokenAuthentication as TokenAuthentication


'''--------------- QuizViewSet ---------------'''
class QuizViewSet(viewsets.ModelViewSet):
    '''
    An API to check for Quiz Sitting statuses.
    '''
    queryset = Sitting.objects.all()
    serializer_class = SittingSerializer
    # authentication_classes = (TokenAuthentication, )
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        _qs = super(QuizViewSet, self).get_queryset()
        return _qs

    @list_route(methods=['get'])
    def check_sitting_complete(self, request):
        '''
        API to check if sitting is complete/exist.
        Include quiz, token, trialgamesave (or game, slot, num, if no trialgamesave.id)
        in the querystring when you call the GET request URL.
        '''
        data = request.GET
        response = {"complete": False, "success": True}
        quiz_id = data.get("quiz", "")
        trialgamesave_id = data.get("trialgamesave", "")
        token = data.get("token", "")
        user, token = TokenAuthentication().authenticate_credentials(key=token)

        try:
            user.masteruser.save_last_activity()
        except:
            pass

        if not trialgamesave_id:
            game = self.request.GET.get("game", "")
            slot = self.request.GET.get("slot", "")
            # num = self.request.GET.get("num", "")
            if game and slot:
                trialgamesave = TrialGameSave.objects.filter(masteruser=user.masteruser,
                                                    game=int(game), slot=int(slot)).latest('num')
            else:
                trialgamesave = TrialGameSave.objects.none()
        else:
            trialgamesave = TrialGameSave.objects.get(id=trialgamesave_id)

        if quiz_id:
            try:
                quiz = Quiz.objects.get(id=quiz_id)
            except:
                response["success"] = False
                return json_response(**response)
        else:
            response["success"] = False
            return json_response(**response)

        if trialgamesave:
            exist_sitting = Sitting.objects.filter(trialgamesave=trialgamesave, masteruser=user.masteruser, quiz=quiz).first()
            if not exist_sitting:
                response["success"] = False
                return json_response(**response)
        else:
            exist_sitting = Sitting.objects.filter(trialgamesave__isnull=True, masteruser=user.masteruser, quiz=quiz)
            if not exist_sitting:
                response["success"] = False
                return json_response(**response)
            else:
                exist_sitting = exist_sitting.latest()

        if exist_sitting and exist_sitting.is_completed:
            response['complete'] = True

        return json_response(**response)


