from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy as reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.utils.translation import ugettext_lazy as _
from django.template.response import TemplateResponse
from django.views.generic import CreateView, TemplateView, DetailView
from django.core.files import File
# from rest_framework.authentication import TokenAuthentication
import re, io
import base64

from cc_savefile.utils import json_response
from cc_savefile.auth_backends import CustomTokenAuthentication as TokenAuthentication
from users.models import MasterUser
from gamesaves.models import TrialGameSave
from .models import Quiz, Question, AnswerChoice, Sitting, SittingImage

class QuizView(TemplateView):
    template_name = "quiz/quiz_sitting.html"

    def get(self, *args, **kwargs):
        '''
        Must provide the quiz pk into kwargs of URL, token auth in HTTP_AUTHORIZATION
        META Headers, game pk, slot and num in URL params. Code will get the user through
        the auth token provided, or get currently logged in user.
        '''
        super(QuizView, self).get(*args, **kwargs)
        context = {}
        try:
            context['complete'] = False
            quiz = Quiz.objects.get(id=self.kwargs.get("pk"))
            context['quiz'] = quiz

            context['token_str'] = self.request.GET.get("token", "")
            context['browsermode'] = True if self.request.GET.get("browsermode", False) else False
            if context['token_str']:
                user, token = TokenAuthentication().authenticate_credentials(key=context['token_str'])
            else:
                auth = self.request.META.get("HTTP_AUTHORIZATION")
                if auth:
                    context['token_str'] = auth.split()[1]
                    user, token = TokenAuthentication().authenticate_credentials(key=context['token_str'])
                elif self.request.user.is_authenticated():
                    context['token_str'] = self.request.user.auth_token.key
                    user = self.request.user

            if hasattr(user, "masteruser") and user.masteruser:
                game = self.request.GET.get("game", "")
                slot = self.request.GET.get("slot", "")
                # num = self.request.GET.get("num", "")
                if game and slot:
                    trialgamesave = TrialGameSave.objects.filter(masteruser=user.masteruser,
                                                        game=int(game), slot=int(slot)).latest('num')
                else:
                    trialgamesave = TrialGameSave.objects.none()
                context['trialgamesave'] = trialgamesave

                exist_sitting = Sitting.objects.filter(trialgamesave=trialgamesave, masteruser=user.masteruser, quiz=quiz).first()
                if exist_sitting and exist_sitting.is_completed:
                    context['complete'] = True
                    context['message'] = "You have already completed this quiz. You may go back to your game and continue from there."
            else:
                context['trialgamesave'] = TrialGameSave.objects.none()
        except Exception as e:
            context['complete'] = True
            context['message'] = e
        
        return TemplateResponse(self.request, self.template_name, context)

    def post(self, *args, **kwargs):
        '''
        Post answers with Token auth in HTTP_AUTHORIZATION Meta Headers.
        '''
        data = {"response": []}

        quiz = Quiz.objects.get(id=self.kwargs.get("pk"))

        questions = quiz.question_set.all()

        browsermode = self.request.POST.get("browsermode")

        token_str = self.request.POST.get("masteruser")

        trialgamesave_id = self.request.POST.get("trialgamesave", "")

        user, token = TokenAuthentication().authenticate_credentials(key=token_str)
        if hasattr(user, "masteruser") and user.masteruser:
            masteruser = user.masteruser
        else:
            return json_response(**data)

        # masteruser = MasterUser.objects.get(id=119)
        # sitting = Sitting.objects.get(id=1)
        if trialgamesave_id:
            trialgamesave = TrialGameSave.objects.get(id=trialgamesave_id)
            exist_sitting = Sitting.objects.filter(trialgamesave=trialgamesave, masteruser=masteruser, quiz=quiz).first()
            if not exist_sitting:
                sitting = Sitting.objects.create(trialgamesave=trialgamesave, masteruser=masteruser, quiz=quiz)
            else:
                sitting = exist_sitting
        else:
            sitting = Sitting.objects.create(masteruser=masteruser, quiz=quiz)

        is_completed = True
        for q in questions:
            field_name = "question_%s_pk_%s" % (q.order, q.id)
            answer_list = self.request.POST.getlist(field_name)

            if q.q_type == q.ANSWER:
                if len(answer_list) == 1:
                    if answer_list[0]:
                        answer = answer_list[0]
                        if answer == q.answer_text:
                            obj = {"is_correct": True, "is_answered": True, "answer": answer, "question": q.id, "order": q.order}
                    else:
                        obj = {"is_correct": False, "is_answered": False, "answer": "", "question": q.id, "order": q.order}
                        is_completed = False

                else:
                    obj = {"is_correct": False, "is_answered": False, "answer": "", "question": q.id, "order": q.order}
                    is_completed = False

                data["response"].append(obj)

            elif q.q_type == q.SINGLE or q.q_type == q.MULTI:
                correct_answers = q.answerchoice_set.filter(is_correct_answer=True)
                obj = {"is_correct": True, "is_answered": True, "answer": [], "question": q.id, "order": q.order}
                if answer_list:
                    for a in answer_list:
                        obj["answer"].append(int(a))
                        try:
                            if not int(a) in correct_answers.values_list("id", flat=True):
                                obj["is_correct"] = False
                        except:
                            pass
                else:
                    obj = {"is_correct": False, "is_answered": False, "answer": [], "question": q.id, "order": q.order}
                    is_completed = False

                data["response"].append(obj)

            elif q.q_type == q.DRAW:
                if len(answer_list) == 1:
                    if answer_list[0]:
                        answer = answer_list[0]
                        dataUrlPattern = re.compile('data:image/(png|jpeg);base64,(.*)$')

                        ImageData = answer
                        ImageData = dataUrlPattern.match(ImageData).group(2)

                        # If none or len 0, means illegal image data
                        if ImageData == None or len(ImageData) == 0:
                            # PRINT ERROR MESSAGE HERE
                            pass

                        # Decode the 64 bit string into 32 bit
                        ImageData = base64.b64decode(ImageData)
                        img_io = io.BytesIO(ImageData)

                        sitting_image = SittingImage.objects.filter(sitting=sitting, question=q).first()
                        if not sitting_image:
                            sitting_image = SittingImage.objects.create(sitting=sitting, question=q)
                        else:
                            # if SittingImage existed for this question, delete the previous image to prevent redundant dupes
                            sitting_image.image.delete()

                        filename = "quiz_%s_question_%s_user_%s_image_%s.jpg" % (quiz.id, q.id, masteruser.user_id, sitting_image.created.strftime("%Y%m%d_%H%M%S"))
                        sitting_image.image.save(filename, File(img_io))
                        obj = {"is_correct": "Submitted Image", "is_answered": True, "answer": sitting_image.id, "question": q.id, "order": q.order}
                    else:
                        obj = {"is_correct": "No Image", "is_answered": False, "answer":"", "question": q.id, "order": q.order}
                        is_completed = False

                else:
                    obj = {"is_correct": "No Image", "is_answered": False, "answer":"", "question": q.id, "order": q.order}
                    is_completed = False

                data["response"].append(obj)

        if is_completed:
            # print data["response"]
            sitting.answers = data["response"]
            sitting.is_completed = True
            sitting.save()
            # return json_response(**data)
            if trialgamesave_id:
                if browsermode == 1:
                    querystring = "?token=%s&trialgamesave=%s&browsermode=%s" % (token.key, trialgamesave.id, browsermode)
                else:
                    querystring = "?token=%s&trialgamesave=%s" % (token.key, trialgamesave.id)
            else:
                if browsermode == 1:
                    querystring = "?token=%s&browsermode=%s" % (token.key, browsermode)
                else:
                    querystring = "?token=%s" % token.key
            return_response = {"redirect": reverse('quiz_sitting_complete', kwargs={"pk": quiz.id}) + querystring}
            return json_response(**return_response)
        else:
            return json_response(**data)


class QuizCompleteView(TemplateView):
    template_name = "quiz/quiz_sitting_complete.html"

    def get(self, *args, **kwargs):
        '''

        '''
        super(QuizCompleteView, self).get(*args, **kwargs)
        context = {}
        context['quiz'] = Quiz.objects.get(id=self.kwargs.get("pk"))

        auth = self.request.META.get("HTTP_AUTHORIZATION")
        if auth:
            context['token_str'] = auth.split()[1]
            # user, token = TokenAuthentication().authenticate_credentials(key=context['token_str'])
        elif self.request.user.is_authenticated():
            context['token_str'] = self.request.user.auth_token.key
            # user = self.request.user
        else:
            context['token_str'] = self.request.GET.get("token", "")
            # user, token = TokenAuthentication().authenticate_credentials(key=context['token_str'])

        context['trialgamesave'] = self.request.GET.get("trialgamesave", "")
        browsermode = self.request.GET.get("browsermode", None)
        if browsermode and browsermode == 1:
            context['browsermode'] = True
        else:
            context['browsermode'] = False

        if context['trialgamesave']:
            context['querystring'] = "?token=%s&quiz=%s&trialgamesave=%s" % (context['token_str'], context['quiz'].id, context['trialgamesave'])
        else:
            context['querystring'] = "?token=%s&quiz=%s" % (context['token_str'], context['quiz'].id)

        # if hasattr(user, "masteruser") and user.masteruser:
        #     game = self.request.GET.get("game", "")
        #     slot = self.request.GET.get("slot", "")
        #     num = self.request.GET.get("num", "")
        #     if game and slot and num:
        #         trialgamesave = TrialGameSave.objects.filter(masteruser=user.masteruser,
        #                                             game=int(game), slot=int(slot),
        #                                             num=int(num)).first()
        #     else:
        #         trialgamesave = TrialGameSave.objects.none()
        #     context['trialgamesave'] = trialgamesave
        # else:
        #     context['trialgamesave'] = TrialGameSave.objects.none()

        return TemplateResponse(self.request, self.template_name, context)