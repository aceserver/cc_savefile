# TEMPLATE URLS
from django.conf.urls import include, url
from django.conf.urls import url
from .views import QuizView, QuizCompleteView


urlpatterns = [
    url(r'^quiz/complete/(?P<pk>.*)/$',
        QuizCompleteView.as_view(),
        name="quiz_sitting_complete"),

    url(r'^quiz/(?P<pk>.*)/$',
        QuizView.as_view(),
        name="quiz_sitting"),
]

# API URLS
from rest_framework import routers
from .api.viewsets import QuizViewSet

router = routers.DefaultRouter()

router.register(r'quiz', QuizViewSet)

urlpatterns += [
    url(r'^api/', include(router.urls)),
]