from django.contrib import admin
import nested_admin
from .models import (Quiz, Question, AnswerChoice,
                QuizAttribute, AnswerAttributeValue,
                Sitting, SittingImage)

class QuizAttributeAdmin(nested_admin.NestedModelAdmin):
    list_display = ("title",)

class AnswerAttributeValueInline(nested_admin.NestedTabularInline):
    model = AnswerAttributeValue
    sortable_field_name = "order"
    extra = 0

class AnswerChoiceInline(nested_admin.NestedTabularInline):
    model = AnswerChoice
    sortable_field_name = "order"
    extra = 0
    inlines = [AnswerAttributeValueInline, ]

class QuestionInline(nested_admin.NestedTabularInline):
    model = Question
    inlines = [AnswerChoiceInline, ]
    sortable_field_name = "order"
    extra = 0

class QuizAdmin(nested_admin.NestedModelAdmin):
    list_display = ("title",)
    inlines = [QuestionInline, ]

class SittingImageInline(nested_admin.NestedTabularInline):
    model = SittingImage
    extra = 0

class SittingAdmin(nested_admin.NestedModelAdmin):
    list_display = ("masteruser", "quiz", "trialgamesave", "created")
    inlines = [SittingImageInline, ]
    list_filter = ["quiz",]
    search_fields = ['masteruser__username', 'masteruser__email', 'masteruser__user_id']

admin.site.register(Quiz, QuizAdmin)
admin.site.register(QuizAttribute, QuizAttributeAdmin)
# admin.site.register(Question, QuestionInline)
admin.site.register(AnswerChoice)
admin.site.register(Sitting, SittingAdmin)
# admin.site.register(SittingImage, SittingImageInline)