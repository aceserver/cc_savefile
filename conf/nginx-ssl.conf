server {
    listen         80;
    server_name    edu.chemcaper.com;
    return         301 https://edu.chemcaper.com$request_uri;
}

server {
    listen 443;
    server_name edu.chemcaper.com;
    client_max_body_size 20M;
    large_client_header_buffers 4 32k;

    ssl on;
    ssl_certificate             /etc/nginx/ssl/aceedventure_com/ssl-bundle.crt;
    ssl_certificate_key         /etc/nginx/ssl/aceedventure_com/aceedventure.key;
    ssl_stapling                on;
    ssl_stapling_verify         on;
    ssl_trusted_certificate     /etc/nginx/ssl/aceedventure_com/comodo.pem;
    ssl_protocols               TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers                 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
    ssl_session_cache           shared:SSL:50m;
    ssl_session_timeout         5m;
    ssl_prefer_server_ciphers   on;
    ssl_dhparam                 /etc/ssl/certs/dhparam.pem;
    add_header                  Strict-Transport-Security "max-age=31536000";

    root /opt/static/cc_savefile;
    include /etc/nginx/includes/static_params;
    include /etc/nginx/includes/deny_params;

    location / {
        try_files $uri @proxy;
    }

    location /media {
        # internal;
        alias /opt/static/cc_savefile/media;
    }

    location @proxy {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://127.0.0.1:10110;
    }
}
