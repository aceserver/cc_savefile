# Available API calls #

### Full Access Version ###
**1. Register user**


```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/register/ --data 'email=<USER EMAIL>&password=<PASSWORD>'
```


**2. Create anonymous user**

```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/create_anonymous/
```

Will return the MasterUser user_id. The actual django.contrib.auth.models User and Token will be created too. All other fields are empty.


**3. Login to get Token**


Using MasterUser.username and password (EdVenture Pass)


Do a POST request with the parameters required to

```
#!

https://edu.chemcaper.com/api/masteruser/login/
```

*Sample parameters that you are required to send:*

```
#!

para = {"user_name" : <STR MasterUser.username>,
        "password"  : <STR MasterUser.password>,
        "game"      : <INT Game.id>,
        "platform"  : <INT Platform.id>,
        };
```

This will be the standard login API call for all future builds.

The reason this API is created is because we can restrict access to games and platforms for School's Bulk Purchased Licenses.

For normal users that are not tied to Schools, we will still use this API call with all the parameters included.

---------------------------------------------------------------------

(For School's Full Access only) Using MasterUser.username & SchoolUser.school_id (DEPRECEATED but remained here for legacy builds)

```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/school_full_login/ --data 'user_name=<MasterUser.username>&school=<SchoolUser.school_id>'

e.g.
curl -X POST https://edu.chemcaper.com/api/masteruser/school_full_login/ --data 'user_name=l33th8ckorz&school=MYCLS1'
```

---------------------------------------------------------------------

(For TeacherGaming accounts only) Using TeacherGaming username & TeacherGaming class ID

```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/teachergaming_login/ --data 'user_name=<TeacherGaming username>&classroom=<TeacherGaming class ID>'
```

---------------------------------------------------------------------

(For Steam accounts only) Using Steam ID

```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/steam_login/ --data 'steam_id=<Steam ID>'
```

---------------------------------------------------------------------

Using MasterUser.username & password (TEMP UNUSED)

```
#!

curl -X POST https://edu.chemcaper.com/auth/login/ --data 'username=<USER EMAIL>&password=<PASSWORD>'
```


If the MasterUser does not exist, login will be unsuccessful.



*Sample return data:*

```
#!

{"auth_token":"d40eba431dbc31d3e4ab5095905a86d2c904c45c"}
```


**4. Get current user details**


```
#!

curl -X GET https://edu.chemcaper.com/api/masteruser/detail/ -H 'Authorization: Token <TOKEN>'
```

*Sample return data:*


```
#!

{"email":"example@email.com",
 "user_id":"<MasterUser.user_id>",
 "is_expired_account": false,
 "is_trial": false,
 "apple_id": null,
 "googleplay_id": null,
 "gamecenter_id": null,
 "filament_id": null,
 "teachergaming_id": null}
```


**5. Logout**


```
#!

curl -X POST https://edu.chemcaper.com/auth/logout/ -H 'Authorization: Token <TOKEN>'
```


**6. Save game / Create new save**

*Sample parameters that you are required to send:*

```
#!

para = {"platform"     : <INT Platform.id>,
        "game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        "data_version" : <STR or FLOAT of the data version>,
        "json_data"    : <JSON, must encapsulate every string with double quotes ("")>,
        "description"  : <STR description of save file e.g. last location, current quest>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/gamesave/perform_save/
```

NOTE: Platform ID should be set to 3 for PC Version save files

NOTE: Platform ID should be set to 5 for TeacherGaming save files


**7. Get game save slots**

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>};
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/gamesave/get_save_slots/
```

*Sample return data:*


```
#!

{
  "data": [
    {
      "slot": 0,
      "description": "Sub Rosa",
      "modified": "14/02/2017 05:52:08 AM",
      "prev_json_data": null,
      "json_data": {
        "position": [
          9,
          20,
          10
        ]
      },
    },
    {
      "slot": 1,
      "description": "Reacta",
      "modified": "20/02/2017 08:23:38 AM",
      "prev_json_data": null,
      "json_data": {
        "position": [
          9,
          20,
          10
        ],
        "potions": 9
      },
    }
  ],
  "success": true,
  "error": ""
}
```


**8. Load latest save**

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/gamesave/load_save/
```


**9. Backup successfully loaded save data to prev_json_data field**

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/gamesave/load_success_backup_save/
```


**10. Delete Save Game**

Delete save game that belongs to a MasterUser. It will set that GameSave of the slot number to inactive.

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/gamesave/delete_save/
```


**11. Update MasterUser with the userID of the platform being used**

*Sample parameters that you are required to send:*

```
#!

para = {"apple_id"         : <STR apple ID. Optional.>,
        "googleplay_id"    : <STR google play ID. Optional.>,
        "gamecenter_id"    : <STR game center ID. Optional.>,
        "filament_id"      : <STR filament games ID. Optional.>,
        "teachergaming_id" : <STR teacher gaming ID. Optional.>,
        "steam_id"         : <STR steam ID. Optional.>,
        };
```

Do a POST request with the parameters chosen & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/masteruser/update_platform_id/
```

It will update the MasterUser with the fields you provided in the POST data. The fields are not compulsory.
User ID and email cannot be updated using this function.

### Trial Version ###
For Trial version, there's no registration API call, because the licenses are generated
from the edu.chemcaper.com website.

**1. Login to get Token**


Using MasterUser.username and password (ACE Login)


Do a POST request with the parameters required to

```
#!

https://edu.chemcaper.com/api/masteruser/login/
```

*Sample parameters that you are required to send:*

```
#!

para = {"user_name" : <STR MasterUser.username>,
        "password"  : <STR MasterUser.password>,
        "game"      : <INT Game.id>,
        "platform"  : <INT Platform.id>,
        };

NOTE: Trial Platform ID = 7
```

This will be the standard login API call for all future builds.

The reason this API is created is because we can restrict access to games and platforms for School's Bulk Purchased Licenses.

For normal users that are not tied to Schools, we will still use this API call with all the parameters included.


---------------------------------------------------------------------

(DEPRECEATED but remained here for legacy builds)

If the MasterUser exists, and MasterUser.auth_user is not activated yet, logging in successfully
will change MasterUser.auth_user.is_active = True (Activated account).
If the MasterUser does not exist, login will be unsuccessful.

```
#!

curl -X POST https://edu.chemcaper.com/api/masteruser/trial_login/ --data 'user_name=<MasterUser.username>&school=<SchoolUser.id>'

e.g.
curl -X POST https://edu.chemcaper.com/api/masteruser/trial_login/ --data 'user_name=student11&school=1'
```


*Sample return data:*

```
#!

{"auth_token":"d40eba431dbc31d3e4ab5095905a86d2c904c45c"}
```

**2. Logout**


```
#!

curl -X POST https://edu.chemcaper.com/auth/logout/ -H 'Authorization: Token <TOKEN>'
```


**3. Save game**

Will find the TrialGameSave of the slot provided with the latest 'num', and save the data.
If no TrialGameSave of the slot provided exists, it will create a new TrialGameSave of that slot.

*Sample parameters that you are required to send:*

```
#!

para = {"platform"     : <INT Platform.id>,
        "game"         : <INT Game.id>,
        "slot"         : <INT slot number. Use 0 if platform doesn't have save slots>,
        "data_version" : <STR or FLOAT of the data version>,
        "json_data"    : <JSON, must encapsulate every string with double quotes ("")>,
        "description"  : <STR description of save file e.g. last location, current quest>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/perform_save/
```

NOTE: Platform ID should be set to 7 for Trial Version save files


**4. Create new Save Game**

Creates new TrialGameSave with the slot provided.
"num" is auto-incremented.
Will deactivate previous TrialGameSaves.

*Sample parameters that you are required to send:*

```
#!

para = {"platform"     : <INT Platform.id>,
        "game"         : <INT Game.id>,
        "slot"         : <INT slot number. Use 0 if platform doesn't have save slots>,
        "data_version" : <STR or FLOAT of the data version>,
        "json_data"    : <JSON, must encapsulate every string with double quotes ("")>,
        "description"  : <STR description of save file e.g. last location, current quest>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/create_new_save/
```

NOTE: Platform ID should be set to 7 for Trial Version save files


**5. Get game save slots**

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>};
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/get_save_slots/
```

*Sample return data:*


```
#!

{
  "data": [
    {
      "slot": 0,
      "num": 1,
      "description": "Sub Rosa",
      "modified": "14/02/2017 05:52:08 AM",
      "prev_json_data": null,
      "json_data": {
        "position": [
          9,
          20,
          10
        ]
      },
    },
    {
      "slot": 1,
      "num": 3,
      "description": "Reacta",
      "modified": "20/02/2017 08:23:38 AM",
      "prev_json_data": null,
      "json_data": {
        "position": [
          9,
          20,
          10
        ],
        "potions": 9
      },
    }
  ],
  "success": true,
  "error": ""
}
```


**6. Load latest save**

Don't need to include the "num", because the code will automatically fetch
the latest "num" of the TrialGameSave of that slot.

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/load_save/
```


**7. Backup successfully loaded save data to prev_json_data field**
Run this if game can load from json_data successfully.
It will backup json_data into prev_json_data.
If failed to load game from json_data, use prev_json_data, but don't run this function.
Just Save (step 3) in the next save.

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/load_success_backup_save/
```

**8. Mark Save Game as Completed**

Mark save game that belongs to a MasterUser as completed.
It will set the is_completed field of the TrialGameSave of the slot number to True.

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/mark_as_completed/
```


**9. Delete Save Game**

Delete save game that belongs to a MasterUser.
It will set the is_active field of the TrialGameSave of the slot number to False.

*Sample parameters that you are required to send:*

```
#!

para = {"game"         : <INT Game.id>,
        "slot"         : <INT slot number>,
        "num"          : <INT save game num>,
        };
```

Do a POST request with the parameters required & include the Token in your Request Headers to

```
#!

https://edu.chemcaper.com/api/trialgamesave/delete_save/
```


## NOTE: ##
How to add Token to Request Header in POSTMAN?
In the Headers section, type Authorization in key field, then type Token <TOKEN> in the value field.

**Platforms:**

ID: TITLE

 1: Android

 2: iOS

 3: PC Kickstarter

 4: Steam

 5: Teacher Gaming

 6: Filament Games

 7: Trial

 8: School PC Windows

 9: School PC Mac

 10: School Mobile Android

 11: School Mobile iOS


**Games:**

ID: TITLE

 1: ChemCaper


This is the end of the Readme file
